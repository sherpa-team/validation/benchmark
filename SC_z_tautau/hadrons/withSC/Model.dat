!-------------------------------------------------------
!-- Model parameters -----------------------------------
!-------------------------------------------------------
MODEL                 = SM       ! Model 
!
GENERATOR_ON          = 0        ! use external spectrum generator
!
! SM parameters
!
EW_SCHEME             = 0        ! which parameters define the ew sector.
ALPHAS(MZ)            = 0.129    ! strong coupling at scale M_Z 
ALPHAS(default)       = 0.0800   ! strong coupling
ORDER_ALPHAS          = 0        ! LO
1/ALPHAQED(0)         = 137.036  ! inverse of alpha QED in the Thomson limit
1/ALPHAQED(default)   = 132.51   ! inverse of alpha QED
SIN2THETAW            = 0.23147   ! Weinberg angle at scale M_Z
VEV                   = 246.     ! Higgs vev        
LAMBDA                = 0.47591  ! SM Higgs self coupling
YUKAWA_E              = 0.       ! Yukawa coupling of electron
YUKAWA_MU             = 0.105    ! Yukawa coupling of mu
YUKAWA_TAU            = 1.77     ! Yukawa coupling of tau
YUKAWA_U              = 0.       ! Yukawa coupling of u quark
YUKAWA_D              = 0.       ! Yukawa coupling of d quark
YUKAWA_S              = 0.       ! Yukawa coupling of s quark
YUKAWA_C              = 1.3      ! Yukawa coupling of c quark
YUKAWA_B              = 4.5      ! Yukawa coupling of b quark
YUKAWA_T              = 175.     ! Yukawa coupling of t quark
MSTRANGE_EFF          = 0.48     ! effective strange mass for higgs-loops.
CKMORDER              = 0        ! order of expansion of CKM matrix in Cabibbo angle
CABIBBO               = 0.22     ! Cabibbo angle (Wolfenstein parametrization)
A                     = 0.85     ! A (Wolfenstein parametrization)
RHO                   = 0.50     ! rho (Wolfenstein parametrization)
ETA                   = 0.50     ! eta (Wolfenstein parametrization)
!
HIGGS_GENERATOR       = Hdecay   ! Generator for Higgs masses & widths
HIGGS_NNLO(M)         = 1        ! Use nnlo running of masses in higgs decays : 
                                 ! allowed values 1,2
HIGGS_ON_SHELL_WZ     = 1        ! Only on-shell gauge bosons in higgs decays : 
                                 ! 0 = double,1 = single  
HIGGS_MASS_SCHEME     = 0        ! Scheme for higgs masses in higgs decays    : 
                                 ! running/pole
HIGGS_NF_LIGHT_Q_IN_G = 5        ! Number of light flavours in higgs 
                                 ! decays h->gg*->g qqbar   
!
! SUSY parameters
!
SUSY_GENERATOR        = LesHouches
LESHOUCHESINPUT       = LesHouches_SPS1A.dat
!
SUSY_SCENARIO         = mSUGRA   ! also available : AMSB, mGMSB,non-minimal GMSB, 
                                 !                  SUGRA with enforced unification, 
                                 !                  non-universal SUGRA
M0                    = 200.     !
M12                   = 200.     !
A0                    = 100.     !
TAN(BETA)             = 3.       !
SIGN(MU)              = -1       !
!
! ADD parameters
!
N_ED                  = 2         ! Number of extra dimensions in ADD model
G_NEWTON       	      = 6.707e-39 ! Newton gravity constant
M_S                   = 2.5e3     ! string scale for ADD model, meaning depends on KK_CONVENTION
M_CUT                 = 2.5e3     ! cut-off scale for the c.m. energy
KK_CONVENTION         = 1         ! 0=const mass   
                                  ! 1=simplified sum(HLZ)   2=exact sum(HLZ)
                                  ! 3=Hewett +1    4=Hewett -1 
                                  ! 5=GRW (Lambda_T(GRW)=M_S for virtual Graviton exchange
				  !        or M_D=M_S for real Gravtion production)
				  ! only KK_CONVENTION's 1,2 or 5 are applicable for real Graviton production!
