(observables5){
  PIECE_SETUP Observables_5.C (kt_jets_jet_1_1_y3p_0.dat){ }(kt_jets_jet_1_1_y3p_0.dat);
  PIECE_SETUP Observables_5.C (kt_jets_jet_1_1_eta3p_1.dat){ }(kt_jets_jet_1_1_eta3p_1.dat);
  ## RESIZE_BINS -6 4;
}(observables5);

(kt_jets_jet_1_1_eta3p_1.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_jet_1_1_eta3p_1.dat;
  DRAW YES;
  X_MIN -6;
  X_MAX 6;
  Y_MIN 1e-06;
  Y_MAX 0.3;
  HISTOGRAM_NAME kt_jets_jet_1_1_eta3p_1;
  WEBPAGE_CAPTION &eta\;<sup>*</sup><sub>3</sub>;
  X_AXIS_TITLE #eta^{*}_{3}= #eta_{3}-(#eta_{1}+ #eta_{2})/2;
}(kt_jets_jet_1_1_eta3p_1.dat);

(kt_jets_jet_1_1_y3p_0.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_jet_1_1_y3p_1.dat;
  DRAW YES;
  X_MIN -6;
  X_MAX 6;
  Y_MIN 0;
  Y_MAX 0.3;
  HISTOGRAM_NAME kt_jets_jet_1_1_y3p_1;
  WEBPAGE_CAPTION y<sup>*</sup><sub>3</sub>;
  X_AXIS_TITLE y^{*}_{3}= y_{3}-(y_{1}+ y_{2})/2;
}(kt_jets_jet_1_1_y3p_0.dat);

