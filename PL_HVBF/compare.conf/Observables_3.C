(observables3){
  PIECE_SETUP Observables_3.C (kt_jets_jet_1_1_dR2_1.dat){ }(kt_jets_jet_1_1_dR2_1.dat);
  PIECE_SETUP Observables_3.C (kt_jets_jet_1_1_dR2_2.dat){ }(kt_jets_jet_1_1_dR2_2.dat);
  PIECE_SETUP Observables_3.C (kt_jets_jet_1_1_dR2_3.dat){ }(kt_jets_jet_1_1_dR2_3.dat);
  PIECE_SETUP Observables_3.C (kt_jets_jet_1_1_dR2_5.dat){ }(kt_jets_jet_1_1_dR2_5.dat);
  PIECE_SETUP Observables_3.C (kt_jets_jet_1_1_dR2_6.dat){ }(kt_jets_jet_1_1_dR2_6.dat);
  PIECE_SETUP Observables_3.C (kt_jets_jet_1_1_dR2_8.dat){ }(kt_jets_jet_1_1_dR2_8.dat);
  ## RESIZE_BINS 0 4;
}(observables3);

(kt_jets_jet_1_1_dR2_1.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_jet_1_1_dR2_1.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 3.96;
  Y_MIN 1e-6;
  Y_MAX 0.6;
  HISTOGRAM_NAME kt_jets_jet_1_1_dR2_1;
  WEBPAGE_CAPTION &Delta\;R<sub>jet1,2</sub>;
  X_AXIS_TITLE #Delta R_{jet 1,jet2};
}(kt_jets_jet_1_1_dR2_1.dat);

(kt_jets_jet_1_1_dR2_2.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_jet_1_1_dR2_2.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 4;
  Y_MIN 1e-6;
  Y_MAX 0.4;
  HISTOGRAM_NAME kt_jets_jet_1_1_dR2_2;
  WEBPAGE_CAPTION &Delta\;R<sub>jet1,3</sub>;
  X_AXIS_TITLE #Delta R_{jet 1,jet3};
}(kt_jets_jet_1_1_dR2_2.dat);

(kt_jets_jet_1_1_dR2_3.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_jet_1_1_dR2_3.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 4;
  Y_MIN 1e-6;
  Y_MAX 0.2;
  HISTOGRAM_NAME kt_jets_jet_1_1_dR2_3;
  WEBPAGE_CAPTION &Delta\;R<sub>jet1,4</sub>;
  X_AXIS_TITLE #Delta R_{jet 1,jet4};
}(kt_jets_jet_1_1_dR2_3.dat);

(kt_jets_jet_1_1_dR2_5.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_jet_1_1_dR2_5.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 4;
  Y_MIN 1e-6;
  Y_MAX 0.4;
  HISTOGRAM_NAME kt_jets_jet_1_1_dR2_5;
  WEBPAGE_CAPTION &Delta\;R<sub>jet2,3</sub>;
  X_AXIS_TITLE #Delta R_{jet 2,jet3};
}(kt_jets_jet_1_1_dR2_5.dat);

(kt_jets_jet_1_1_dR2_6.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_jet_1_1_dR2_6.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 4;
  Y_MIN 1e-6;
  Y_MAX 0.2;
  HISTOGRAM_NAME kt_jets_jet_1_1_dR2_6;
  WEBPAGE_CAPTION &Delta\;R<sub>jet2,4</sub>;
  X_AXIS_TITLE #Delta R_{jet 2,jet4};
}(kt_jets_jet_1_1_dR2_6.dat);

(kt_jets_jet_1_1_dR2_8.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_jet_1_1_dR2_8.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 4;
  Y_MIN 1e-6;
  Y_MAX 0.2;
  HISTOGRAM_NAME kt_jets_jet_1_1_dR2_8;
  WEBPAGE_CAPTION &Delta\;R<sub>jet3,4</sub>;
  X_AXIS_TITLE #Delta R_{jet 3,jet4};
}(kt_jets_jet_1_1_dR2_8.dat);

