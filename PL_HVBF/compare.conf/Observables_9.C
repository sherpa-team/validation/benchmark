(observables9){
  PIECE_SETUP Observables_9.C (kt_jets_picci_jet_1_1_dphi2_1.dat){ }(kt_jets_picci_jet_1_1_dphi2_1.dat);
//   PIECE_SETUP Observables_9.C (kt_jets_picci_jet_1_1_dphi2_2.dat){ }(kt_jets_picci_jet_1_1_dphi2_2.dat);
//   PIECE_SETUP Observables_9.C (kt_jets_picci_jet_1_1_dphi2_3.dat){ }(kt_jets_picci_jet_1_1_dphi2_3.dat);
//   PIECE_SETUP Observables_9.C (kt_jets_picci_jet_1_1_dphi2_4.dat){ }(kt_jets_picci_jet_1_1_dphi2_4.dat);
  ## RESIZE_BINS 0 8;
}(observables9);

(kt_jets_picci_jet_1_1_dphi2_1.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_picci_jet_1_1_dphi2_1.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 3.2;
  Y_MIN 0;
  Y_MAX 0.4;
  LEG_LEFT 0.1; LEG_RIGHT 0.3; LEG_TOP 0.95;
  HISTOGRAM_NAME kt_jets_picci_jet_1_1_dphi2_1;
  WEBPAGE_CAPTION &Delta\;&phi\;<sub>jet1,2</sub>(VBF Cuts);
  X_AXIS_TITLE #Delta#phi_{jet 1,jet 2} (VBF cuts);
}(kt_jets_picci_jet_1_1_dphi2_1.dat);

(kt_jets_picci_jet_1_1_dphi2_2.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_picci_jet_1_1_dphi2_2.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 3.2;
  Y_MIN 0.0;
  Y_MAX 0.2;
  HISTOGRAM_NAME kt_jets_picci_jet_1_1_dphi2_2;
  WEBPAGE_CAPTION &Delta\;&phi\;<sub>jet1,3</sub>(VBF Cuts);
  X_AXIS_TITLE #Delta#phi_{jet 1,jet 3} (VBF cuts);
}(kt_jets_picci_jet_1_1_dphi2_2.dat);

(kt_jets_picci_jet_1_1_dphi2_3.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE kt_jets_picci_jet_1_1_dphi2_3.dat;
  DRAW YES;
  X_MIN 0;
  X_MAX 3.2;
  Y_MIN 0.0;
  Y_MAX 0.06;
  HISTOGRAM_NAME kt_jets_picci_jet_1_1_dphi2_3;
  WEBPAGE_CAPTION &Delta\;&phi\;<sub>jet1,4</sub>(VBF Cuts);
  X_AXIS_TITLE #Delta#phi_{jet 1,jet 4} (VBF cuts);
}(kt_jets_picci_jet_1_1_dphi2_3.dat);

