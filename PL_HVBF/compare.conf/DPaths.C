(dpaths){
  // PIECE_SETUP DPaths.C (dpath){ }(dpath);
  // PIECE_SETUP DPaths.C (dpathb){ }(dpathb);
  PIECE_SETUP DPaths.C (dpath[1[+1]5]){ }(dpath[1[+1]5]);
  DRAW_LEGEND NO;
  ## COLUMNS 2 2 5 5;
  X_ERROR_PLUS 3; X_ERROR_MINUS 4;
  Y_ERROR_PLUS 5; Y_ERROR_MINUS 6;
  DATA_TYPE ALGEBRA(y[0]/y[1]-1)(y[2]/y[1]+y[3]/y[1]*y[0]/y[1])(y[2]/y[1]+y[3]/y[1]*y[0]/y[1]);
}(dpaths);

(dpathb){
  PATH_PIECE DPATH/; FILE_PIECE .dat;
  Y_FUNCTION 0|dy/y; DRAW_PRIORITY -15;
  X_VALUE 1; Y_VALUE 4;
  X_ERROR_MINUS 2; X_ERROR_PLUS 3;
  Y_ERROR_MINUS 5; Y_ERROR_PLUS 5;
  DRAW_OPTION P2; FILL_COLOUR 5; MARKER_SIZE 0;
}(dpathb);
(dpath){
  PATH_PIECE DPATH/; FILE_PIECE .dat;
  Y_FUNCTION 0|dy/y;
  X_VALUE 1; Y_VALUE 4;
  X_ERROR_MINUS 2; X_ERROR_PLUS 3;
  Y_ERROR_MINUS 5; Y_ERROR_PLUS 5;
  MARKER_STYLE 21; MARKER_SIZE 0.5;
  MARKER_COLOUR 12; LINE_COLOUR 12;
  DRAW_OPTION P; DRAW_PRIORITY -5;
}(dpath);

(dpath1){
  PIECE_SETUP Jets.C (jets){ }(jets);
  PATH_PIECE PATH1/ PATH1/ PATH1/ PATH1/;
  // ## ADOPT_BINS PATH1/ATYPE1/;
  if (JETCONTS&2) LINE_COLOUR RED1;
  else LINE_STYLE 1;
//   if (DRAWCUT!=false) {
//     if (LOGX==true) DRAW_LINE V log10(sqrt(CUT1)*ECMS) | STYLE 1 \;;
//     if (LOGX!=true) DRAW_LINE V sqrt(CUT1)*ECMS | STYLE 1 \;;
//   }
  @@ NOCONT true;
}(dpath1);

(dpath2){
  PIECE_SETUP Jets.C (jets){ }(jets);
  PATH_PIECE PATH2/ PATH1/ PATH2/ PATH1/;
  // ## ADOPT_BINS PATH1/ATYPE1/;
  if (JETCONTS&2) LINE_COLOUR GREEN1;
  else LINE_STYLE 2;
//   if (DRAWCUT!=false) {
//     if (LOGX==true) DRAW_LINE V log10(sqrt(CUT2)*ECMS) | STYLE 2 \;;
//     if (LOGX!=true) DRAW_LINE V sqrt(CUT2)*ECMS | STYLE 2 \;;
//   }
  @@ NOCONT true;
}(dpath2);

(dpath3){
  PIECE_SETUP Jets.C (jets){ }(jets);
  PATH_PIECE PATH3/ PATH1/ PATH3/ PATH1/;
  // ## ADOPT_BINS PATH1/ATYPE1/;
  if (JETCONTS&2) LINE_COLOUR BLUE1;
  else LINE_STYLE 3;
//   if (DRAWCUT!=false) {
//     if (LOGX==true) DRAW_LINE V log10(sqrt(CUT3)*ECMS) | STYLE 3 \;;
//     if (LOGX!=true) DRAW_LINE V sqrt(CUT3)*ECMS | STYLE 3 \;;
//   }
  @@ NOCONT true;
}(dpath3);

(dpath4){
  PIECE_SETUP Jets.C (jets){ }(jets);
  PATH_PIECE PATH4/ PATH1/ PATH4/ PATH1/;
  // ## ADOPT_BINS PATH1/ATYPE1/;
  if (JETCONTS&2) LINE_COLOUR VIOLET1;
  else LINE_STYLE 4;
//   if (DRAWCUT!=false) {
//     if (LOGX==true) DRAW_LINE V log10(sqrt(CUT4)*ECMS) | PRIORITY -10 STYLE 4 \;;
//     if (LOGX!=true) DRAW_LINE V sqrt(CUT4)*ECMS | PRIORITY -10 STYLE 4 \;;
//   }
  @@ NOCONT true;
}(dpath4);

(dpath5){
  PIECE_SETUP Jets.C (jets){ }(jets);
  PATH_PIECE PATH5/ PATH1/ PATH5/ PATH1/; DRAW_PRIORITY -1;
  // ## ADOPT_BINS PATH1/ATYPE1/;
  if (JETCONTS&2) LINE_COLOUR YELLOW1;
  else LINE_STYLE 5;
//   if (DRAWCUT!=false) {
//     if (LOGX==true) DRAW_LINE V log10(sqrt(CUT4)*ECMS) | PRIORITY -10 STYLE 4 \;;
//     if (LOGX!=true) DRAW_LINE V sqrt(CUT4)*ECMS | PRIORITY -10 STYLE 4 \;;
//   }
  @@ NOCONT true;
}(dpath5);

