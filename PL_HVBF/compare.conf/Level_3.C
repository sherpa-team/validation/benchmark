(level3){
  PIECE_SETUP Level_3.C (Hadron){ }(Hadron);
  PIECE_SETUP Level_3.C (ME){ }(ME);
  PIECE_SETUP Level_3.C (Shower){ }(Shower);
}(level3);

(Hadron){
  PIECE_SETUP Level_4.C (level4){ }(level4);
  PATH_PIECE Hadron/;
}(Hadron);

(ME){
  PIECE_SETUP Level_4.C (level4){ }(level4);
  PATH_PIECE ME/;
}(ME);

(Shower){
  PIECE_SETUP Level_4.C (level4){ }(level4);
  PATH_PIECE Shower/;
}(Shower);
