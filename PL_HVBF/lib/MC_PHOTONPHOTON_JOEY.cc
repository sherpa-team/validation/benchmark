// -*- C++ -*-
#include "Rivet/Analyses/MC_JetAnalysis.hh"
#include "Rivet/Math/MathUtils.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/RivetAIDA.hh"
#include <map>
#include <sstream>

namespace Rivet {


  /// @brief MC validation analysis for higgs [-> tau tau] + jets events
  class MC_PHOTONPHOTON_JOEY : public Analysis {
  private:
    double _phy, _ph1pT, _ph2pT, _phisoE, _phisodR;
    double _jphdR, _jeta, _jR, _jpTtag, _jpT, _jjdy, _jjmass;
    std::vector<double> _j1pTs;
    std::map<std::string,AIDA::IHistogram1D *> histos, jetvetohistos;
  public:
    MC_PHOTONPHOTON_JOEY() : 
      Analysis("MC_PHOTONPHOTON_JOEY"),
      _phy(2.37), _ph1pT(43.75), _ph2pT(31.25), _phisoE(14.), _phisodR(0.4),
      _jphdR(0.4), _jeta(4.4), _jR(0.4), _jpT(30.)
    {}
    
    double sqr(const double& x) { return x*x; }

    void init() {
      FinalState fs;
      IdentifiedFinalState photons(-_phy,_phy,_ph2pT*GeV);
      photons.acceptId(PHOTON);
      addProjection(fs, "FS");
      addProjection(photons, "Photons");

      inithistos();
    }

    void inithistos() {
      histos["XS"] = bookHistogram1D("XS",1,0.,1.);
      histos["m_gammagamma"] = bookHistogram1D("m_gammagamma",BWspace(21,124.9,125.1,125.,0.00407));

      std::vector<double> H_pT_bins,H_y_bins,jet1_pT_bins,
                          deltaphi_jj_bins,deltaphi_Hjj_bins,
                          Hjj_pT_bins,Hjj_pT_fine_bins;
      H_pT_bins += 0.,20.,30.,40.,50.,60.,80.,100.,200.;
      histos["H_pT"] = bookHistogram1D("H_pT",H_pT_bins);

      H_y_bins += 0.,0.3,0.65,1.0,1.4,2.4;
      histos["H_y"] = bookHistogram1D("H_y",H_y_bins);

      histos["cos_theta_star"] = bookHistogram1D("cos_theta_star",10,0.,1.);

      histos["NJet_incl"] = bookHistogram1D("NJet_incl",4,-0.5,3.5);
      histos["NJet_excl"] = bookHistogram1D("NJet_excl",4,-0.5,3.5);

      jet1_pT_bins += 0.,30.,50.,70.,100.,140.;
      histos["jet1_pT"] = bookHistogram1D("jet1_pT",jet1_pT_bins);

      deltaphi_jj_bins += 0.,PI/3.,2.*PI/3.,5.*PI/6.,PI;
      histos["deltaphi_jj"] = bookHistogram1D("deltaphi_jj",deltaphi_jj_bins);
      deltaphi_Hjj_bins += 0.,PI/3.,2.*PI/3.,5.*PI/6.,PI;
      histos["deltaphi_Hjj"] = bookHistogram1D("deltaphi_Hjj",deltaphi_Hjj_bins);

      Hjj_pT_bins += 0.,30.,55.,80.,140.;
      histos["Hjj_pT"] = bookHistogram1D("Hjj_pT",Hjj_pT_bins);
      Hjj_pT_fine_bins += 0.,3.75,7.5,11.25,15.,18.75,22.5,26.25,
                          30.,33.125,36.25,39.375,42.5,45.625,48.75,51.875,
                          55.,58.125,61.25,64.375,67.5,70.625,73.75,76.875,
                          80.,87.5,95.,102.5,110.,117.5,125.,132.5,140.;
      histos["Hjj_pT_fine"] = bookHistogram1D("Hjj_pT_fine",Hjj_pT_fine_bins);
    }

    /// Do the analysis
    void analyze(const Event & e) {
      const double weight = e.weight();
      ParticleVector photons =
        applyProjection<IdentifiedFinalState>(e, "Photons").particlesByPt();
      // require at least two photons
      if (photons.size() < 2) vetoEvent;
      // photon isolation
      ParticleVector fs = applyProjection<FinalState>(e, "FS").particles();
      ParticleVector jetparts;
      std::vector<FourMomentum> phs;
      // energy in cone
      // add to jet-input everything but the isolated photons
      foreach (const Particle& ph, photons) {
        FourMomentum mom_in_Cone = -ph.momentum();
        foreach (const Particle& p, fs) {
          if (deltaR(ph,p) < _phisodR) mom_in_Cone += p.momentum();
          if (p.pdgId()!=PHOTON) jetparts.push_back(p);
        }
        if (mom_in_Cone.Et()<_phisoE) phs.push_back(ph.momentum());
        else                          jetparts.push_back(ph);
      }
      // at least two isolated photons
      // TODO: use subleading photons if leading ones are not isolated?
      if (phs.size() < 2) vetoEvent;
      // photon pT cuts
      if (phs[0].pT() < _ph1pT) vetoEvent;
      if (phs[1].pT() < _ph2pT) vetoEvent;
      // isolate photons from one-another
      if (deltaR(phs[0],phs[1]) < _phisodR) vetoEvent;
      // TODO: feed all but the two isolated photons back into jets?

      // define diphoton (Higgs) momentum
      FourMomentum hmom(phs[0]+phs[1]);

      // calculate jets
      FastJets jetpro(FastJets::ANTIKT, _jR);
      jetpro.calc(jetparts);
      Jets jets;
      // apply etamax and dRmin to photons
      foreach (const Jet& jetcand, jetpro.jetsByPt(_jpT*GeV)) {
        FourMomentum jmom = jetcand.momentum();
        if (fabs(jetcand.momentum().rapidity()) < _jeta && 
            deltaR(phs[0],jetcand.momentum()) > _jphdR &&
            deltaR(phs[1],jetcand.momentum()) > _jphdR) {
          jets.push_back(jetcand);
        }
      }

      // fill histograms
      histos["XS"]->fill(0.5,weight);
      histos["m_gammagamma"]->fill(hmom.mass()/GeV,weight);
      histos["H_pT"]->fill(hmom.pT()/GeV,weight);
      histos["H_y"]->fill(fabs(hmom.rapidity()),weight);
      // |costheta*| from 1307.1432
      double cts(abs(sinh(phs[0].eta()-phs[1].eta()))/
                   sqrt(1.+sqr(hmom.pT()/hmom.mass()))
                 * 2.*phs[0].pT()*phs[1].pT()/sqr(hmom.mass()));
      histos["cos_theta_star"]->fill(cts,weight);
      histos["NJet_excl"]->fill(jets.size(),weight);
      for (size_t i(0);i<4;++i) {
        if (jets.size()>=i) histos["NJet_incl"]->fill(i,weight);
      }
      if (jets.size()>0) {
        const FourMomentum& j1(jets[0].momentum());
        histos["jet1_pT"]->fill(j1.pT()/GeV,weight);
        if (jets.size()>1) {
          const FourMomentum& j2(jets[1].momentum());
          histos["deltaphi_jj"]->fill(deltaPhi(j1,j2),weight);
          histos["deltaphi_Hjj"]->fill(deltaPhi(hmom,j1+j2),weight);
          histos["Hjj_pT"]->fill((hmom+j1+j2).pT()/GeV,weight);
          histos["Hjj_pT_fine"]->fill((hmom+j1+j2).pT()/GeV,weight);
        }
      }
    }

    /// Finalize
    void finalize() {
      double scalefactor(crossSection()/sumOfWeights());
      for (std::map<std::string,AIDA::IHistogram1D *>::iterator 
             hit=histos.begin(); hit!=histos.end();hit++) 
        scale(hit->second,scalefactor);
    }
  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_PHOTONPHOTON_JOEY);
}
