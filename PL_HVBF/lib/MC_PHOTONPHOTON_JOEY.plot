# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/XS
Title=Fiducial cross section
XLabel=
YLabel=$\sigma$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/m_gammagamma
Title=Diphoton invariant mass
XLabel=$m_{\gamma\gamma}$ [GeV]
YLabel=d$\sigma$/d$m_{\gamma\gamma}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/H_pT
Title=Transverse momentum of the reconstructed Higgs boson
XLabel=$p_\perp^h$ [GeV]
YLabel=d$\sigma$/d$p_\perp^h$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/H_y
Title=Rapidity of the reconstructed Higgs boson
XLabel=$y_h$
YLabel=d$\sigma$/d$y_h$ [pb/GeV]
LegendYPos=0.4
LegendXPos=0.05
LogY=0
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/cos_theta_star
Title=$\cos\theta^\ast$
XLabel=$\cos\theta^\ast$
YLabel=d$\sigma$/d$\cos\theta^\ast$ [pb]
LogY=0
ShowZero=1
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/NJet_incl
Title=Inclusive jet multiplicity
XLabel=$N_\text{jet}^\text{incl}$
YLabel=d$\sigma$/d$N_\text{jet}^\text{incl}$ [pb]
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/NJet_excl
Title=Exclusive jet multiplicity
XLabel=$N_\text{jet}^\text{excl}$
YLabel=d$\sigma$/d$N_\text{jet}^\text{excl}$ [pb]
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/jet1_pT
Title=Transverse momentum of the leading jet
XLabel=$p_\perp^{j_1}$ [GeV]
YLabel=d$\sigma$/d$p_\perp^{j_1}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/deltaphi_jj
Title=Azumuthal separation of the two leading jets
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/deltaphi_Hjj
Title=Azumuthal separation of the Higgs and the two leading jets
XLabel=$\Delta\phi(h,j_1j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_PHOTONPHOTON_JOEY/Hjj_pT
Title=Transverse momentum of the Higgs plus dijet system
XLabel=$p_\perp^{hj_1j_2}$ [GeV]
YLabel=d$\sigma$/d$p_\perp^{hj_1j_2}$ [pb/GeV]
# END PLOT
