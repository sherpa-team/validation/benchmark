// -*- C++ -*-
#include "Rivet/Analyses/MC_JetAnalysis.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/RivetAIDA.hh"
#include <map>
#include <sstream>

namespace Rivet {


  /// @brief MC validation analysis for higgs [-> tau tau] + jets events
  class MC_HJETS_PHOTONPHOTON_CUTS : public Analysis {
  private:
    double _phy, _ph1pT, _ph2pT, _phisoE, _phisodR, _jphdR;
    double _jeta, _jR, _jpTtag, _jpT, _jjdy, _jjmass;
    std::vector<std::pair<double,std::string> > _jpTs;
    std::map<std::string,AIDA::IHistogram1D *> histos, jetvetohistos;
  public:
    MC_HJETS_PHOTONPHOTON_CUTS() : 
      Analysis("MC_HJETS_PHOTONPHOTON_CUTS"),
      _phy(2.37), _ph1pT(43.75), _ph2pT(31.25), _phisoE(20.), _phisodR(0.4),
      _jphdR(0.4),
      _jeta(5.), _jR(0.4), _jpTtag(30.), _jpT(30.),
      _jjdy(2.8), _jjmass(400.)
    {
      _jpTs += make_pair(15,"15"),make_pair(20,"20"),make_pair(25,"25"),
               make_pair(30,"30"),make_pair(35,"35"),make_pair(40,"40"),
               make_pair(50,"50");
    }

    void init() {
      FinalState fs;
      IdentifiedFinalState photons(-_phy,_phy,_ph2pT*GeV);
      photons.acceptId(PHOTON);
      addProjection(fs, "FS");
      addProjection(photons, "Photons");

      inithistos();
    }

    void inithistos() {
      histos["N_photons"] 
        = bookHistogram1D("N_photons",6,-0.5,5.5);
      histos["N_photons_isolated"] 
        = bookHistogram1D("N_photons_isolated",6,-0.5,5.5);
      histos["cos_theta_star"]
        = bookHistogram1D("cos_theta_star",10,0.,1.);
      histos["cos_theta_star_2jet"]
        = bookHistogram1D("cos_theta_star_2jet",10,0.,1.);
      histos["cos_theta_star_WBF"]
        = bookHistogram1D("cos_theta_star_WBF",10,0.,1.);

      for (size_t i(0);i<_jpTs.size();++i) if (_jpT<=_jpTs[i].first) {
        histos["H_pT_nojet_j"+_jpTs[i].second] 
          = bookHistogram1D("H_pT_nojet_j"+_jpTs[i].second,100,0.,100.);
        histos["jet_multi_j"+_jpTs[i].second+"_i"] 
          = bookHistogram1D("jet_multi_j"+_jpTs[i].second+"_incl",6,-0.5,5.5);
        histos["jet_multi_j"+_jpTs[i].second+"_e"] 
          = bookHistogram1D("jet_multi_j"+_jpTs[i].second+"_excl",6,-0.5,5.5);
      }

      histos["H_pT_peak"] 
        = bookHistogram1D("H_pT_peak",20,1.,21.);
      histos["H_pT_low"] 
        = bookHistogram1D("H_pT_low",20,0.,50.);
      histos["H_pT_long"] 
        = bookHistogram1D("H_pT_long",50,0.,500.);
      histos["H_pT_log"] 
        = bookHistogram1D("H_pT_log",logBinEdges(30,1., 1000.));
      histos["H_jet1_dR"] 
        = bookHistogram1D("H_jet1_dR",24,0.,6.);
      histos["H_jet1_dy"] 
        = bookHistogram1D("H_jet1_dy",20,0.,5.);
      histos["H_jet1_dphi"] 
        = bookHistogram1D("H_jet1_dphi",20,0.,PI);
      histos["Hj_pT"] 
        = bookHistogram1D("Hj_pT",60,0.,300.);
      histos["jet1_pT_log"] 
        = bookHistogram1D("jet1_pT_log",logBinEdges(20,20.,2000.));
      histos["jet2_pT_log"] 
        = bookHistogram1D("jet2_pT_log",logBinEdges(20,20.,2000.));
      histos["jet3_pT_log"]         
        = bookHistogram1D("jet3_pT_log",logBinEdges(10,20.,200.));
      histos["jet4_pT_log"] 
        = bookHistogram1D("jet4_pT_log",logBinEdges(10,20.,200.));
      histos["jet4_y"] 
        = bookHistogram1D("jet4_y",20,-5.,5.);
      histos["jet12_dR"] 
        = bookHistogram1D("jet12_dR",20,0.,10.);

      histos["H_pT"] 
        = bookHistogram1D("H_pT", 20, 0.0, 100.0);
      histos["H_y"]
        = bookHistogram1D("H_y",20,-5.,5.);
      histos["H_jet12_dphi"] 
        = bookHistogram1D("H_jet12_dphi",20,0.,PI);
      histos["H_jet12_pi-dphi"] 
        = bookHistogram1D("H_jet12_pi-dphi",30,0.,1.5);
      histos["jet1_pT"] 
        = bookHistogram1D("jet1_pT",60,0.,300.);
      histos["jet2_pT"] 
        = bookHistogram1D("jet2_pT",60,0.,300.);
      histos["jet3_pT"] 
        = bookHistogram1D("jet3_pT",60,0.,300.);
      histos["jet1_y"] 
        = bookHistogram1D("jet1_y",20,-5.,5.);
      histos["jet2_y"] 
        = bookHistogram1D("jet2_y",20,-5.,5.);
      histos["jet3_y"] 
        = bookHistogram1D("jet3_y",20,-5.,5.);
      histos["jet3_ystar"]
        = bookHistogram1D("jet3_ystar",20,-5.,5.);
      histos["jet12_dy"] 
        = bookHistogram1D("jet12_dy",20,0.,10.);
      histos["jet12_dphi"] 
        = bookHistogram1D("jet12_dphi",20,0.,PI);
      histos["jet12_mass"] 
        = bookHistogram1D("jet12_mass",25,0.,1000.);
      histos["Hjj_pT"] 
        = bookHistogram1D("Hjj_pT",60,0.,300.);
      histos["Hjj_etastar"] 
        = bookHistogram1D("Hjj_etastar",20,0.,10.);

      histos["H_pT_2jet"] 
        = bookHistogram1D("H_pT_2jet", 20, 0.0, 100.0);
      histos["H_y_2jet"]
        = bookHistogram1D("H_y_2jet",20,-5.,5.);
      histos["H_jet12_dphi_2jet"] 
        = bookHistogram1D("H_jet12_dphi_2jet",20,0.,PI);
      histos["H_jet12_pi-dphi_2jet"] 
        = bookHistogram1D("H_jet12_pi-dphi_2jet",30,0.,1.5);
      histos["jet1_pT_2jet"] 
        = bookHistogram1D("jet1_pT_2jet",60,0.,300.);
      histos["jet2_pT_2jet"] 
        = bookHistogram1D("jet2_pT_2jet",60,0.,300.);
      histos["jet3_pT_2jet"] 
        = bookHistogram1D("jet3_pT_2jet",60,0.,300.);
      histos["jet1_y_2jet"] 
        = bookHistogram1D("jet1_y_2jet",20,-5.,5.);
      histos["jet2_y_2jet"] 
        = bookHistogram1D("jet2_y_2jet",20,-5.,5.);
      histos["jet3_y_2jet"] 
        = bookHistogram1D("jet3_y_2jet",20,-5.,5.);
      histos["jet3_ystar_2jet"]
        = bookHistogram1D("jet3_ystar_2jet",20,-5.,5.);
      histos["jet12_dy_2jet"] 
        = bookHistogram1D("jet12_dy_2jet",20,0.,10.);
      histos["jet12_dphi_2jet"] 
        = bookHistogram1D("jet12_dphi_2jet",20,0.,PI);
      histos["jet12_mass_2jet"] 
        = bookHistogram1D("jet12_mass_2jet",25,0.,1000.);
      histos["Hjj_pT_2jet"] 
        = bookHistogram1D("Hjj_pT_2jet",60,0.,300.);
      histos["Hjj_etastar_2jet"] 
        = bookHistogram1D("Hjj_etastar_2jet",20,0.,10.);

      histos["H_pT_WBF"] 
        = bookHistogram1D("H_pT_WBF", 20, 0.0, 100.0);
      histos["H_y_WBF"]
        = bookHistogram1D("H_y_WBF",20,-5.,5.);
      histos["H_jet12_dphi_WBF"] 
        = bookHistogram1D("H_jet12_dphi_WBF",20,0.,PI);
      histos["H_jet12_pi-dphi_WBF"] 
        = bookHistogram1D("H_jet12_pi-dphi_WBF",30,0.,1.5);
      histos["jet1_pT_WBF"] 
        = bookHistogram1D("jet1_pT_WBF",60,0.,300.);
      histos["jet2_pT_WBF"] 
        = bookHistogram1D("jet2_pT_WBF",60,0.,300.);
      histos["jet3_pT_WBF"] 
        = bookHistogram1D("jet3_pT_WBF",60,0.,300.);
      histos["jet1_y_WBF"] 
        = bookHistogram1D("jet1_y_WBF",20,-5.,5.);
      histos["jet2_y_WBF"] 
        = bookHistogram1D("jet2_y_WBF",20,-5.,5.);
      histos["jet3_y_WBF"] 
        = bookHistogram1D("jet3_y_WBF",20,-5.,5.);
      histos["jet3_ystar_WBF"]
        = bookHistogram1D("jet3_ystar_WBF",20,-5.,5.);
      histos["jet12_dy_WBF"] 
        = bookHistogram1D("jet12_dy_WBF",20,0.,10.);
      histos["jet12_dphi_WBF"] 
        = bookHistogram1D("jet12_dphi_WBF",20,0.,PI);
      histos["jet12_mass_WBF"] 
        = bookHistogram1D("jet12_mass_WBF",25,0.,1000.);
      histos["Hjj_pT_WBF"] 
        = bookHistogram1D("Hjj_pT_WBF",60,0.,300.);
      histos["Hjj_etastar_WBF"] 
        = bookHistogram1D("Hjj_etastar_WBF",20,0.,10.);

      histos["H_pT_WBF_central_jet_veto"] 
        = bookHistogram1D("H_pT_WBF_central_jet_veto", 20, 0.0, 100.0);
      histos["H_y_WBF_central_jet_veto"]
        = bookHistogram1D("H_y_WBF_central_jet_veto",20,-5.,5.);
      histos["H_jet12_dphi_WBF_central_jet_veto"] 
        = bookHistogram1D("H_jet12_dphi_WBF_central_jet_veto",20,0.,PI);
      histos["H_jet12_pi-dphi_WBF_central_jet_veto"] 
        = bookHistogram1D("H_jet12_pi-dphi_WBF_central_jet_veto",30,0.,1.5);
      histos["jet1_pT_WBF_central_jet_veto"] 
        = bookHistogram1D("jet1_pT_WBF_central_jet_veto",60,0.,300.);
      histos["jet2_pT_WBF_central_jet_veto"] 
        = bookHistogram1D("jet2_pT_WBF_central_jet_veto",60,0.,300.);
      histos["jet3_pT_WBF_central_jet_veto"] 
        = bookHistogram1D("jet3_pT_WBF_central_jet_veto",60,0.,300.);
      histos["jet1_y_WBF_central_jet_veto"] 
        = bookHistogram1D("jet1_y_WBF_central_jet_veto",20,-5.,5.);
      histos["jet2_y_WBF_central_jet_veto"] 
        = bookHistogram1D("jet2_y_WBF_central_jet_veto",20,-5.,5.);
      histos["jet3_y_WBF_central_jet_veto"] 
        = bookHistogram1D("jet3_y_WBF_central_jet_veto",20,-5.,5.);
      histos["jet3_ystar_WBF_central_jet_veto"]
        = bookHistogram1D("jet3_ystar_WBF_central_jet_veto",20,-5.,5.);
      histos["jet12_dy_WBF_central_jet_veto"] 
        = bookHistogram1D("jet12_dy_WBF_central_jet_veto",20,0.,10.);
      histos["jet12_dphi_WBF_central_jet_veto"] 
        = bookHistogram1D("jet12_dphi_WBF_central_jet_veto",20,0.,PI);
      histos["jet12_mass_WBF_central_jet_veto"] 
        = bookHistogram1D("jet12_mass_WBF_central_jet_veto",25,0.,1000.);
      histos["Hjj_pT_WBF_central_jet_veto"] 
        = bookHistogram1D("Hjj_pT_WBF_central_jet_veto",60,0.,300.);
      histos["Hjj_etastar_WBF_central_jet_veto"] 
        = bookHistogram1D("Hjj_etastar_WBF_central_jet_veto",20,0.,10.);

      histos["H_pT_WBF_jet_veto"] 
        = bookHistogram1D("H_pT_WBF_jet_veto", 20, 0.0, 100.0);
      histos["H_y_WBF_jet_veto"]
        = bookHistogram1D("H_y_WBF_jet_veto",20,-5.,5.);
      histos["H_jet12_dphi_WBF_jet_veto"] 
        = bookHistogram1D("H_jet12_dphi_WBF_jet_veto",20,0.,PI);
      histos["H_jet12_pi-dphi_WBF_jet_veto"] 
        = bookHistogram1D("H_jet12_pi-dphi_WBF_jet_veto",30,0.,1.5);
      histos["jet1_pT_WBF_jet_veto"] 
        = bookHistogram1D("jet1_pT_WBF_jet_veto",60,0.,300.);
      histos["jet2_pT_WBF_jet_veto"] 
        = bookHistogram1D("jet2_pT_WBF_jet_veto",60,0.,300.);
      histos["jet3_pT_WBF_jet_veto"] 
        = bookHistogram1D("jet3_pT_WBF_jet_veto",60,0.,300.);
      histos["jet1_y_WBF_jet_veto"] 
        = bookHistogram1D("jet1_y_WBF_jet_veto",20,-5.,5.);
      histos["jet2_y_WBF_jet_veto"] 
        = bookHistogram1D("jet2_y_WBF_jet_veto",20,-5.,5.);
      histos["jet3_y_WBF_jet_veto"] 
        = bookHistogram1D("jet3_y_WBF_jet_veto",20,-5.,5.);
      histos["jet3_ystar_WBF_jet_veto"]
        = bookHistogram1D("jet3_ystar_WBF_jet_veto",20,-5.,5.);
      histos["jet12_dy_WBF_jet_veto"] 
        = bookHistogram1D("jet12_dy_WBF_jet_veto",20,0.,10.);
      histos["jet12_dphi_WBF_jet_veto"] 
        = bookHistogram1D("jet12_dphi_WBF_jet_veto",20,0.,PI);
      histos["jet12_mass_WBF_jet_veto"] 
        = bookHistogram1D("jet12_mass_WBF_jet_veto",25,0.,1000.);
      histos["Hjj_pT_WBF_jet_veto"] 
        = bookHistogram1D("Hjj_pT_WBF_jet_veto",60,0.,300.);
      histos["Hjj_etastar_WBF_jet_veto"] 
        = bookHistogram1D("Hjj_etastar_WBF_jet_veto",20,0.,10.);

      histos["cutflow"] = bookHistogram1D("cutflow",6,-0.5,5.5);


      jetvetohistos["jet_veto_efficiency_0"]
        = bookHistogram1D("jet_veto_efficiency_0",logBinEdges(300,1.,1000.));
      for (size_t i(0);i<_jpTs.size();++i) {
        std::string jc("jet_veto_efficiency_1_j"+_jpTs[i].second);
        jetvetohistos[jc] = bookHistogram1D(jc,logBinEdges(300,1.,1000.));
      }
      jetvetohistos["jet_veto_efficiency_2"]
        = bookHistogram1D("jet_veto_efficiency_2",logBinEdges(300,1.,1000.));
      jetvetohistos["jet_veto_efficiency_2_central"]
        = bookHistogram1D("jet_veto_efficiency_2_central",
                          logBinEdges(300,1.,1000.));
      jetvetohistos["jet_veto_efficiency_2_WBF"]
        = bookHistogram1D("jet_veto_efficiency_2_WBF",
                          logBinEdges(300,1.,1000.));
      jetvetohistos["jet_veto_efficiency_2_WBF_central"]
        = bookHistogram1D("jet_veto_efficiency_2_WBF_certral",
                          logBinEdges(300,1.,1000.));
      jetvetohistos["jet_veto_cross_sections"]
        = bookHistogram1D("jet_veto_cross_sections",
                          _jpTs.size()+5,-0.5,_jpTs.size()+4.5);
    }

    /// Do the analysis
    void analyze(const Event & e) {
      const double weight = e.weight();
      const ParticleVector& photons =
        applyProjection<IdentifiedFinalState>(e, "Photons").particlesByPt();
      histos["N_photons"]->fill(photons.size(),weight);
      // require at least two photons
      if (photons.size() < 2) vetoEvent;
      // photon isolation
      ParticleVector fs = applyProjection<FinalState>(e, "FS").particles();
      ParticleVector jetparts;
      std::vector<FourMomentum> phs;
      // energy in cone
      // add to jet-input everything but the isolated photons
      foreach (const Particle& p, fs)
	if (p.pdgId()!=PHOTON) jetparts.push_back(p);
      foreach (const Particle& ph, photons) {
        FourMomentum mom_in_Cone = -ph.momentum();
        foreach (const Particle& p, fs)
          if (deltaR(ph,p) < _phisodR) mom_in_Cone += p.momentum();
        if (mom_in_Cone.Et()<_phisoE) phs.push_back(ph.momentum());
        else                          jetparts.push_back(ph);
      }
      histos["N_photons_isolated"]->fill(phs.size(),weight);
      // at least two isolated photons
      // TODO: use subleading photons if leading ones are not isolated?
      if (phs.size() < 2) vetoEvent;
      // photon pT cuts
      if (phs[0].pT() < _ph1pT) vetoEvent;
      if (phs[1].pT() < _ph2pT) vetoEvent;
      // isolate photons from one-another
      if (deltaR(phs[0],phs[1]) < _phisodR) vetoEvent;
      // TODO: feed all but the two isolated photons back into jets?

      // define diphoton (Higgs) momentum
      FourMomentum hmom(phs[0]+phs[1]);

      // calculate jets
      FastJets jetpro(FastJets::ANTIKT, _jR);
      jetpro.calc(jetparts);
      Jets jets;
      // apply etamax and dRmin to photons
      foreach (const Jet& jetcand, jetpro.jetsByPt(_jpT*GeV)) {
        FourMomentum jmom = jetcand.momentum();
        if (fabs(jetcand.momentum().rapidity()) < _jeta && 
            deltaR(phs[0],jetcand.momentum()) > _jphdR &&
            deltaR(phs[1],jetcand.momentum()) > _jphdR) {
          jets.push_back(jetcand);
        }
      }

      // exclusive and inclusive jet-multis
      for (size_t i(0);i<_jpTs.size();++i) if (_jpT<=_jpTs[i].first) {
        size_t jetsXsize(0);
        for (size_t j(0);j<jets.size();++j)
          if (jets[j].momentum().pT()>_jpTs[i].first*GeV) ++jetsXsize;
	  else break;
        histos["jet_multi_j"+_jpTs[i].second+"_e"]->fill(jetsXsize,weight);
        for (size_t j(0);j<6;++j) if (jetsXsize>=j)
            histos["jet_multi_j"+_jpTs[i].second+"_i"]->fill(j,weight);
      }
      // exclusive observables
      for (size_t i(0);i<_jpTs.size();++i) if (_jpT<=_jpTs[i].first) {
        if (jets.size()==0 || jets[0].momentum().pT()<_jpTs[i].first) {
          histos["H_pT_nojet_j"+_jpTs[i].second]->fill(hmom.pT(),weight);
        }
      }
      // inclusive observables
      histos["H_pT"]->fill(hmom.pT(),weight);
      histos["H_pT_low"]->fill(hmom.pT(),weight);
      histos["H_pT_peak"]->fill(hmom.pT(),weight);
      histos["H_pT_long"]->fill(hmom.pT(),weight);
      histos["H_pT_log"]->fill(hmom.pT(),weight);
      histos["H_y"]->fill(hmom.rapidity(),weight);
      double cts(abs(sinh(phs[0].eta()-phs[1].eta()))/
                   sqrt(1.+sqr(hmom.pT()/hmom.mass()))
                 * 2.*phs[0].pT()*phs[1].pT()/sqr(hmom.mass()));
      histos["cos_theta_star"]->fill(cts,weight);

      // 1-jet observables
      if (jets.size()>0) {
        histos["jet1_pT"]->fill(jets[0].momentum().pT(),weight);
        histos["jet1_pT_log"]->fill(jets[0].momentum().pT(),weight);
        histos["jet1_y"]->fill(jets[0].momentum().rapidity(),weight);
        histos["H_jet1_dR"]->fill(deltaR(jets[0].momentum(),hmom),weight);
        histos["H_jet1_dy"]->fill(fabs(jets[0].momentum().rapidity()-
                                       hmom.rapidity()),weight);
        histos["H_jet1_dphi"]->fill(deltaPhi(jets[0].momentum(),hmom),weight);
        histos["Hj_pT"]->fill((hmom+jets[0].momentum()).pT(),weight);
      }
      // 2-jet observables
      if (jets.size()>1) {
        histos["jet2_pT"]->fill(jets[1].momentum().pT(),weight);
        histos["jet2_pT_log"]->fill(jets[1].momentum().pT(),weight);
        histos["jet2_y"]->fill(jets[1].momentum().rapidity(),weight);
        histos["jet12_dR"]->fill(deltaR(jets[0].momentum(),
                                        jets[1].momentum()),weight);
        histos["jet12_dy"]->fill(fabs(jets[0].momentum().rapidity()-
                                      jets[1].momentum().rapidity()),weight);
        histos["jet12_dphi"]->fill(deltaPhi(jets[0].momentum(),
                                            jets[1].momentum()),weight);
        histos["jet12_mass"]->fill((jets[0].momentum()+
                                    jets[1].momentum()).mass(),weight);
        FourMomentum j12(jets[0].momentum()+jets[1].momentum());
        double hjjdphi(deltaPhi(hmom,j12));
        histos["H_jet12_dphi"]->fill(hjjdphi,weight);
        histos["H_jet12_pi-dphi"]->fill(PI-hjjdphi,weight);
        histos["Hjj_pT"]->fill((hmom+jets[0].momentum()
                                    +jets[1].momentum()).pT(),weight);
        histos["Hjj_etastar"]->fill(abs(hmom.eta()
                                         -0.5*(jets[0].momentum().eta()
                                               +jets[1].momentum().eta())),
                                    weight);
      }
      // 3-jet observables
      if (jets.size()>2) {
        histos["jet3_pT"]->fill(jets[2].momentum().pT(),weight);
        histos["jet3_pT_log"]->fill(jets[2].momentum().pT(),weight);
        histos["jet3_y"]->fill(jets[2].momentum().rapidity(),weight);
        double ystar((jets[0].momentum().rapidity()+
                      jets[1].momentum().rapidity())/2.-
                      jets[2].momentum().rapidity());
        histos["jet3_ystar"]->fill(ystar,weight);
      }
      // 4-jet observables
      if (jets.size()>4) {
        histos["jet4_pT_log"]->fill(jets[3].momentum().pT(),weight);
        histos["jet4_y"]->fill(jets[3].momentum().rapidity(),weight);
      }
      // WBF observables w/o tagging cuts
      histos["cutflow"]->fill(0.,weight); // cutflow[0] -> incl.xsec
      if (jets.size()>1 && jets[1].momentum().pT()>_jpTtag*GeV) {
        histos["cutflow"]->fill(1.,weight); // cutflow[1] -> 2 tagjets
        double dyjj(fabs(jets[0].momentum().rapidity()-
                         jets[1].momentum().rapidity()));
        double massjj((jets[0].momentum()+jets[1].momentum()).mass());
        histos["H_pT_2jet"]->fill(hmom.pT(),weight);
	histos["H_y_2jet"]->fill(hmom.rapidity(),weight);
        histos["jet1_pT_2jet"]->fill(jets[0].momentum().pT(),weight);
        histos["jet1_y_2jet"]->fill(jets[0].momentum().rapidity(),weight);
        histos["jet2_pT_2jet"]->fill(jets[1].momentum().pT(),weight);
        histos["jet2_y_2jet"]->fill(jets[1].momentum().rapidity(),weight);
        histos["jet12_dy_2jet"]->fill(dyjj,weight);
        histos["jet12_dphi_2jet"]->fill(deltaPhi(jets[0].momentum(),
                                                 jets[1].momentum()),weight);
        histos["jet12_mass_2jet"]->fill(massjj,weight);
        FourMomentum j12(jets[0].momentum()+jets[1].momentum());
        histos["H_jet12_dphi_2jet"]->fill(deltaPhi(hmom,j12),weight);
        histos["H_jet12_pi-dphi_2jet"]->fill(PI-deltaPhi(hmom,j12),weight);
        histos["Hjj_pT_2jet"]->fill((hmom+jets[0].momentum()
                                         +jets[1].momentum()).pT(),weight);
        histos["Hjj_etastar_2jet"]->fill(abs(hmom.eta()
                                             -0.5*(jets[0].momentum().eta()
                                                   +jets[1].momentum().eta())),
                                         weight);
        histos["cos_theta_star_2jet"]->fill(cts,weight);
        if (jets.size()>2) {
          histos["jet3_pT_2jet"]->fill(jets[2].momentum().pT(),weight);
          histos["jet3_y_2jet"]->fill(jets[2].momentum().rapidity(),weight);
          double ystar((jets[0].momentum().rapidity()+
                        jets[1].momentum().rapidity())/2.-
                        jets[2].momentum().rapidity());
          histos["jet3_ystar_2jet"]->fill(ystar,weight);
        }
      }
      // WBF observables w/ tagging cuts
      if (jets.size()>1 && jets[1].momentum().pT()>_jpTtag*GeV) {
        double dyjj(fabs(jets[0].momentum().rapidity()-
                         jets[1].momentum().rapidity()));
        double massjj((jets[0].momentum()+jets[1].momentum()).mass());
        if (dyjj>_jjdy && massjj>_jjmass) {
          histos["cutflow"]->fill(2.,weight); // cutflow[2] -> dyjj and mjj
          double dyjj(fabs(jets[0].momentum().rapidity()-
                          jets[1].momentum().rapidity()));
          double massjj((jets[0].momentum()+jets[1].momentum()).mass());
          histos["H_pT_WBF"]->fill(hmom.pT(),weight);
	  histos["H_y_WBF"]->fill(hmom.rapidity(),weight);
          histos["jet1_pT_WBF"]->fill(jets[0].momentum().pT(),weight);
          histos["jet1_y_WBF"]->fill(jets[0].momentum().rapidity(),weight);
          histos["jet2_pT_WBF"]->fill(jets[1].momentum().pT(),weight);
          histos["jet2_y_WBF"]->fill(jets[1].momentum().rapidity(),weight);
          histos["jet12_dy_WBF"]->fill(dyjj,weight);
          histos["jet12_dphi_WBF"]->fill(deltaPhi(jets[0].momentum(),
                                                  jets[1].momentum()),weight);
          histos["jet12_mass_WBF"]->fill(massjj,weight);
          FourMomentum j12(jets[0].momentum()+jets[1].momentum());
          histos["H_jet12_dphi_WBF"]->fill(deltaPhi(hmom,j12),weight);
          histos["H_jet12_pi-dphi_WBF"]->fill(PI-deltaPhi(hmom,j12),weight);
          histos["Hjj_pT_WBF"]->fill((hmom+jets[0].momentum()
                                          +jets[1].momentum()).pT(),weight);
          histos["Hjj_etastar_WBF"]->fill(abs(hmom.eta()
                                              -0.5*(jets[0].momentum().eta()
                                                    +jets[1].momentum().eta())),
                                          weight);
	  histos["cos_theta_star_WBF"]->fill(cts,weight);
          if (jets.size()>2) {
            histos["cutflow"]->fill(3.,weight); // cutflow[3] -> 3rd jet
            histos["jet3_pT_WBF"]->fill(jets[2].momentum().pT(),weight);
            histos["jet3_y_WBF"]->fill(jets[2].momentum().rapidity(),weight);
            double ystar((jets[0].momentum().rapidity()+
                          jets[1].momentum().rapidity())/2.-
                          jets[2].momentum().rapidity());
            histos["jet3_ystar_WBF"]->fill(ystar,weight);
          }
        }
      }
      // WBF observables w/ tagging cuts and central jet veto
      if (jets.size()>1 && jets[1].momentum().pT()>_jpTtag*GeV) {
        double dyjj(fabs(jets[0].momentum().rapidity()-
                         jets[1].momentum().rapidity()));
        double massjj((jets[0].momentum()+jets[1].momentum()).mass());
	bool centraljetveto(false);
	if (jets.size()>2 && jets[2].momentum().pT()>_jpTtag*GeV) {
	  double y1(jets[0].momentum().rapidity());
	  double y2(jets[1].momentum().rapidity());
	  double y3(jets[2].momentum().rapidity());
	  if ((y1<y3 && y3<y2) || (y2<y3 && y3<y1)) centraljetveto=true;
	}
        if (dyjj>_jjdy && massjj>_jjmass && !centraljetveto) {
          histos["cutflow"]->fill(4.,weight); // cutflow[4] -> central jet veto
          double dyjj(fabs(jets[0].momentum().rapidity()-
                          jets[1].momentum().rapidity()));
          double massjj((jets[0].momentum()+jets[1].momentum()).mass());
          histos["H_pT_WBF_central_jet_veto"]->fill(hmom.pT(),weight);
          histos["H_y_WBF_central_jet_veto"]->fill(hmom.rapidity(),weight);
          histos["jet1_pT_WBF_central_jet_veto"]
            ->fill(jets[0].momentum().pT(),weight);
          histos["jet1_y_WBF_central_jet_veto"]
            ->fill(jets[0].momentum().rapidity(),weight);
          histos["jet2_pT_WBF_central_jet_veto"]
            ->fill(jets[1].momentum().pT(),weight);
          histos["jet2_y_WBF_central_jet_veto"]
            ->fill(jets[1].momentum().rapidity(),weight);
          histos["jet12_dy_WBF_central_jet_veto"]->fill(dyjj,weight);
          histos["jet12_dphi_WBF_central_jet_veto"]
            ->fill(deltaPhi(jets[0].momentum(),jets[1].momentum()),weight);
          histos["jet12_mass_WBF_central_jet_veto"]->fill(massjj,weight);
          FourMomentum j12(jets[0].momentum()+jets[1].momentum());
          histos["H_jet12_dphi_WBF_central_jet_veto"]
            ->fill(deltaPhi(hmom,j12),weight);
          histos["H_jet12_pi-dphi_WBF_central_jet_veto"]
            ->fill(PI-deltaPhi(hmom,j12),weight);
          histos["Hjj_pT_WBF_central_jet_veto"]
            ->fill((hmom+jets[0].momentum()+jets[1].momentum()).pT(),weight);
          histos["Hjj_etastar_WBF_central_jet_veto"]
            ->fill(abs(hmom.eta()-0.5*(jets[0].momentum().eta()
                                       +jets[1].momentum().eta())),weight);
	  histos["cos_theta_star_WBF_central_jet_veto"]->fill(cts,weight);
          if (jets.size()>2) {
            histos["jet3_pT_WBF_central_jet_veto"]
              ->fill(jets[2].momentum().pT(),weight);
            histos["jet3_y_WBF_central_jet_veto"]
              ->fill(jets[2].momentum().rapidity(),weight);
            double ystar((jets[0].momentum().rapidity()+
                          jets[1].momentum().rapidity())/2.-
                          jets[2].momentum().rapidity());
            histos["jet3_ystar_WBF_central_jet_veto"]->fill(ystar,weight);
          }
        }
      }
      // WBF observables w/ tagging cuts and jet veto
      if (jets.size()>1 && jets[1].momentum().pT()>_jpTtag*GeV) {
        double dyjj(fabs(jets[0].momentum().rapidity()-
                         jets[1].momentum().rapidity()));
        double massjj((jets[0].momentum()+jets[1].momentum()).mass());
	bool jetveto(false);
        if (jets.size()>2 && jets[2].momentum().pT()>_jpTtag*GeV) jetveto=true;
        if (dyjj>_jjdy && massjj>_jjmass && !jetveto) {
          histos["cutflow"]->fill(5.,weight); // cutflow[5] -> jet veto
          double dyjj(fabs(jets[0].momentum().rapidity()-
                          jets[1].momentum().rapidity()));
          double massjj((jets[0].momentum()+jets[1].momentum()).mass());
          histos["H_pT_WBF_jet_veto"]->fill(hmom.pT(),weight);
          histos["H_y_WBF_jet_veto"]->fill(hmom.rapidity(),weight);
          histos["jet1_pT_WBF_jet_veto"]->fill(jets[0].momentum().pT(),weight);
          histos["jet1_y_WBF_jet_veto"]
            ->fill(jets[0].momentum().rapidity(),weight);
          histos["jet2_pT_WBF_jet_veto"]
            ->fill(jets[1].momentum().pT(),weight);
          histos["jet2_y_WBF_jet_veto"]
            ->fill(jets[1].momentum().rapidity(),weight);
          histos["jet12_dy_WBF_jet_veto"]->fill(dyjj,weight);
          histos["jet12_dphi_WBF_jet_veto"]
            ->fill(deltaPhi(jets[0].momentum(),jets[1].momentum()),weight);
          histos["jet12_mass_WBF_jet_veto"]->fill(massjj,weight);
          FourMomentum j12(jets[0].momentum()+jets[1].momentum());
          histos["H_jet12_dphi_WBF_jet_veto"]->fill(deltaPhi(hmom,j12),weight);
          histos["H_jet12_pi-dphi_WBF_jet_veto"]
            ->fill(PI-deltaPhi(hmom,j12),weight);
          histos["Hjj_pT_WBF_jet_veto"]
            ->fill((hmom+jets[0].momentum()+jets[1].momentum()).pT(),weight);
          histos["Hjj_etastar_WBF_jet_veto"]
            ->fill(abs(hmom.eta()-0.5*(jets[0].momentum().eta()
                                       +jets[1].momentum().eta())),weight);
	  histos["cos_theta_star_WBF_jet_veto"]->fill(cts,weight);
          if (jets.size()>2) {
            histos["jet3_pT_WBF_jet_veto"]
              ->fill(jets[2].momentum().pT(),weight);
            histos["jet3_y_WBF_jet_veto"]
              ->fill(jets[2].momentum().rapidity(),weight);
            double ystar((jets[0].momentum().rapidity()+
                          jets[1].momentum().rapidity())/2.-
                          jets[2].momentum().rapidity());
            histos["jet3_ystar_WBF_jet_veto"]->fill(ystar,weight);
          }
        }
      }

      // jet veto efficiencies
      // 0-jet bin
      const Jets& alljets(jetpro.jetsByPt(0.*GeV));
      double xs0pT1 = alljets.size()>0?alljets[0].momentum().pT():0.;
      fillVetoEfficiency(xs0pT1,"jet_veto_efficiency_0",0,weight);
      // 1-jet bin
      if (alljets.size()>0) {
        for (size_t i(0);i<_jpTs.size();++i) {
          if (alljets[0].momentum().pT()<_jpTs[i].first*GeV) break;
          std::string jc("jet_veto_efficiency_1_j"+_jpTs[i].second);
          double xs1pT2 = alljets.size()>1?alljets[1].momentum().pT():0.;
          fillVetoEfficiency(xs1pT2,jc,i+1,weight);
        }
      }
      // 2-jet bin
      if (alljets.size()>1 && alljets[1].momentum().pT()>_jpTtag*GeV) {
        double y1 = alljets[0].momentum().rapidity();
        double y2 = alljets[1].momentum().rapidity();
        double xs2pT3 = alljets.size()>2?alljets[2].momentum().pT():0.;
        fillVetoEfficiency(xs2pT3,"jet_veto_efficiency_2",
                           _jpTs.size()+1,weight);
        double xs2pT3c = 0.;
        if (alljets.size()>2 &&
            ((alljets[2].momentum().rapidity() < y1 &&
              alljets[2].momentum().rapidity() > y2) ||
             (alljets[2].momentum().rapidity() < y2 &&
              alljets[2].momentum().rapidity() > y1)))
          xs2pT3c = alljets[2].momentum().pT();
        fillVetoEfficiency(xs2pT3c,"jet_veto_efficiency_2_central",
                           _jpTs.size()+2,weight);
        double dyjj(fabs(y1-y2));
        double massjj((alljets[0].momentum()+alljets[1].momentum()).mass());
        if (dyjj>_jjdy && massjj>_jjmass) {
          double xs2pT3WBF = alljets.size()>2?alljets[2].momentum().pT():0.;
          fillVetoEfficiency(xs2pT3WBF,"jet_veto_efficiency_2_WBF",
                             _jpTs.size()+3,weight);
          double xs2pT3WBFc = 0.;
          if (alljets.size()>2 &&
              ((alljets[2].momentum().rapidity() < y1 &&
                alljets[2].momentum().rapidity() > y2) ||
               (alljets[2].momentum().rapidity() < y2 &&
                alljets[2].momentum().rapidity() > y1)))
            xs2pT3WBFc = alljets[2].momentum().pT();
          fillVetoEfficiency(xs2pT3WBFc,"jet_veto_efficiency_2_WBF_central",
                             _jpTs.size()+4,weight);
        }
      }
    }

    void fillVetoEfficiency(double pT, string id, int xsindex, double weight) {
      AIDA::IHistogram1D * jveh = jetvetohistos[id];
      int nbins = jveh->axis().bins();
      int index = jveh->coordToIndex(pT);
      if      (index==jveh->axis().OVERFLOW_BIN)
        index = jveh->axis().bins();
      else if (index==jveh->axis().UNDERFLOW_BIN)
        index = 0;
      for (int i=index;i<nbins;++i) {
        jveh->fill(jveh->binMean(i),weight*jveh->axis().binWidth(i));
      }
      jetvetohistos["jet_veto_cross_sections"]->fill(xsindex,weight);
    }

    /// Finalize
    void finalize() {
      double scalefactor(crossSection()/sumOfWeights());
      for (std::map<std::string,AIDA::IHistogram1D *>::iterator 
             hit=histos.begin(); hit!=histos.end();hit++) 
        scale(hit->second,scalefactor);
      for (std::map<std::string,AIDA::IHistogram1D *>::iterator 
             hit=jetvetohistos.begin(); hit!=jetvetohistos.end();hit++) 
        scale(hit->second,scalefactor);
    }
  };



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_HJETS_PHOTONPHOTON_CUTS);
}
