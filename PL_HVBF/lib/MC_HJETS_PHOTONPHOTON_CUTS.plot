# BEGIN PLOT .*
RatioPlotSameStyle=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/N_photons
Title=Number of photons in $|\eta|<$ 2.37 and $p_\perp>$ 31.25 GeV
XLabel=$N_\gamma$
YLabel=d$\sigma$/d$N_\gamma$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/N_photons_isolated
Title=Number of isolated photons in $|\eta|<$ 2.37 and $p_\perp>$ 31.25 GeV
XLabel=$N_\gamma$
YLabel=d$\sigma$/d$N_\gamma$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/cos_theta_star
Title=$|\cos\theta^*|$
XLabel=$|\cos\theta^*|$
YLabel=d$\sigma$/d$|\cos\theta^*|$ [pb]
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/cos_theta_star_2jet
Title=$|\cos\theta^*|$ ($n_\text{jet}\ge 2$)
XLabel=$|\cos\theta^*|$
YLabel=d$\sigma$/d$|\cos\theta^*|$ [pb]
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/cos_theta_star_WBF
Title=$|\cos\theta^*|$ (VBF cuts)
XLabel=$|\cos\theta^*|$
YLabel=d$\sigma$/d$|\cos\theta^*|$ [pb]
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/cos_theta_star_WBF_central_jet_veto
Title=$|\cos\theta^*|$ (VBF cuts, ctrl.\ jet veto)
XLabel=$|\cos\theta^*|$
YLabel=d$\sigma$/d$|\cos\theta^*|$ [pb]
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/cos_theta_star_WBF_jet_veto
Title=$|\cos\theta^*|$ (VBF cuts, 3rd jet veto)
XLabel=$|\cos\theta^*|$
YLabel=d$\sigma$/d$|\cos\theta^*|$ [pb]
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet1_dR
Title=Angular separation of Higgs boson and leading jet
XLabel=$\Delta R(H,j_1)$
YLabel=d$\sigma$/d$\Delta R$ [pb]
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet1_dphi
Title=Azimuthal separation of Higgs boson and leading jet
XLabel=$\Delta\phi(H,j_1)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet1_dy
Title=Rapidity separation of Higgs boson and leading jet
XLabel=$\Delta y(H,j_1)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hj_pT
Title=Transverse momentum of the $H\,j_1$ system
XLabel=$p_\perp(Hj_1)$
YLabel=d$\sigma$/d$p_\perp(Hj_1)$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT
Title=Higgs boson transverse momentum
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_2jet
Title=Higgs boson transverse momentum ($n_\text{jet}\ge 2$)
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_WBF
Title=Higgs boson transverse momentum (VBF cuts)
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_WBF_central_jet_veto
Title=Higgs boson transverse momentum (VBF cuts, ctrl.\ jet veto)
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_WBF_jet_veto
Title=Higgs boson transverse momentum (VBF cuts, 3rd jet veto)
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_y
Title=Higgs boson rapidity
XLabel=$y(H)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_y_2jet
Title=Higgs boson rapidity ($n_\text{jet}\ge 2$)
XLabel=$y(H)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_y_WBF
Title=Higgs boson rapidity (VBF cuts)
XLabel=$y(H)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_y_WBF_central_jet_veto
Title=Higgs boson rapidity (VBF cuts, ctrl.\ jet veto)
XLabel=$y(H)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_y_WBF_jet_veto
Title=Higgs boson rapidity (VBF cuts, 3rd jet veto)
XLabel=$y(H)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.4
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_log
Title=Higgs boson transverse momentum
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=1
LegendXPos=0.1
LegendYPos=0.4
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_long
Title=Higgs boson transverse momentum
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_low
Title=Higgs boson transverse momentum
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.2
LegendYPos=0.5
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_nojet
Title=Higgs boson transverse momentum ($n_\text{jet}=0$)
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_pT_peak
Title=Higgs boson transverse momentum
XLabel=$p_\perp(H)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.3
LegendYPos=0.5
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/cutflow
Title=Cutflow of VBF cuts
XLabel=cut level
YLabel=$\sigma$(cut level) [pb]
XCustomMajorTicks=0	incl.	1	2jet	2	VBF	3	3jet	4	ctrl.veto	5	jet veto
XMin=-0.5
XMax=5.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dR
Title=Angular separation of the two leading jets
XLabel=$\Delta R(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta R$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dphi
Title=Azumuthal separation of the two leading jets
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dphi_2jet
Title=Azumuthal separation of the two leading jets ($n_\text{jet}\ge 2$)
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dphi_WBF
Title=Azumuthal separation of the two leading jets (VBF cuts)
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dphi_WBF_central_jet_veto
Title=Azumuthal separation of the two leading jets (VBF cuts, ctrl. jet veto)
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dphi_WBF_jet_veto
Title=Azumuthal separation of the two leading jets (VBF cuts, 3rd jet veto)
XLabel=$\Delta\phi(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_dphi
Title=Azumuthal separation of the Higgs and the two leading jets
XLabel=$\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_dphi_2jet
Title=Azumuthal separation of the Higgs and the two leading jets ($n_\text{jet}\ge 2$)
XLabel=$\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_dphi_WBF
Title=Azumuthal separation of the Higgs and the two leading jets (VBF cuts)
XLabel=$\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_dphi_WBF_central_jet_veto
Title=Azumuthal separation of the Higgs and the two leading jets (VBF cuts, ctrl.\ jet veto)
XLabel=$\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_dphi_WBF_jet_veto
Title=Azumuthal separation of the Higgs and the two leading jets (VBF cuts, 3rd jet veto)
XLabel=$\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d$\Delta\phi$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_pi-dphi
Title=Azumuthal separation of the Higgs and the two leading jets
XLabel=$\pi-\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d(\pi-$\Delta\phi)$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_pi-dphi_2jet
Title=Azumuthal separation of the Higgs and the two leading jets ($n_\text{jet}\ge 2$)
XLabel=$\pi-\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d(\pi-$\Delta\phi)$ [pb]
LegendXPos=0.1
LogY=0
ShowZero=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_pi-dphi_WBF
Title=Azumuthal separation of the Higgs and the two leading jets (VBF cuts)
XLabel=$\pi-\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d(\pi-$\Delta\phi)$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_pi-dphi_WBF_central_jet_veto
Title=Azumuthal separation of the Higgs and the two leading jets (VBF cuts, ctrl.\ jet veto)
XLabel=$\pi-\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d(\pi-$\Delta\phi)$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/H_jet12_pi-dphi_WBF_jet_veto
Title=Azumuthal separation of the Higgs and the two leading jets (VBF cuts, 3rd jet veto)
XLabel=$\pi-\Delta\phi(H,j_1j_2)$
YLabel=d$\sigma$/d(\pi-$\Delta\phi)$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dy
Title=Rapidity separation of the two leading jets
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dy_2jet
Title=Rapidity separation of the two leading jets ($n_\text{jet}\ge 2$)
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dy_WBF
Title=Rapidity separation of the two leading jets (VBF cuts)
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dy_WBF_central_jet_veto
Title=Rapidity separation of the two leading jets (VBF cuts, ctrl.\ jet veto)
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_dy_WBF_jet_veto
Title=Rapidity separation of the two leading jets (VBF cuts, 3rd jet veto)
XLabel=$\Delta y(j_1,j_2)$
YLabel=d$\sigma$/d$\Delta y$ [pb]
LegendXPos=0.1
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_mass
Title=Invariant mass of the two leading jets
XLabel=$m_{jj}$ [GeV]
YLabel=d$\sigma$/d$m_{jj}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_mass_2jet
Title=Invariant mass of the two leading jets ($n_\text{jet}\ge 2$)
XLabel=$m_{jj}$ [GeV]
YLabel=d$\sigma$/d$m_{jj}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_mass_WBF
Title=Invariant mass of the two leading jets (VBF cuts)
XLabel=$m_{jj}$ [GeV]
YLabel=d$\sigma$/d$m_{jj}$ [pb/GeV]
LegendXPos=0.1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_mass_WBF_central_jet_veto
Title=Invariant mass of the two leading jets (VBF cuts, ctrl.\ jet veto)
XLabel=$m_{jj}$ [GeV]
YLabel=d$\sigma$/d$m_{jj}$ [pb/GeV]
LegendXPos=0.1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet12_mass_WBF_jet_veto
Title=Invariant mass of the two leading jets (VBF cuts, 3rd jet veto)
XLabel=$m_{jj}$ [GeV]
YLabel=d$\sigma$/d$m_{jj}$ [pb/GeV]
LegendXPos=0.1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_pT
Title=Transverse momentum of leading jet
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_pT_log
Title=Transverse momentum of leading jet
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_pT_2jet
Title=Transverse momentum of leading jet ($n_\text{jet}\ge 2$)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.1
LegendYPos=0.5
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_pT_WBF
Title=Transverse momentum of leading jet VBF cuts)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.1
LegendYPos=0.5
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_pT_WBF_central_jet_veto
Title=Transverse momentum of leading jet VBF cuts, ctrl.\ jet veto)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.1
LegendYPos=0.5
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_pT_WBF_jet_veto
Title=Transverse momentum of leading jet VBF cuts, 3rd jet veto)
XLabel=$p_\perp(j_1)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LegendXPos=0.1
LegendYPos=0.5
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_y
Title=Rapidity of leading jet
XLabel=$y(j_1)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_y_2jet
Title=Rapidity of leading jet ($n_\text{jet}\ge 2$)
XLabel=$y(j_1)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_y_WBF
Title=Rapidity of leading jet (VBF cuts)
XLabel=$y(j_1)$
YLabel=d$\sigma$/d$y$ [pb]
LogY=0
LegendXPos=0.3
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_y_WBF_central_jet_veto
Title=Rapidity of leading jet (VBF cuts, ctrl.\ jet veto)
XLabel=$y(j_1)$
YLabel=d$\sigma$/d$y$ [pb]
LogY=0
LegendXPos=0.3
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet1_y_WBF_jet_veto
Title=Rapidity of leading jet (VBF cuts, 3rd jet veto)
XLabel=$y(j_1)$
YLabel=d$\sigma$/d$y$ [pb]
LogY=0
LegendXPos=0.3
LegendYPos=0.2
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_pT
Title=Transverse momentum of subleading jet
XLabel=$p_\perp(j_2)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_pT_log
Title=Transverse momentum of subleading jet
XLabel=$p_\perp(j_2)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_pT_2jet
Title=Transverse momentum of subleading jet ($n_\text{jet}\ge 2$)
XLabel=$p_\perp(j_2)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_pT_WBF
Title=Transverse momentum of subleading jet (VBF cuts)
XLabel=$p_\perp(j_2)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_pT_WBF_central_jet_veto
Title=Transverse momentum of subleading jet (VBF cuts, ctrl.\ jet veto)
XLabel=$p_\perp(j_2)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_pT_WBF_jet_veto
Title=Transverse momentum of subleading jet (VBF cuts, 3rd jet veto)
XLabel=$p_\perp(j_2)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_y
Title=Rapidity of subleading jet
XLabel=$y(j_2)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_y_2jet
Title=Rapidity of subleading jet ($n_\text{jet}\ge 2$)
XLabel=$y(j_2)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_y_WBF
Title=Rapidity of subleading jet (VBF cuts)
XLabel=$y(j_2)$
YLabel=d$\sigma$/d$y$ [pb]
LogY=0
LegendXPos=0.3
LegendYPos=0.9
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_y_WBF_central_jet_veto
Title=Rapidity of subleading jet (VBF cuts, ctrl.\ jet veto)
XLabel=$y(j_2)$
YLabel=d$\sigma$/d$y$ [pb]
LogY=0
LegendXPos=0.3
LegendYPos=0.9
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet2_y_WBF_jet_veto
Title=Rapidity of subleading jet (VBF cuts, 3rd jet veto)
XLabel=$y(j_2)$
YLabel=d$\sigma$/d$y$ [pb]
LogY=0
LegendXPos=0.3
LegendYPos=0.9
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_pT
Title=Transverse momentum of 3rd jet
XLabel=$p_\perp(j_3)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_pT_log
Title=Transverse momentum of 3rd jet
XLabel=$p_\perp(j_3)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_pT_2jet
Title=Transverse momentum of 3rd jet ($n_\text{jet}\ge 2$)
XLabel=$p_\perp(j_3)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_pT_WBF
Title=Transverse momentum of 3rd jet (VBF cuts)
XLabel=$p_\perp(j_3)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_pT_WBF_central_jet_veto
Title=Transverse momentum of 3rd jet (VBF cuts, ctrl.\ jet veto)
XLabel=$p_\perp(j_3)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_pT_WBF_jet_veto
Title=Transverse momentum of 3rd jet (VBF cuts, 3rd jet veto)
XLabel=$p_\perp(j_3)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_y
Title=Rapidity of 3rd jet
XLabel=$y(j_3)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_y_2jet
Title=Rapidity of 3rd jet ($n_\text{jet}\ge 2$)
XLabel=$y(j_3)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_y_WBF
Title=Rapidity of 3rd jet (VBF cuts)
XLabel=$y(j_3)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_y_WBF_central_jet_veto
Title=Rapidity of 3rd jet (VBF cuts, ctrl.\ jet veto)
XLabel=$y(j_3)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_y_WBF_jet_veto
Title=Rapidity of 3rd jet (VBF cuts, 3rd jet veto)
XLabel=$y(j_3)$
YLabel=d$\sigma$/d$y$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_ystar_2jet
Title=$y^*$ of 3rd jet ($n_\text{jet}\ge 2$)
XLabel=$y^*$
YLabel=d$\sigma$/d$y^*$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_ystar_WBF
Title=$y^*$ of 3rd jet (VBF cuts)
XLabel=$y^*$
YLabel=d$\sigma$/d$y^*$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_ystar_WBF_central_jet_veto
Title=$y^*$ of 3rd jet (VBF cuts, ctrl.\ jet veto)
XLabel=$y^*$
YLabel=d$\sigma$/d$y^*$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet3_ystar_WBF_jet_veto
Title=$y^*$ of 3rd jet (VBF cuts, 3rd jet veto)
XLabel=$y^*$
YLabel=d$\sigma$/d$y^*$ [pb]
LegendXPos=0.3
LegendYPos=0.4
LogY=0
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet4_pT
Title=Transverse momentum of 4th jet
XLabel=$p_\perp(j_4)$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet4_y
Title=Rapidity of 4th jet
XLabel=$y(j_4)$
YLabel=d$\sigma$/d$y$ [pb]
LogY=0
LegendXPos=0.3
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_etastar
Title=$\eta_H^*$
XLabel=$\eta_H^*$
YLabel=d$\sigma$/d$\eta_H^*$ [pb]
LogY=0
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_etastar_2jet
Title=$\eta_H^*$ ($n_\text{jet}\ge 2$)
XLabel=$\eta_H^*$
YLabel=d$\sigma$/d$\eta_H^*$ [pb]
LogY=0
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_etastar_WBF
Title=$\eta_H^*$ (VBF cuts)
XLabel=$\eta_H^*$
YLabel=d$\sigma$/d$\eta_H^*$ [pb]
LogY=0
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_etastar_WBF_central_jet_veto
Title=$\eta_H^*$ (VBF cuts, ctrl.\ jet veto)
XLabel=$\eta_H^*$
YLabel=d$\sigma$/d$\eta_H^*$ [pb]
LogY=0
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_etastar_WBF_jet_veto
Title=$\eta_H^*$ (VBF cuts, 3rd jet veto)
XLabel=$\eta_H^*$
YLabel=d$\sigma$/d$\eta_H^*$ [pb]
LogY=0
LegendXPos=0.1
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_pT
Title=Transverse momentum of the $H\,j_1\,j_2$ system
XLabel=$p_\perp(Hj_1j_2)$
YLabel=d$\sigma$/d$p_\perp(Hj_1j_2)$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_pT_2jet
Title=Transverse momentum of the $H\,j_1\,j_2$ system ($n_\text{jet}\ge 2$)
XLabel=$p_\perp(Hj_1j_2)$
YLabel=d$\sigma$/d$p_\perp(Hj_1j_2)$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_pT_WBF
Title=Transverse momentum of the $H\,j_1\,j_2$ system (VBF cuts)
XLabel=$p_\perp(Hj_1j_2)$
YLabel=d$\sigma$/d$p_\perp(Hj_1j_2)$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_pT_WBF_central_jet_veto
Title=Transverse momentum of the $H\,j_1\,j_2$ system (VBF cuts, ctrl.\ jet veto)
XLabel=$p_\perp(Hj_1j_2)$
YLabel=d$\sigma$/d$p_\perp(Hj_1j_2)$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/Hjj_pT_WBF_jet_veto
Title=Transverse momentum of the $H\,j_1\,j_2$ system (VBF cuts, 3rd jet veto)
XLabel=$p_\perp(Hj_1j_2)$
YLabel=d$\sigma$/d$p_\perp(Hj_1j_2)$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_multi_j30_incl
Title=Inclusive jet multiplicity at $p_\perp^\text{min}=30$ GeV
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_multi_j35_incl
Title=Inclusive jet multiplicity at $p_\perp^\text{min}=35$ GeV
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_multi_j40_incl
Title=Inclusive jet multiplicity at $p_\perp^\text{min}=40$ GeV
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_multi_j50_incl
Title=Inclusive jet multiplicity at $p_\perp^\text{min}=50$ GeV
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_multi_j30_excl
Title=Exclusive jet multiplicity at $p_\perp^\text{min}=30$ GeV
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_multi_j35_excl
Title=Exclusive jet multiplicity at $p_\perp^\text{min}=35$ GeV
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_multi_j40_excl
Title=Exclusive jet multiplicity at $p_\perp^\text{min}=40$ GeV
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_multi_j50_excl
Title=Exclusive jet multiplicity at $p_\perp^\text{min}=50$ GeV
XLabel=$N_\text{jet}$
YLabel=$\sigma(n_\text{jet}\ge N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_0
Title=Jet veto efficiency on inclusive Higgs production
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_0(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_1_j15
Title=Jet veto efficiency on $pp\to h+$jet production ($p_\perp^{j_1}>15\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_1(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_1_j20
Title=Jet veto efficiency on $pp\to h+$jet production ($p_\perp^{j_1}>20\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_1(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_1_j25
Title=Jet veto efficiency on $pp\to h+$jet production ($p_\perp^{j_1}>25\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_1(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_1_j30
Title=Jet veto efficiency on $pp\to h+$jet production ($p_\perp^{j_1}>30\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_1(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_1_j35
Title=Jet veto efficiency on $pp\to h+$jet production ($p_\perp^{j_1}>35\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_1(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_1_j40
Title=Jet veto efficiency on $pp\to h+$jet production ($p_\perp^{j_1}>40\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_1(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_1_j50
Title=Jet veto efficiency on $pp\to h+$jet production ($p_\perp^{j_1}>50\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_1(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_2
Title=Jet veto efficiency on $pp\to h+$2 jet production ($p_\perp^{j_1},p_\perp^{j_2}>25\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_1(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_2_central
Title=Central jet veto efficiency on $pp\to h+$2 jet production ($p_\perp^{j_1},p_\perp^{j_2}>25\text{ GeV}$)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_2(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_2_WBF
Title=Jet veto efficiency on $pp\to h+$2 jet production ($p_\perp^{j_1},p_\perp^{j_2}>25\text{ GeV}$) (WBF cuts)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_2(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_efficiency_2_WBF_central
Title=Central jet veto efficiency on $pp\to h+$2 jet production ($p_\perp^{j_1},p_\perp^{j_2}>25\text{ GeV}$) (WBF cuts)
XLabel=$p_\perp^\text{veto}$
YLabel=$\epsilon_2(p_\perp^\text{veto})$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETS_PHOTONPHOTON_CUTS/jet_veto_cross_sections
Title=Inclusive cross sections for jet veto efficiency denominators
XLabel=N
YLabel=$\sigma_N^\text{incl}$ [pb]
# END PLOT

# BEGIN PLOT .*
RatioPlotSameStyle=1
# END PLOT

