# BEGIN PLOT /MC_WH_ATLAS/pt1st
Title=$p_\perp$ of hardest isolated lepton (after pre-selection)
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=0
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/pt2nd
Title=$p_\perp$ of second hardest isolated lepton (after pre-selection)
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=0
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/pt3rd
Title=$p_\perp$ of softest isolated lepton (after pre-selection)
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=0
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/mass01
Title=invariant mass of opposite sign lepton pair (l0,l1) (after pre-selection)
XLabel=$m_{01}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta m_{01}$ [pb/GeV]
LogY=0
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/mass02
Title=invariant mass of opposite sign lepton pair (l0,l2) (after pre-selection)
XLabel=$m_{02}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta m_{02}$ [pb/GeV]
LogY=0
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/Njets
Title=number of jets with $p_\perp > 25$ [GeV] (after pre-selection)
XLabel=$N_\mathrm{jets}$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\, N_\mathrm{jets}$ [GeV]
LogY=1
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/Nbjets
Title=number of b-jets with $p_\perp > 25$ [GeV] (after pre-selection)
XLabel=$N_\mathrm{b-jets}$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\, N_\mathrm{b-jets}$ [GeV]
LogY=1
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/etmiss
Title=missing $E_\perp$ (after pre-selection)
XLabel=$E_{\perp,\,\mathrm{rel}}^\mathrm{miss}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,E_{\perp,\,\mathrm{rel}}^\mathrm{miss}$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/etmiss_Zrich
Title=missing $E_\perp$ in Z-enriched sample (after pre-selection)
XLabel=$E_{\perp,\,\mathrm{rel}}^\mathrm{miss}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,E_{\perp,\,\mathrm{rel}}^\mathrm{miss}$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/etmiss_Zdepl
Title=missing $E_\perp$ in Z-depleted sample (after pre-selection)
XLabel=$E_{\perp,\,\mathrm{rel}}^\mathrm{miss}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,E_{\perp,\,\mathrm{rel}}^\mathrm{miss}$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/deltaR01
Title=$\Delta R$ between closer opposite-sign lepton pair (after pre-selection)
XLabel=$\Delta R_{01}$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta R_{01}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/deltaR01_Zrich
Title=$\Delta R$ between closer opposite-sign lepton pair in Z-enriched sample (after pre-selection)
XLabel=$\Delta R_{01}$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta R_{01}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/deltaR01
Title=$\Delta R$ between closer opposite-sign lepton pair in Z-depleted sample (after pre-selection)
XLabel=$\Delta R_{01}$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta R_{01}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/cuts_Zrich
Title=cut efficiency in Z-enriched sample
XLabel=cut
YLabel=cut efficiency
LogY=1
Scale=1445.2
# END PLOT

# BEGIN PLOT /MC_WH_ATLAS/cuts_Zdepl
Title=cut efficiency in Z-depleted sample
XLabel=cut
YLabel=cut efficiency
LogY=1
Scale=1445.2
# END PLOT


