#include "Rivet/Analysis.hh"
#include "Rivet/Math/LorentzTrans.hh"
#include "Rivet/Math/Constants.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Projections/Beam.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/LeptonClusters.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/AnalysisLoader.hh"
#include "Rivet/RivetAIDA.hh"
#include <map>

namespace Rivet {
    class ptsort {
  public:
    bool operator()(const Particle& a, const Particle & b){
      return (a.momentum().pT()>b.momentum().pT());
    }
  };  
  
  class ATLAStrileptonVR2 : public Analysis {
  private:
    double _mupt_both, _mupt_first, _mupt_second;
    double _ept_both, _ept_first, _ept_second;
    double _muept_e1, _muept_e2, _muept_mu1, _muept_mu2;
    double _mupt, _ept, _mueta, _eeta;
    double _jR, _etj, _etaj;
    double _dr_ll, _dr_lj, _dr_jl, _isoR_lj;
    double _e_ptmax, _mu_ptmax, _e_trackpt, _mu_trackpt;
    double _mt, _sigMT, _mW, _sigMW;
    double _missEt, _leta, _lpt;
 
    std::map<string,AIDA::IHistogram1D*> histos;
    
    void inithistos() {
      histos["jet_mult"]     = bookHistogram1D("jet_mult", 11, -0.5, 10.5);
      histos["bjet_mult"]    = bookHistogram1D("bjet_mult", 11, -0.5, 10.5);
      histos["HTjets"]       = bookHistogram1D("HTjets", logspace(50, 10.0, 500.0));
      histos["HTobjs"]       = bookHistogram1D("HTobjs", logspace(50, 10.0, 1500.0));
      histos["HTvis"]        = bookHistogram1D("HTvis", logspace(50, 10.0, 1500.0));
      histos["HTlep"]        = bookHistogram1D("HTlep", logspace(50, 10.0, 1500.0));
      histos["mass_ll_ss"]   = bookHistogram1D("mass_ll_ss", 50, 0.0, 500.0);
      histos["mass_ll_os_1"] = bookHistogram1D("mass_ll_os_1", 50, 0.0, 1000.0);
      histos["mass_ll_os_2"] = bookHistogram1D("mass_ll_os_2", 50, 0.0, 200.0);
      histos["mass_lll"]     = bookHistogram1D("mass_lll", 50, 0.0, 1000.0);
      histos["pT_l1"]        = bookHistogram1D("pT_l1", logspace(50, 10.0, 500.0));
      histos["pT_l2"]        = bookHistogram1D("pT_l2", logspace(50, 10.0, 500.0));
      histos["pT_l3"]        = bookHistogram1D("pT_l3", logspace(50, 10.0, 500.0));
      histos["pT_los"]       = bookHistogram1D("pT_los", logspace(50, 10.0, 500.0));
      histos["pT_lp1"]       = bookHistogram1D("pT_lp1", logspace(50, 10.0, 500.0));
      histos["pT_lm1"]       = bookHistogram1D("pT_lm1", logspace(50, 10.0, 500.0));
      histos["pT_lll"]       = bookHistogram1D("pT_lll", logspace(50, 10.0, 500.0));
      histos["eta_l1"]       = bookHistogram1D("eta_l1", 10, -_leta, _leta);
      histos["eta_l2"]       = bookHistogram1D("eta_l2", 10, -_leta, _leta); 
      histos["eta_l3"]       = bookHistogram1D("eta_l3", 10, -_leta, _leta); 
      histos["eta_los"]      = bookHistogram1D("eta_los", 10, -_leta, _leta); 
      histos["eta_lp1"]      = bookHistogram1D("eta_lp1", 10, -_leta, _leta); 
      histos["eta_lm1"]      = bookHistogram1D("eta_lm1", 10, -_leta, _leta); 
      histos["eta_lll"]      = bookHistogram1D("eta_lll", 10, -_leta, _leta); 
      histos["missET"]       = bookHistogram1D("missET", 10, 40.0, 240.0);
      histos["pT_lj"]        = bookHistogram1D("pT_lj", logspace(50, 10.0, 200.0));
      histos["eta_lj"]       = bookHistogram1D("eta_lj", 20, -_etaj, _etaj); 
      histos["dphi_l1j"]     = bookHistogram1D("dphi_l1j", 20, 0.0, M_PI); 
      histos["dR_l1j"]       = bookHistogram1D("dR_l1j", 20, 0.0, 6.0); 
      histos["deta_l1j"]     = bookHistogram1D("deta_l1j", 20, 0.0, 6.0); 
      histos["dphi_l1l2"]    = bookHistogram1D("dphi_l1l2", 20, 0.0, M_PI); 
      histos["dR_l1l2"]      = bookHistogram1D("dR_l1l2", 20, 0.0, 6.0); 
      histos["dR_llmin"]     = bookHistogram1D("dR_llmin", 20, 0.0, 6.0); 
      histos["dR_llmax"]     = bookHistogram1D("dR_llmax", 20, 0.0, 6.0); 
      histos["dphi_ss"]      = bookHistogram1D("dphi_ss", 20, 0.0, M_PI);
      histos["deta_ss"]      = bookHistogram1D("deta_ss", 20, 0.0, 6.0);
      histos["dR_ss"]        = bookHistogram1D("dR_ss", 20, 0.0, 6.0);
      histos["deta_l1l2"]    = bookHistogram1D("deta_l1l2", 20, 0.0, 6.0); 
      histos["ll_dphi50"]    = bookHistogram1D("ll_dphi50",16, 0.0, M_PI);
    }
  public:
    ATLAStrileptonVR2() : 
      Analysis("ATLAStrileptonVR2"),
      _mupt_both(14.), _mupt_first(18.), _mupt_second(8.), _ept_both(14.), 
      _ept_first(25.), _ept_second(10.), _muept_e1(14.), _muept_e2(10.), 
      _muept_mu1(8.), _muept_mu2(18.), _mupt(10.), _ept(10.), _mueta(2.4),
      _eeta(2.47), _jR(0.4), _etj(20.), _etaj(2.5), _dr_ll(0.1), _dr_lj(0.4),
      _dr_jl(0.2), _isoR_lj(0.3), _e_ptmax(0.16), _mu_ptmax(0.12), _e_trackpt(0.4),
      _mu_trackpt(1.0), _mt(172), _sigMT(17.), _mW(80.414), _sigMW(10.), 
      _missEt(50.), _leta(2.5), _lpt(10)
    {setNeedsCrossSection(true);}
    
    void init() {
      ChargedLeptons lfs(FinalState(-_leta, _leta, _lpt*GeV));
      addProjection(lfs, "LFS");
      VisibleFinalState vfs(VisibleFinalState(-_etaj, _etaj));
      addProjection(vfs, "VFS");
      addProjection(MissingMomentum(vfs), "MissingET");
      IdentifiedFinalState nfs(-50., 50., 0.0*GeV);
      nfs.acceptNeutrinos();
      addProjection(nfs, "NFS");
      IdentifiedFinalState efs(-5, 5, 0*GeV);
      efs.acceptIdPair(ELECTRON);
      addProjection(efs, "EFS");
      IdentifiedFinalState mufs(-_mueta, _mueta, _mupt*GeV);
      mufs.acceptIdPair(MUON);
      addProjection(mufs, "MUFS");
      IdentifiedFinalState pfs(-5, 5, 0.0*GeV);
      pfs.acceptId(PHOTON);
      addProjection(pfs, "PFS");
      std::vector<std::pair<double, double> > etaRanges;
      etaRanges.push_back(make_pair(-_eeta, _eeta));
      LeptonClusters electrons(pfs, efs,0.1, true, 
				      etaRanges, _ept*GeV);
      addProjection(electrons, "DRESSEDELECTRONS");
      VetoedFinalState fs(FinalState(-5, 5, 0*GeV));
      fs.addVetoOnThisFinalState(mufs);
      fs.addVetoOnThisFinalState(electrons);
      addProjection(FastJets(fs, FastJets::ANTIKT, _jR), "Jets");
         
      inithistos();
    }
    void analyze(const Event& event) {
      const double weight = event.weight();
      // Use the "LFS" projection to require at least one hard charged
      // lepton. This is an experimental signature for the leptonically 
      // decaying W. This helps to reduce pure QCD backgrounds.
      const ParticleVector& tracks = 
	applyProjection<VisibleFinalState>(event, "VFS").particles();
      const vector<ClusteredLepton>& electrons = 
	applyProjection<LeptonClusters>(event, "DRESSEDELECTRONS").clusteredLeptons();
      const ParticleVector& muons = 
	applyProjection<IdentifiedFinalState>(event, "MUFS").particlesByPt();
      const MissingMomentum& met = 
	applyProjection<MissingMomentum>(event, "MissingET");
      const Jets alljets = 
	applyProjection<FastJets>(event, "Jets").jetsByPt();
      

      ParticleVector leptons;

      foreach (const Particle & muon, muons)
	leptons.push_back(muon);
      foreach (const ClusteredLepton & electron, electrons)
	leptons.push_back(Particle(electron.pdgId(),electron.momentum()));
      std::sort(leptons.begin(), leptons.end(), ptsort());

      double misset = met.vectorEt().mod();
      if (misset<_missEt*GeV) vetoEvent;
      // lepton triggering
      bool trigger(false);
      if (muons.size()>1){
	if ( (muons[0].momentum().pT()>_mupt_both && muons[1].momentum().pT()>_mupt_both) 
	     || (muons[0].momentum().pT()>_mupt_first && muons[1].momentum().pT()>_mupt_second))
	  trigger=true;
      }
      else if (electrons.size()>1){
	if ( (electrons[0].momentum().Et()>_ept_both && electrons[1].momentum().Et()>_ept_both) 
	     || (electrons[0].momentum().Et()>_ept_first && electrons[1].momentum().Et()>_ept_second))
	  trigger=true;
      }
      if (!trigger){
	if (muons.size()>0 && electrons.size()>0){
	  if ( (electrons[0].momentum().Et()>_muept_e1 && muons[0].momentum().pT()>_muept_mu1)
	       || (electrons[0].momentum().Et()>_muept_e2 && muons[0].momentum().pT()>_muept_mu2))
	    trigger=true;
	}
      }
      if(!trigger) vetoEvent;

      // setting pseudorapidity and transverse energy cuts on jets
      Jets jets, bjets,ljets;
      foreach(const Jet& jet,alljets){
	if ((jet.momentum().Et()<_etj)||(jet.momentum().eta()>_etaj)) continue;
	jets.push_back(jet);
      }
      //separate jets from electrons
      Jets isolatedJets;
      bool iso(true);
      foreach (const Jet& jet, jets){
	iso=true;
	foreach (const Particle& elec, electrons){
	  if (deltaR(jet.momentum(),elec.momentum())<_dr_jl){
	    iso=false;
	    break;
	  }
	}
	if (iso){
	  isolatedJets.push_back(jet);
	  if (jet.containsBottom()) bjets.push_back(jet);
	  else ljets.push_back(jet);
	}
      }

      // isolate leptons from leptons 
      ParticleVector isolatedLeptons;
      int k(leptons.size());
      bool separated(true);
      bool hard_elec(false);
      for (int i=0; i<k; i++){
	separated=true;
	for (int j=0; j<k; j++){
	  if (i==j) continue;
	  if (deltaR(leptons[i].momentum(),leptons[j].momentum())<_dr_ll){
	    separated=false;
	    if ((abs(leptons[i].pdgId())==11 && abs(leptons[j].pdgId())==11)
		&& leptons[i].momentum().pT()>leptons[j].momentum().pT()) hard_elec=true;
	  }
	}
	foreach (const Jet& jet, isolatedJets){
	  if (deltaR(jet.momentum(), leptons[i].momentum())<_dr_lj) {
	    separated=false;
	    hard_elec=false;
	  }
	}
	// now have tagged leptons with separated=true.
	// isolation
	double trackpt(0);
	if (abs(leptons[i].pdgId())==11) trackpt=_e_trackpt;
	if (abs(leptons[i].pdgId())==13) trackpt=_mu_trackpt;

	double coneET(0.);
	foreach (const Particle& track, tracks) {
	  if (track.momentum().pT()>trackpt){
	    if (deltaR(track.momentum(), leptons[i].momentum()) < _isoR_lj) {
	      coneET += track.momentum().pT();
	    }
	  }
	}
	if (abs(leptons[i].pdgId())==11 && coneET>(1+_e_ptmax)*leptons[i].momentum().Et()) {
	  separated=false;
	  hard_elec=false;
	}
	else if (abs(leptons[i].pdgId())==13 && coneET>(1+_mu_ptmax)*leptons[i].momentum().pT()) {
	  separated=false;
	}
	if (separated || hard_elec) isolatedLeptons.push_back(leptons[i]);
      }
      if (isolatedLeptons.size()!=3) vetoEvent;
      // sfos veto
      for (int i=0; i<2; i++){
	for (int j=i+1; j<3; j++){
	  if (isolatedLeptons[i].pdgId()==-isolatedLeptons[j].pdgId())
	    vetoEvent;
	}
      }
      FourMomentum lepton_ss1, lepton_ss2, lepton_os, lepmoms[3], lp1, lm1;
      int charge(PID::charge(isolatedLeptons[0]));
      if (charge==PID::charge(isolatedLeptons[1]))
	{
	  lepton_ss1=isolatedLeptons[0].momentum();
	  lepton_ss2=isolatedLeptons[1].momentum();
	  lepton_os=isolatedLeptons[2].momentum();
	}
      else if (charge==PID::charge(isolatedLeptons[2])){
	lepton_ss1=isolatedLeptons[0].momentum();
	lepton_ss2=isolatedLeptons[2].momentum();
	lepton_os=isolatedLeptons[1].momentum();
      }
      else{
	lepton_ss1=isolatedLeptons[1].momentum();
	lepton_ss2=isolatedLeptons[2].momentum();
	lepton_os=isolatedLeptons[0].momentum();
      }
      if (charge==PID::charge(isolatedLeptons[1])){
	if (charge==1){
	  lp1=isolatedLeptons[0].momentum();
	  lm1=isolatedLeptons[2].momentum();
	}
	else{
	  lm1=isolatedLeptons[0].momentum();
	  lp1=isolatedLeptons[2].momentum();
	}
      }
      else{
	if (charge==1){
	  lp1=isolatedLeptons[0].momentum();
	  lm1=isolatedLeptons[1].momentum();
	}
	else{
	  lm1=isolatedLeptons[0].momentum();
	  lp1=isolatedLeptons[1].momentum();
	}
      }
      for (int i=0; i<3; i++)
	lepmoms[i]=isolatedLeptons[i].momentum();

      double HTjet(0.);
      double HTob(0.);
      double HTv(0.);
      double HTl(0.);
      double min_dR(1e4);
      double max_dR(0);
      foreach (const Jet & jet, isolatedJets)
	HTjet+=jet.momentum().pT();
      HTv+=HTjet;
      foreach (const Particle & lep, isolatedLeptons)
      	HTl+=lep.momentum().pT();
      HTv+=HTl;
      HTob+=HTv;
      HTob+=misset;

     // find minimum separated
      for (int i=0; i<2; i++){
	for (int j=i+1; j<3; j++){
	  if (deltaR(lepmoms[i],lepmoms[j])<min_dR)
	    min_dR   = deltaR(lepmoms[i],lepmoms[j]);
	  if (deltaR(lepmoms[i],lepmoms[j])>max_dR)
	    max_dR   = deltaR(lepmoms[i],lepmoms[j]);
	}
      }
      
      histos["dphi_l1l2"]  -> fill(deltaPhi(lepmoms[0],lepmoms[1]),weight);
      histos["dR_l1l2"]    -> fill(deltaR(lepmoms[0],lepmoms[1]),weight);
      histos["deta_l1l2"]  -> fill(deltaEta(lepmoms[0],lepmoms[1]),weight);
      histos["dphi_ss"]    -> fill(deltaPhi(lepton_ss1,lepton_ss2),weight);
      histos["deta_ss"]    -> fill(deltaEta(lepton_ss1,lepton_ss2),weight);
      histos["dR_ss"]      -> fill(deltaR(lepton_ss1,lepton_ss2),weight);
      histos["dR_llmin"]   -> fill(min_dR,weight);
      histos["dR_llmax"]   -> fill(max_dR,weight);
      
      histos["jet_mult"]   -> fill(isolatedJets.size(),weight);
      histos["bjet_mult"]  -> fill(bjets.size(),weight);
      histos["HTjets"]     -> fill(HTjet,weight);
      histos["HTobjs"]     -> fill(HTob,weight);
      histos["HTvis"]      -> fill(HTv,weight);
      histos["HTlep"]      -> fill(HTl,weight);
      histos["mass_ll_ss"] -> fill((lepton_ss1+lepton_ss2).mass(),weight);
      float test;
      test=(lepton_ss1+lepton_os).mass();
      if ((lepton_ss2+lepton_os).mass()>test){
      	histos["mass_ll_os_1"] -> fill((lepton_ss2+lepton_os).mass(),weight); 
      	histos["mass_ll_os_2"] -> fill((lepton_ss1+lepton_os).mass(),weight); 
      }
      else {
      	histos["mass_ll_os_1"] -> fill((lepton_ss1+lepton_os).mass(),weight); 
      	histos["mass_ll_os_2"] -> fill((lepton_ss2+lepton_os).mass(),weight);
      } 
      histos["mass_lll"]     -> fill((lepmoms[0]+lepmoms[1]+lepmoms[2]).mass(),weight);
      histos["pT_l1"]        -> fill(lepmoms[0].pT()/GeV,weight);
      histos["pT_l2"]        -> fill(lepmoms[1].pT()/GeV,weight);
      histos["pT_l3"]        -> fill(lepmoms[2].pT()/GeV,weight);
      histos["pT_los"]       -> fill(lepton_os.pT(),weight);
      histos["pT_lp1"]       -> fill(lp1.pT(),weight);
      histos["pT_lm1"]       -> fill(lm1.pT(),weight);
      histos["eta_los"]      -> fill(lepton_os.eta(),weight);
      histos["eta_lp1"]      -> fill(lp1.eta(),weight);
      histos["eta_lm1"]      -> fill(lm1.eta(),weight);
      histos["pT_lll"]       -> fill((lepmoms[0]+lepmoms[1]+lepmoms[2]).pT()/GeV,weight);
      histos["eta_l1"]       -> fill(lepmoms[0].pseudorapidity(),weight);
      histos["eta_l2"]       -> fill(lepmoms[1].pseudorapidity(),weight);
      histos["eta_l3"]       -> fill(lepmoms[2].pseudorapidity(),weight);
      histos["eta_lll"]      -> fill((lepmoms[0]+lepmoms[1]+lepmoms[2]).pseudorapidity(),weight);
      histos["missET"]       -> fill(misset,weight); 
      if (lepmoms[0].pT()<50 and lepmoms[1].pT()<50){
	histos["ll_dphi50"] -> fill(deltaPhi(lepmoms[0],lepmoms[1]),weight);
      }
      if (isolatedJets.size()>0){
	histos["dphi_l1j"] -> fill(deltaPhi(lepmoms[0],isolatedJets[0].momentum()),weight);
	histos["dR_l1j"]   -> fill(deltaR(lepmoms[0],isolatedJets[0].momentum()),weight);
	histos["deta_l1j"] -> fill(deltaEta(lepmoms[0],isolatedJets[0].momentum()),weight);
	histos["pT_lj"]    -> fill(isolatedJets[0].momentum().pT(),weight);
	histos["eta_lj"]   -> fill(isolatedJets[0].momentum().eta(),weight);
      }
      
    }
    void finalize() {
      for (std::map<string,AIDA::IHistogram1D*>::iterator hit=histos.begin();
	   hit!=histos.end();hit++){
	Analysis::scale(hit->second, Analysis::crossSection()/Analysis::sumOfWeights());
	//normalize(hit->second);
      }
    }
  };

  
  
  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAStrileptonVR2);

}
