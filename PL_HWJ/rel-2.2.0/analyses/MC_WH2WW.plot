# BEGIN PLOT /MC_WH2WW/weights
Title=Cutflow
XLabel=
YLabel=$\sigma$ [pb]
XCustomMajorTicks=0	gen XS	1	iso lep	2	$E_\perp^\text{miss,min}$	3	veto Z	4	veto top	5	$m_{\ell\ell}^\text{min}$	6	$m_{\ell\ell}^\text{max}$	7	$\Delta R_{\ell\ell}^\text{min}$
# END PLOT

# BEGIN PLOT /MC_WH2WW/pt1st
Title=Leading lepton transverse momentum
XLabel=$p_\perp$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/pt2nd
Title=Subleading lepton transverse momentum
XLabel=$p_\perp$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/pt3rd
Title=Third lepton transverse momentum
XLabel=$p_\perp$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/Njets
Title=Inclusive jet multiplicity
XLabel=$N_\text{jet}$
YLabel=$\sigma(N_\text{jet})$ [pb]
# END PLOT

# BEGIN PLOT /MC_WH2WW/Nnu
Title=Neutrino multiplicity
XLabel=$N_\nu$
YLabel=$\sigma(N_\nu)$ [pb]
# END PLOT

# BEGIN PLOT /MC_WH2WW/etmiss
Title=Missing transverse energy
XLabel=$E_\perp^\text{miss}$ [GeV]
YLabel=d$\sigma$/d$/E_\perp^\text{miss}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/mass123
Title=Trilepton invariant mass
XLabel=$m_{3\ell}$ [GeV]
YLabel=d$\sigma$/d$m_{3\ell}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/ptjet1
Title=Leading jet transverse momentum
XLabel=$p_\perp$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/ptjet2
Title=Subleading jet transverse momentum
XLabel=$p_\perp$ [GeV]
YLabel=d$\sigma$/d$p_\perp$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/massOSSF
Title=Invariant mass of all opposite-sign same-flavour lepton pairs
XLabel=$m_\text{OSSF}$ [GeV]
YLabel=d$\sigma$/d$m_\text{OSSF}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/massOSAF
Title=Minimum invariant mass of all opposite-sign lepton pairs
XLabel=$m_\text{OSAF}$ [GeV]
YLabel=d$\sigma$/d$m_\text{OSAF}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/sROSAF
Title=Radial distance of opposite-sign lepton pair with minimal invariant mass
XLabel=$\Delta R$
YLabel=d$\sigma$/d$\Delta R$ [pb]
# END PLOT

# BEGIN PLOT /MC_WH2WW/fig1_l
Title=Missing transverse energy in SSSF events
XLabel=$E_\perp^\text{miss}$ [GeV]
YLabel=d$\sigma$/d$E_\perp^\text{miss}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/fig1_r
Title=Missing transverse energy in OSSF events
XLabel=$E_\perp^\text{miss}$ [GeV]
YLabel=d$\sigma$/d$E_\perp^\text{miss}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/fig2_t
Title=Invariant mass of the opposite-sign lepton pair in OSSF events
XLabel=$|m_{\ell\ell}-m_Z|$ [GeV]
YLabel=d$\sigma$/d$|m_{\ell\ell}-m_Z|$ [pb/GeV]
# END PLOT

# BEGIN PLOT /MC_WH2WW/fig3_l
Title=Radial distance of the opposite-sign lepton pair in SSSF events
XLabel=$\Delta R_{\ell^+\ell^-}$
YLabel=d$\sigma$/d$\Delta R_{\ell^+\ell^-}$ [pb]
# END PLOT

# BEGIN PLOT /MC_WH2WW/fig3_r
Title=Radial distance of the opposite-sign lepton pair in OSSF events
XLabel=$\Delta R_{\ell^+\ell^-}$
YLabel=d$\sigma$/d$\Delta R_{\ell^+\ell^-}$ [pb]
# END PLOT

##--amendments-------------------------------------------------------

# BEGIN PLOT /MC_WH2WW/ptjet1_j
XMax=50
LogY=0
# END PLOT

# BEGIN PLOT /MC_WH2WW/ptjet2_j
XMax=50
LogY=0
# END PLOT

# BEGIN SPECIAL .*_Z
\rput[lt]{0}(0.05,0.95){\bf w/ Z veto}
# END SPECIAL

# BEGIN SPECIAL .*_j
\rput[lt]{0}(0.05,0.95){\bf w/ Z and top veto}
# END SPECIAL
