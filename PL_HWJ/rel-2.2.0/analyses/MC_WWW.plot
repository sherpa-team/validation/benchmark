# BEGIN PLOT /MC_WWW/*
Rebin=5
# END PLOT

# BEGIN PLOT /MC_WWW/detal12
Title=$\Delta \eta$ between two hardest leptons	
XLabel=$\Delta \eta (l_1,l_2)$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta (l_1,l_2)$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/detal13
Title=$\Delta \eta$ between first and third lepton	
XLabel=$\Delta \eta (l_1,l_3)$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta (l_1,l_3)$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/detal23
Title=$\Delta \eta$ between second and third lepton
XLabel=$\Delta \eta (l_2,l_3)$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta (l_2,l_3)$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/detal1jet1
Title=$\Delta \eta$ between hardest lepton and hardest jet	
XLabel=$\Delta \eta (l_1,j_1)$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta (l_1,j_1)$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/detaw12
Title=$\Delta \eta$ between two hardest W's
XLabel=$\Delta \eta (W_1,W_2)$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta (W_1,W_2)$ [pb]
LogY=1
YMin=1.e-6
# END PLOT


# BEGIN PLOT /MC_WWW/detaw13
Title=$\Delta \eta$ between first and third W	
XLabel=$\Delta \eta (W_1,W_3)$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta (W_1,W_3)$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/detaw23
Title=$\Delta \eta$ between second and third W	
XLabel=$\Delta \eta (W_2,W_3)$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta (W_2,W_3)$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/detaw1jet1
Title=$\Delta \eta$ between hardest W and hardest jet	
XLabel=$\Delta \eta (W_1,j_1)$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta (W_1,j_1)$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/jeteta1
Title=$\eta$ of hardest jet	
XLabel=$\eta$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/leta1
Title=$\eta$ of hardest lepton	
XLabel=$\eta$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/leta2
Title=$\eta$ of second hardest lepton	
XLabel=$\eta$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/leta3
Title=$\eta$ of third hardest lepton
XLabel=$\eta$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/weta1
Title=$\eta$ of hardest W	
XLabel=$\eta$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/weta2
Title=$\eta$ of second hardest W	
XLabel=$\eta$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/weta3
Title=$\eta$ of third hardest W	
XLabel=$\eta$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta \eta$ [pb]
LogY=1
YMin=1.e-6
# END PLOT

# BEGIN PLOT /MC_WWW/jetpt1
Title=$p_\perp$ of hardest jet	
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=1
YMin=1.e-10
# END PLOT

# BEGIN PLOT /MC_WWW/lpt1
Title=$p_\perp$ of hardest lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=1
YMin=1.e-10
# END PLOT

# BEGIN PLOT /MC_WWW/lpt2
Title=$p_\perp$ of second hardest lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=1
YMin=1.e-10
# END PLOT

# BEGIN PLOT /MC_WWW/lpt3
Title=$p_\perp$ of third hardest lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=1
YMin=1.e-10
# END PLOT

# BEGIN PLOT /MC_WWW/wpt1
Title=$p_\perp$ of hardest W
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=1
YMin=1.e-10
# END PLOT

# BEGIN PLOT /MC_WWW/wpt2
Title=$p_\perp$ of second hardest W
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=1
YMin=1.e-10
# END PLOT

# BEGIN PLOT /MC_WWW/wpt3
Title=$p_\perp$ of third hardest W
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta p_\perp$ [pb/GeV]
LogY=1
YMin=1.e-10
# END PLOT


