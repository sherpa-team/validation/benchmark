#include "Rivet/Analysis.hh"
#include "Rivet/Math/LorentzTrans.hh"
#include "Rivet/Math/Constants.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Projections/Beam.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/LeptonClusters.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/AnalysisLoader.hh"
#include "Rivet/RivetAIDA.hh"
#include <map>
 
namespace Rivet {
  class ptsort {
  public:
    bool operator()(const Particle& a, const Particle & b){
      return (a.momentum().pT()>b.momentum().pT());
    }
  };

  class MC_WH2WW : public Analysis {
  private:
    double _mueta, _mupt, _muisoR, _muisotot;
    double _eeta, _ept, _eisoR, _eisoECAL, _eisoHCAL, _eisotrack, _eisotot;
    double _ptfirst, _ptsecond, _ptthird;
    double _jeta, _jpt, _jpt_top, _jR, _jlR, _llR; 
    double _mossfmin, _mossfmax;
    double _etmiss_ossf, _etmiss_sssf, _etaetmiss;
    double _pairmass_min, _pairmass_max, _pairdist;
    bool   _partonlevel, _simplifiedleptonisolation;

    std::map<std::string,AIDA::IHistogram1D *> _histos;

    void inithistos() {
      // OSSF -- opposite-sign same-flavour
      // OSAF -- opposite-sign any-flavour
      _histos["weights"] = bookHistogram1D("weights", 8, -0.5, 7.5);

      double ptmin(0.), ptmax(250.), massmin(20.), massmax(500.);
      int npt(50), nmass(50);
      _histos["pt1st"]     = bookHistogram1D("pt1st",   npt,ptmin,ptmax);
      _histos["pt2nd"]     = bookHistogram1D("pt2nd",   npt,ptmin,ptmax);
      _histos["pt3rd"]     = bookHistogram1D("pt3rd",   npt,ptmin,ptmax);
      _histos["etmiss"]    = bookHistogram1D("etmiss",  npt,ptmin,ptmax);
      _histos["mass123"]   = bookHistogram1D("mass123", nmass,massmin,massmax);
      _histos["massOSSF"]  = bookHistogram1D("massOSSF",nmass,massmin,massmax);
      _histos["Njets"]     = bookHistogram1D("Njets",   5,-0.5,4.5);
      _histos["ptjet1"]    = bookHistogram1D("ptjet1",  npt,ptmin,ptmax);
      _histos["ptjet2"]    = bookHistogram1D("ptjet2",  npt,ptmin,ptmax);
      _histos["massOSAF"]  = bookHistogram1D("massOSAF",nmass,massmin,massmax);
      _histos["dROSAF"]    = bookHistogram1D("dROSAF",  50,0.,5.);

      _histos["pt1st_Z"]   = bookHistogram1D("pt1st_Z",  npt,ptmin,ptmax);
      _histos["pt2nd_Z"]   = bookHistogram1D("pt2nd_Z",  npt,ptmin,ptmax);
      _histos["pt3rd_Z"]   = bookHistogram1D("pt3rd_Z",  npt,ptmin,ptmax);
      _histos["etmiss_Z"]  = bookHistogram1D("etmiss_Z", npt,ptmin,ptmax);
      _histos["mass123_Z"] = bookHistogram1D("mass123_Z",nmass,massmin,massmax);
      _histos["massOSSF_Z"]= bookHistogram1D("massOSSF_Z",nmass,massmin,massmax);

      _histos["pt1st_j"]   = bookHistogram1D("pt1st_j",  npt,ptmin,ptmax);
      _histos["pt2nd_j"]   = bookHistogram1D("pt2nd_j",  npt,ptmin,ptmax);
      _histos["pt3rd_j"]   = bookHistogram1D("pt3rd_j",  npt,ptmin,ptmax);
      _histos["etmiss_j"]  = bookHistogram1D("etmiss_j", npt,ptmin,ptmax);
      _histos["mass123_j"] = bookHistogram1D("mass123_j",nmass,massmin,massmax);
      _histos["Njets_j"]   = bookHistogram1D("Njets_j",  5,-0.5,4.5);
      _histos["ptjet1_j"]  = bookHistogram1D("ptjet1_j", npt,ptmin,ptmax);
      _histos["ptjet2_j"]  = bookHistogram1D("ptjet2_j", npt,ptmin,ptmax);

      _histos["fig1_l"]    = bookHistogram1D("fig1_l", 20, 0., 200.);
      _histos["fig1_r"]    = bookHistogram1D("fig1_r", 20, 0., 200.);
      _histos["fig2_t"]    = bookHistogram1D("fig2_t", 20, 0., 100.);
      _histos["fig3_l"]    = bookHistogram1D("fig3_l",  5, 0., 4.);
      _histos["fig3_r"]    = bookHistogram1D("fig3_r",  5, 0., 4.);

      _histos["Nnu"]       = bookHistogram1D("Nnu",    31,-0.5,30.5);
    }

    bool vetoTop(Jets & jets, const double & weight) {
      if (jets.size()==0) return true;
      if (jets[0].momentum().pT()>_jpt_top) return false;
      foreach (Jet jet, jets) if (jet.containsBottom()) return false;
      return true;
    }

    bool vetoZ(ParticleVector & isolatedLeptons, const bool & ossf, 
               const double & weight) {
      bool vetoit = false;
      double minmassopp(1.e10), dRopp(0.);
      for (size_t i=0;i<isolatedLeptons.size()-1;i++) {
	Particle leptoni = isolatedLeptons[i];
	int pdgidi       = int(leptoni.pdgId());
	int chargei      = int(PID::charge(leptoni.pdgId()));
	for (size_t j=i+1;j<isolatedLeptons.size();j++) {
	  Particle leptonj = isolatedLeptons[j];
	  int pdgidj       = int(leptonj.pdgId());
	  int chargej      = int(PID::charge(leptonj.pdgId()));
	  double massij(0.);
	  if ((leptoni.momentum()+leptonj.momentum()).mass2()>0)
	    massij  = (leptoni.momentum()+leptonj.momentum()).mass();
	  if (pdgidi==-pdgidj) {
	    if (ossf) _histos["fig2_t"]->fill(fabs(massij-91.2*GeV),weight);
	    _histos["massOSSF"]->fill(massij,weight);
	      if (massij>_mossfmin && massij<_mossfmax) vetoit = true;
	      else _histos["massOSSF_Z"]->fill(massij,weight);
	  }
	  if (chargei==-chargej && massij<minmassopp) {
	    minmassopp = massij;
	    dRopp       = deltaR(leptoni.momentum(),leptonj.momentum());
	  }
	}
	_histos["massOSAF"]->fill(minmassopp,weight);
	_histos["dROSAF"]->fill(dRopp,weight);
      }
      return (!vetoit);
    }

    void isolateLeptons(const ParticleVector & leptons,
                        ParticleVector & isolatedLeptons,
                        const ParticleVector & visibles) {
      foreach (const Particle& lepton, leptons) {
        double isoR(100.);
        bool iselectron(true);
        if      (fabs(lepton.pdgId())==ELECTRON) {
          isoR = _eisoR;
          iselectron = true;
        }
        else if (fabs(lepton.pdgId())==MUON) {
          isoR = _muisoR;
          iselectron = false;
        }
        else 
          std::cout << "Unknown lepton flavour "<< lepton.pdgId()
                    << " . Please revise analysis.\n";
        double leppt(lepton.momentum().pT());
        double lepeta(lepton.momentum().eta());
        if      ( iselectron && (leppt<_ept  || fabs(lepeta)>_eeta )) continue;
        else if (!iselectron && (leppt<_mupt || fabs(lepeta)>_mueta)) continue;
        double iso(-lepton.momentum().Et());
        // hadrons>100; leptons>10,<20; quarks<10; gluon 21; photon 22
        foreach (const Particle& particle, visibles) {
          if (deltaR(particle.momentum(), lepton.momentum()) < isoR) {
            iso += particle.momentum().Et();
          }
        }
        if ( iselectron && iso< _eisotot*leppt)
          isolatedLeptons.push_back(lepton);
        if (!iselectron && iso<_muisotot*leppt)
          isolatedLeptons.push_back(lepton);
      }
    }

  public:
    MC_WH2WW() : 
      Analysis("MC_WH2WW"),
      _mueta(2.4), _mupt(10.*GeV), _muisoR(0.3), _muisotot(0.15),
      _eeta(2.5), _ept(10.*GeV), _eisoR(0.4), _eisotot(0.15),
      _ptfirst(20.*GeV), _ptsecond(10.*GeV), _ptthird(10.*GeV),
      _jeta(2.5), _jpt(20.*GeV), _jpt_top(40.*GeV),
      _jR(0.5), _jlR(0.5), _llR(0.2),
      _mossfmin(66.2*GeV), _mossfmax(116.2*GeV),
      _etmiss_ossf(40.*GeV), _etmiss_sssf(30.*GeV), _etaetmiss(2.5),
      _pairmass_min(12.*GeV), _pairmass_max(100.*GeV), _pairdist(2.)
    {}

    void init() {
      double leta = max(_mueta,_eeta), lpt  = min(_mupt,_ept);
      ChargedLeptons lfs(FinalState(-leta, leta, lpt*GeV));
      addProjection(lfs, "LeptonFS");
      IdentifiedFinalState nus(-MAXRAPIDITY,MAXRAPIDITY,0.*GeV);
      nus.acceptNeutrinos();
      addProjection(nus, "Neutrinos");
      IdentifiedFinalState efs(-5, 5, 0.0*GeV);
      efs.acceptIdPair(ELECTRON);
      addProjection(efs, "EFS");
      IdentifiedFinalState pfs(-5, 5, 0.0*GeV);
      pfs.acceptId(PHOTON);
      addProjection(pfs, "PFS");
      IdentifiedFinalState mufs(-_mueta, _mueta, _mupt*GeV);
      mufs.acceptIdPair(MUON);
      addProjection(mufs, "MUFS");
      std::vector<std::pair<double, double> > etaRanges;
      etaRanges.push_back(make_pair(-_eeta, _eeta));
      LeptonClusters dressedelectrons(pfs, efs,0.1, true, 
				      etaRanges, _ept*GeV);
      addProjection(dressedelectrons, "DRESSEDELECTRONS");
      VetoedFinalState fs(FinalState(-_jeta, _jeta, 0*GeV));
      fs.addVetoOnThisFinalState(mufs);
      fs.addVetoOnThisFinalState(dressedelectrons);
      addProjection(FastJets(fs, FastJets::ANTIKT, _jR), "Jets");
      VisibleFinalState vfs(VisibleFinalState(-_etaetmiss, _etaetmiss));
      addProjection(vfs, "VisibleFS");
      addProjection(MissingMomentum(vfs), "MissingET");

      inithistos();
    }

    void analyze(const Event& event) {
      const double weight = event.weight();
      _histos["weights"]->fill(0,weight);
      // Use the "LFS" projection to require at least three hard charged
      // lepton. This is an experimental signature for the leptonically 
      // decaying W. This helps to reduce pure QCD backgrounds.
      const ParticleVector& neutrinos = 
             applyProjection<IdentifiedFinalState>(event,
                                                   "Neutrinos").particlesByPt();
      const ParticleVector& visibles = 
             applyProjection<VisibleFinalState>(event, "VisibleFS").particles();
      const MissingMomentum& met = 
             applyProjection<MissingMomentum>(event, "MissingET");
      const Jets& alljets = 
             applyProjection<FastJets>(event, "Jets").jetsByPt();
      const ParticleVector& muons = 
	applyProjection<IdentifiedFinalState>(event, "MUFS").particlesByPt();
      const vector<ClusteredLepton>& dressedelectrons = 
	applyProjection<LeptonClusters>(event, "DRESSEDELECTRONS").clusteredLeptons();

      ParticleVector leptons;

      foreach (const Particle & muon, muons)
	leptons.push_back(muon);
      foreach (const ClusteredLepton & electron, dressedelectrons)
	leptons.push_back(Particle(electron.pdgId(),electron.momentum()));
      std::sort(leptons.begin(), leptons.end(), ptsort());

      // simple curiosity -> number of neutrinos to build up the MET
      _histos["Nnu"]->fill(neutrinos.size(),weight);

      // compute missing E_T
      double misset = met.vectorEt().mod();
      // require at least 3 leptons satisfieing the pt cuts
      if (leptons.size()<3 ||
        leptons[0].momentum().pT()<_ptfirst ||
        leptons[1].momentum().pT()<_ptsecond ||
        leptons[2].momentum().pT()<_ptthird) vetoEvent;
//      _histos["weights"]->fill(1,weight);

      Jets jets;
      foreach (const Jet& jet, alljets) {
        if (jet.momentum().pT()>_jpt &&
            fabs(jet.momentum().pseudorapidity())<_jeta)
          jets.push_back(jet);
      }

      ParticleVector isolatedLeptons;
      isolateLeptons(leptons,isolatedLeptons,visibles);
      if (isolatedLeptons.size()<3 ||
          isolatedLeptons[0].momentum().pT()<_ptfirst ||
          isolatedLeptons[1].momentum().pT()<_ptsecond ||
          isolatedLeptons[2].momentum().pT()<_ptthird) vetoEvent;
      // in the following only operate on the three leading isolated leptons
      isolatedLeptons.resize(3);
      // require total charge +/-1
      if (fabs(  PID::charge(leptons[0].pdgId())
               + PID::charge(leptons[1].pdgId())
               + PID::charge(leptons[2].pdgId()))!=1) vetoEvent;
      _histos["weights"]->fill(1,weight);

      // determine whether ossf
      bool ossf(isolatedLeptons[0].pdgId() == -isolatedLeptons[1].pdgId() ||
                isolatedLeptons[0].pdgId() == -isolatedLeptons[2].pdgId() ||
                isolatedLeptons[1].pdgId() == -isolatedLeptons[2].pdgId());
      if (ossf) _histos["fig1_r"]->fill(misset,weight);
      else      _histos["fig1_l"]->fill(misset,weight);

      // cut on E_T,miss
      if (( ossf && misset < _etmiss_ossf) ||
          (!ossf && misset < _etmiss_sssf)) vetoEvent;
      _histos["weights"]->fill(2,weight);
      _histos["etmiss"]->fill(misset,weight);
      _histos["pt1st"]->fill(isolatedLeptons[0].momentum().pT(),weight);
      _histos["pt2nd"]->fill(isolatedLeptons[1].momentum().pT(),weight);
      _histos["pt3rd"]->fill(isolatedLeptons[2].momentum().pT(),weight);
      _histos["Njets"]->fill(jets.size(),weight);
      if (jets.size()>0) {
        _histos["ptjet1"]->fill(jets[0].momentum().pT(),weight);
        if (jets.size()>1) {
          _histos["ptjet2"]->fill(jets[1].momentum().pT(),weight);
        }
      }

      double mass123(( isolatedLeptons[0].momentum()
                      +isolatedLeptons[1].momentum()
                      +isolatedLeptons[2].momentum()).mass());
      _histos["mass123"]->fill(mass123,weight);

      // veto Z->ll decays
      if (!vetoZ(isolatedLeptons,ossf,weight)) vetoEvent;
      _histos["weights"]->fill(3,weight);
      _histos["etmiss_Z"]->fill(misset,weight);
      _histos["pt1st_Z"]->fill(isolatedLeptons[0].momentum().pT(),weight);
      _histos["pt2nd_Z"]->fill(isolatedLeptons[1].momentum().pT(),weight);
      _histos["pt3rd_Z"]->fill(isolatedLeptons[2].momentum().pT(),weight);
      _histos["mass123_Z"]->fill(mass123,weight);

      // veto tops with a jet veto/b-jet veto
      if (!vetoTop(jets,weight)) vetoEvent;
      _histos["weights"]->fill(4,weight);
      _histos["etmiss_j"]->fill(misset,weight);
      _histos["pt1st_j"]->fill(isolatedLeptons[0].momentum().pT(),weight);
      _histos["pt2nd_j"]->fill(isolatedLeptons[1].momentum().pT(),weight);
      _histos["pt3rd_j"]->fill(isolatedLeptons[2].momentum().pT(),weight);
      _histos["mass123_j"]->fill(mass123,weight);
      _histos["Njets_j"]->fill(jets.size(),weight);
      if (jets.size()>0) {
        _histos["ptjet1_j"]->fill(jets[0].momentum().pT(),weight);
        if (jets.size()>1) {
          _histos["ptjet2_j"]->fill(jets[1].momentum().pT(),weight);
        }
      }

      // cut on Delta lepton pair mass and distance
      double dRmin(100.), massmin(100000.*GeV);
      for (size_t i(0); i<isolatedLeptons.size(); i++) {
        for (size_t j(i+1); j<isolatedLeptons.size(); j++) {
          if (PID::charge(isolatedLeptons[i].pdgId())
                == -PID::charge(isolatedLeptons[j].pdgId())) {
	    double pairmass(0.);
	    if ((isolatedLeptons[i].momentum()+isolatedLeptons[j].momentum()).mass2()>0)
	      pairmass=( isolatedLeptons[i].momentum()
			 +isolatedLeptons[j].momentum()).mass();
            double pairdist(deltaR(isolatedLeptons[i],isolatedLeptons[j]));
            if (pairmass < _pairmass_min) vetoEvent;
            if (pairmass < massmin) massmin = pairmass;
            if (pairdist < dRmin) dRmin = pairdist;
          }
        }
      }
      _histos["weights"]->fill(5,weight);
      if (massmin > _pairmass_max) vetoEvent;
      _histos["weights"]->fill(6,weight);

      for (size_t i(0); i<isolatedLeptons.size(); i++) {
        for (size_t j(i+1); j<isolatedLeptons.size(); j++) {
          if (PID::charge(isolatedLeptons[i].pdgId()) 
                == -PID::charge(isolatedLeptons[j].pdgId())) {
            double pairdist(deltaR(isolatedLeptons[i],isolatedLeptons[j]));
            if (ossf) _histos["fig3_r"]->fill(pairdist,weight);
            else      _histos["fig3_l"]->fill(pairdist,weight);
          }
        }
      }

      if (dRmin > _pairdist) vetoEvent;
      _histos["weights"]->fill(7,weight);
    }

    void finalize() {
      double scalefactor(crossSection()/sumOfWeights());
      for (std::map<std::string,AIDA::IHistogram1D *>::iterator 
              hit=_histos.begin(); hit!=_histos.end();hit++) 
        scale(hit->second,scalefactor);
    }
  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_WH2WW);
}
