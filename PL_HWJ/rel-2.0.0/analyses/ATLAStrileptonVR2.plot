# BEGIN PLOT /ATLAStrileptonVR2/jet_mult
Title=jet multiplicity
XLabel=$N_\text{jet}$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,N_\text{jet}$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/bjet_mult
Title=b jet multiplicity
XLabel=$N_\text{b jet}$
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,N_\text{b jet}$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/HTjets
Title=$H_\perp$ of all jets
XLabel=$H_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,H_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/HTobjs
Title=$H_\perp$ of all objects in the event
XLabel=$H_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,H_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/HTvis
Title=$H_\perp$ of visible objects in the event
XLabel=$H_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,H_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/mass_ll_ss
Title=Invariant mass of the 2 same sign electrons
XLabel=$m_{ll}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,m_{ll}$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/mass_ll_os_1
Title= Invariant mass of the harder pair of oppositely charged leptons.
XLabel=$m_{ll}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,m_{ll}$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/mass_ll_os_2
Title= Invariant mass of the softer pair of oppositely charged leptons.
XLabel=$m_{ll}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,m_{ll}$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/mass_lll
Title= Invariant mass of the leptons
XLabel=$m_{lll}$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,m_{lll}$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/pT_l1
Title= $p_\perp$ of the hardest lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,p_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/pT_l2
Title= $p_\perp$ of the second hardest lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,p_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/pT_l3
Title= $p_\perp$ of the softest lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,p_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/pT_los
Title= $p_\perp$ of the oppostiely charged lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,p_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/pT_lp1
Title= $p_\perp$ of the hardest positively charged lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,p_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/pT_lm1
Title= $p_\perp$ of the hardest negatively charged lepton
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,p_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/pT_lll
Title= $p_\perp$ of the tri-lepton system
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,p_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/eta_l1
Title= $\eta$ of the hardest lepton
XLabel=$\eta$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\eta$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/eta_l2
Title= $\eta$ of the second hardest lepton
XLabel=$\eta$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\eta$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/eta_l3
Title= $\eta$ of the softest lepton
XLabel=$\eta$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\eta$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/eta_los
Title= $\eta$ of the oppositely charged lepton
XLabel=$\eta$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\eta$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/eta_lp1
Title= $\eta$ of the hardest positively charged lepton
XLabel=$\eta$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\eta$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/eta_lm1
Title= $\eta$ of the hardest negatively charged lepton
XLabel=$\eta$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\eta$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/eta_lll
Title= $\eta$ of the tri-lepton system
XLabel=$\eta$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\eta$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/missET
Title= missing $E_\perp$
XLabel=$E_\perp$ miss  [GeV]
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,E_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/pT_lj
Title= $p_\perp$ of the hardest jet
XLabel=$p_\perp$ [GeV] 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,p_\perp$ [pb/GeV]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/eta_lj
Title= $\eta$ of the hardest jet
XLabel=$\eta$  
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\eta$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/dphi_l1j
Title= $\Delta\,\Phi$ between the hardest lepton and the nearest jet
XLabel=$\Delta\,\Phi_{lj}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,\Phi_{lj}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/dR_l1j
Title= $\Delta\,R$ between the hardest lepton and the nearest jet
XLabel=$\Delta\,R_{lj}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,R_{lj}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/deta_l1j
Title= $\Delta\,\eta$ between the hardest lepton and the nearest jet
XLabel=$\Delta\,\eta_{lj}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,\eta_{lj}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/dphi_l1l2
Title= $\Delta\,\Phi$ between the hardest 2 leptons
XLabel=$\Delta\,\Phi_{l_1l_2}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,\Phi_{l_1l_2}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/dR_l1l2
Title= $\Delta\,R$ between the hardest 2 leptons
XLabel=$\Delta\,R_{l_1l_2}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,R_{l_1l_2}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/dR_llmin
Title= The minimum $\Delta\,R$ between 2 leptons
XLabel=$\Delta\,R_{ll\test{ min}}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,R_{ll\text{ min}}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/dR_llmax
Title= The maximum $\Delta\,R$ between 2 leptons
XLabel=$\Delta\,R_{ll\text{ max}}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,R_{ll\text{ max}}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/dphi_ss
Title= $\Delta\,\Phi$ between the 2 same sign leptons
XLabel=$\Delta\,\Phi_{ll}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,\Phi_{ll}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/deta_ss
Title= $\Delta\,\eta$ between the 2 same sign leptons
XLabel=$\Delta\,\eta_{ll}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,\eta_{ll}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/dR_ss
Title= $\Delta\,R$ between the 2 same sign leptons
XLabel=$\Delta\,R_{ll}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,R_{ll}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/deta_l1l2
Title= $\Delta\,\eta$ between the 2 hardest leptons
XLabel=$\Delta\,\eta_{l_1l_2}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,\eta_{l_1l_2}$ [pb]
LogY=1
# END PLOT

# BEGIN PLOT /ATLAStrileptonVR2/ll_dphi50
Title= $\Delta\,\Phi$ between the 2 hardest leptons (if hardest $p_\perp\,<\,50$ GeV)
XLabel=$\Delta\,\Phi_{ll}$ 
YLabel=$\mathrm{d}\,\sigma/\mathrm{d}\,\Delta\,\Phi_{ll}$ [pb]
LogY=1
# END PLOT