// -*- C++ -*-

#include "Rivet/Analysis.hh"
#include "Rivet/RivetAIDA.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/WFinder.hh"

namespace Rivet {


  class MC_WWW : public Analysis {
  public:

    /// Constructor
    MC_WWW() : Analysis("MC_WWW") {
      setNeedsCrossSection(true);
    }

    void init() {
      // Set up projections
      // W finders
      FinalState fs;
      WFinder wfinder1(fs, -5., 5., 10.0*GeV, MUON, 60.0*GeV, 100.0*GeV, 10.0*GeV, 0.1);
      addProjection(wfinder1, "WFinder1");
      VetoedFinalState w2input;
      w2input.addVetoOnThisFinalState(wfinder1);
      WFinder wfinder2(w2input, -5., 5., 10.0*GeV, MUON, 60.0*GeV, 100.0*GeV, 10.0*GeV, 0.1);
      addProjection(wfinder2, "WFinder2");
      VetoedFinalState w3input;
      w3input.addVetoOnThisFinalState(wfinder1);
      w3input.addVetoOnThisFinalState(wfinder2);
      WFinder wfinder3(w3input, -5., 5., 10.0*GeV, MUON, 60.0*GeV, 100.0*GeV, 10.0*GeV, 0.1);
      addProjection(wfinder3, "WFinder3");
      // jets
      VetoedFinalState veto;
      veto.addVetoOnThisFinalState(wfinder1);
      veto.addVetoOnThisFinalState(wfinder2);
      veto.addVetoOnThisFinalState(wfinder3);
      FastJets jets(veto, FastJets::ANTIKT, 0.4);
      jets.useInvisibles(true);
      addProjection(jets, "JETS");

	_h_wpt_1        = bookHistogram1D("wpt1", 100, 10., 500.);
	_h_wpt_2        = bookHistogram1D("wpt2", 100, 10., 500.);
	_h_wpt_3        = bookHistogram1D("wpt3", 100, 10., 500.);
	_h_weta_1       = bookHistogram1D("weta1", 100, -5., 5.);
	_h_weta_2       = bookHistogram1D("weta2", 100, -5., 5.);
	_h_weta_3       = bookHistogram1D("weta3", 100, -5., 5.);
	_h_jetpt_1      = bookHistogram1D("jetpt1", 100, 10., 500.);
	_h_jeteta_1     = bookHistogram1D("jeteta1", 100, -5., 5.);
	_h_deta_w12     = bookHistogram1D("detaw12", 100, -5., 5.);
	_h_deta_w13     = bookHistogram1D("detaw13", 100, -5., 5.);
	_h_deta_w23     = bookHistogram1D("detaw23", 100, -5., 5.);
	_h_deta_w1jet1  = bookHistogram1D("detaw1jet1", 100, -5., 5.);
	_h_lpt_1        = bookHistogram1D("lpt1", 100, 10., 500.);
	_h_lpt_2        = bookHistogram1D("lpt2", 100, 10., 500.);
	_h_lpt_3        = bookHistogram1D("lpt3", 100, 10., 500.);
	_h_leta_1       = bookHistogram1D("leta1", 100, -5., 5.);
	_h_leta_2       = bookHistogram1D("leta2", 100, -5., 5.);
	_h_leta_3       = bookHistogram1D("leta3", 100, -5., 5.);
	_h_deta_l12     = bookHistogram1D("detal12", 100, -5., 5.);
	_h_deta_l13     = bookHistogram1D("detal13", 100, -5., 5.);
	_h_deta_l23     = bookHistogram1D("detal23", 100, -5., 5.);
	_h_deta_l1jet1  = bookHistogram1D("detal1jet1", 100, -5., 5.);
    }
	
	/// Do the analysis
    void analyze(const Event& event) {
      const double weight = event.weight();
      const WFinder& wfinder1 = applyProjection<WFinder>(event, "WFinder1");
      const WFinder& wfinder2 = applyProjection<WFinder>(event, "WFinder2");
      const WFinder& wfinder3 = applyProjection<WFinder>(event, "WFinder3");
      const Jets& jets = applyProjection<FastJets>(event, "JETS").jetsByPt(20.,1000.,-5.,5.);

      if (wfinder3.bosons().size() == 0) vetoEvent;

      double pt1,pt2,pt3;
      FourMomentum w1,w2,w3;
      pt1 = wfinder1.bosons()[0].momentum().pT();
      pt2 = wfinder2.bosons()[0].momentum().pT();
      pt3 = wfinder3.bosons()[0].momentum().pT();
      if (pt1 > pt2 && pt1 > pt3) {
	w1 = wfinder1.bosons()[0].momentum();
        if (pt2 > pt3) {
          w2 = wfinder2.bosons()[0].momentum();
          w3 = wfinder3.bosons()[0].momentum();
        }
        else {
          w2 = wfinder3.bosons()[0].momentum();
          w3 = wfinder2.bosons()[0].momentum();
        }
      }
      else if (pt2 > pt1 && pt2 > pt3) {
	w1 = wfinder2.bosons()[0].momentum();
        if (pt1 > pt3) {
          w2 = wfinder1.bosons()[0].momentum();
          w3 = wfinder3.bosons()[0].momentum();
        }
        else {
          w2 = wfinder3.bosons()[0].momentum();
          w3 = wfinder1.bosons()[0].momentum();
        }
      }
      else {
	w1 = wfinder3.constituentLeptons()[0].momentum();
        if (pt1 > pt2) {
          w2 = wfinder1.bosons()[0].momentum();
          w3 = wfinder2.bosons()[0].momentum();
        }
        else {
          w2 = wfinder2.bosons()[0].momentum();
          w3 = wfinder1.bosons()[0].momentum();
        }
      }


      FourMomentum l1,l2,l3;
      pt1 = wfinder1.constituentLeptons()[0].momentum().pT();
      pt2 = wfinder2.constituentLeptons()[0].momentum().pT();
      pt3 = wfinder3.constituentLeptons()[0].momentum().pT();
      if (pt1 > pt2 && pt1 > pt3) {
	l1 = wfinder1.constituentLeptons()[0].momentum();
        if (pt2 > pt3) {
          l2 = wfinder2.constituentLeptons()[0].momentum();
          l3 = wfinder3.constituentLeptons()[0].momentum();
        }
        else {
          l2 = wfinder3.constituentLeptons()[0].momentum();
          l3 = wfinder2.constituentLeptons()[0].momentum();
        }
      }
      else if (pt2 > pt1 && pt2 > pt3) {
	l1 = wfinder2.constituentLeptons()[0].momentum();
        if (pt1 > pt3) {
          l2 = wfinder1.constituentLeptons()[0].momentum();
          l3 = wfinder3.constituentLeptons()[0].momentum();
        }
        else {
          l2 = wfinder3.constituentLeptons()[0].momentum();
          l3 = wfinder1.constituentLeptons()[0].momentum();
        }
      }
      else {
	l1 = wfinder3.constituentLeptons()[0].momentum();
        if (pt1 > pt2) {
          l2 = wfinder1.constituentLeptons()[0].momentum();
          l3 = wfinder2.constituentLeptons()[0].momentum();
        }
        else {
          l2 = wfinder2.constituentLeptons()[0].momentum();
          l3 = wfinder1.constituentLeptons()[0].momentum();
        }
      }

      _h_wpt_1->fill(w1.pT(),weight);
      _h_wpt_2->fill(w2.pT(),weight);
      _h_wpt_3->fill(w3.pT(),weight);
      _h_weta_1->fill(w1.eta(),weight);
      _h_weta_2->fill(w2.eta(),weight);
      _h_weta_3->fill(w3.eta(),weight);
      _h_deta_w12->fill(w1.eta()-w2.eta(),weight);
      _h_deta_w13->fill(w1.eta()-w3.eta(),weight);
      _h_deta_w23->fill(w2.eta()-w3.eta(),weight);

      _h_lpt_1->fill(l1.pT(),weight);
      _h_lpt_2->fill(l2.pT(),weight);
      _h_lpt_3->fill(l3.pT(),weight);
      _h_leta_1->fill(l1.eta(),weight);
      _h_leta_2->fill(l2.eta(),weight);
      _h_leta_3->fill(l3.eta(),weight);
      _h_deta_l12->fill(l1.eta()-l2.eta(),weight);
      _h_deta_l13->fill(l1.eta()-l3.eta(),weight);
      _h_deta_l23->fill(l2.eta()-l3.eta(),weight);
      if (jets.size()>0) {
	FourMomentum jet1(jets[0].momentum());
	_h_jetpt_1->fill(jet1.pT(),weight);
	_h_jeteta_1->fill(jet1.eta(),weight);
	_h_deta_w1jet1->fill(w1.eta()-jet1.eta(),weight);
	_h_jetpt_1->fill(jet1.pT(),weight);
	_h_jeteta_1->fill(jet1.eta(),weight);
	_h_deta_l1jet1->fill(l1.eta()-jet1.eta(),weight);
      }

   }


    // Normalize histos
    void finalize() {
      scale(_h_wpt_1, crossSection()/sumOfWeights());
      scale(_h_wpt_2, crossSection()/sumOfWeights());;
      scale(_h_wpt_3, crossSection()/sumOfWeights());
      scale(_h_weta_1, crossSection()/sumOfWeights());
      scale(_h_weta_1, crossSection()/sumOfWeights());
      scale(_h_weta_1, crossSection()/sumOfWeights());
      scale(_h_jetpt_1, crossSection()/sumOfWeights());
      scale(_h_jeteta_1, crossSection()/sumOfWeights());
      scale(_h_deta_w12, crossSection()/sumOfWeights());
      scale(_h_deta_w13, crossSection()/sumOfWeights());
      scale(_h_deta_w23, crossSection()/sumOfWeights());
	scale(_h_deta_w1jet1, crossSection()/sumOfWeights());
      scale(_h_lpt_1, crossSection()/sumOfWeights());
      scale(_h_lpt_2, crossSection()/sumOfWeights());;
      scale(_h_lpt_3, crossSection()/sumOfWeights());
      scale(_h_leta_1, crossSection()/sumOfWeights());
      scale(_h_leta_1, crossSection()/sumOfWeights());
      scale(_h_leta_1, crossSection()/sumOfWeights());
      scale(_h_deta_l12, crossSection()/sumOfWeights());
      scale(_h_deta_l13, crossSection()/sumOfWeights());
      scale(_h_deta_l23, crossSection()/sumOfWeights());
	scale(_h_deta_l1jet1, crossSection()/sumOfWeights());
   }



  private:

	AIDA::IHistogram1D* _h_wpt_1;       
	AIDA::IHistogram1D* _h_wpt_2;       
	AIDA::IHistogram1D* _h_wpt_3;       
	AIDA::IHistogram1D* _h_weta_1;      
	AIDA::IHistogram1D* _h_weta_2;      
	AIDA::IHistogram1D* _h_weta_3;      
	AIDA::IHistogram1D* _h_jetpt_1;     
	AIDA::IHistogram1D* _h_jeteta_1;    
	AIDA::IHistogram1D* _h_deta_w12;    
	AIDA::IHistogram1D* _h_deta_w13;    
	AIDA::IHistogram1D* _h_deta_w23;    
      AIDA::IHistogram1D* _h_deta_w1jet1; 
	AIDA::IHistogram1D* _h_lpt_1;       
	AIDA::IHistogram1D* _h_lpt_2;       
	AIDA::IHistogram1D* _h_lpt_3;       
	AIDA::IHistogram1D* _h_leta_1;      
	AIDA::IHistogram1D* _h_leta_2;      
	AIDA::IHistogram1D* _h_leta_3;      
	AIDA::IHistogram1D* _h_deta_l12;    
	AIDA::IHistogram1D* _h_deta_l13;    
	AIDA::IHistogram1D* _h_deta_l23;    
      AIDA::IHistogram1D* _h_deta_l1jet1; 
  };


  // This global object acts as a hook for the plugin system
  AnalysisBuilder<MC_WWW> plugin_MC_WWW;

}
