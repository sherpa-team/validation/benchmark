#include "Rivet/Analysis.hh"
#include "Rivet/Math/LorentzTrans.hh"
#include "Rivet/Math/Constants.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
#include "Rivet/Projections/Beam.hh"
#include "Rivet/Projections/UnstableFinalState.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/LeptonClusters.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/ChargedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/AnalysisLoader.hh"
#include "Rivet/RivetAIDA.hh"
#include <map>

namespace Rivet {
  class ptsort {
  public:
    bool operator()(const Particle& a, const Particle & b){
      return (a.momentum().pT()>b.momentum().pT());
    }
  };
  class MC_WH_ATLAS : public Analysis {
  private:
    double _mueta, _mupt, _eeta, _ept, _jR, _jpt, _isoptfrac, _isoR1, _isoR2;
    double _mutrigpt, _etrigpt, _etmisszrich, _etmisszdepl, _dMsfos, _Mllmin, _dR01max;

    std::map<std::string,AIDA::IHistogram1D *> _histos;
    
    void inithistos() {
      double ptmin(0.), ptmax(200.), massmin(0.), massmax(200.), dRmin(0.), dRmax(5.);
      int npt(40), nmass(20), ndR(25);
      _histos["pt1st"]     = bookHistogram1D("pt1st",   npt,ptmin,ptmax);
      _histos["pt2nd"]     = bookHistogram1D("pt2nd",   npt,ptmin,ptmax);
      _histos["pt3rd"]     = bookHistogram1D("pt3rd",   npt,ptmin,ptmax);
      _histos["mass01"]    = bookHistogram1D("mass01",nmass,massmin,massmax);
      _histos["mass02"]    = bookHistogram1D("mass02",nmass,massmin,massmax);
      _histos["Njets"]     = bookHistogram1D("Njets",  8,-0.5,7.5);
      _histos["Nbjets"]    = bookHistogram1D("Nbjets",  5,-0.5,4.5);
      _histos["etmiss"]    = bookHistogram1D("etmiss",  npt,ptmin,ptmax);
      _histos["etmiss_Zrich"]   = bookHistogram1D("etmiss_Zrich",  npt,ptmin,ptmax);
      _histos["etmiss_Zdepl"]   = bookHistogram1D("etmiss_Zdepl",  5,ptmin,ptmax);
      _histos["deltaR01"]  = bookHistogram1D("deltaR01",  ndR,dRmin,dRmax);
      _histos["deltaR01_Zrich"] = bookHistogram1D("deltaR01_Zrich",  ndR,dRmin,dRmax);
      _histos["deltaR01_Zdepl"] = bookHistogram1D("deltaR01_Zdepl",   5,dRmin,dRmax);

      _histos["cuts_Zrich"] = bookHistogram1D("cuts_Zrich",   8,-0.5,7.5);
      _histos["cuts_Zdepl"] = bookHistogram1D("cuts_Zdepl",   8,-0.5,7.5);
	
    }
    

    bool LeptonIsolation(const Particle& lepton, const ParticleVector & particles,
				  const double dR, const double ptfrac) {
	double isopt = -lepton.momentum().Et();
	foreach (const Particle& particle, particles) {
	  if (deltaR(particle, lepton) < dR) { 
	    isopt += particle.momentum().Et();
	    if (isopt > ptfrac*lepton.momentum().pT()) return false;
	  }
	}
	return true;
    }

    bool preSelection(const ParticleVector & leptons,
			ParticleVector & isolatedLeptons, ParticleVector & orderedLeptons,
			const ParticleVector & particles) {
	ParticleVector leptonstmp;
// lepton isolation first round
      foreach (const Particle& lepton, leptons) {
	  if (LeptonIsolation(lepton,particles,_isoR1,_isoptfrac)) {
	    orderedLeptons.push_back(lepton);
	    leptonstmp.push_back(lepton);
	  }
	}
// require exactly three leptons with total charge +/-1
	if (leptonstmp.size()!=3) return false;
// require one lepton to fulfill trigger requirement
	if ((leptonstmp[0].pdgId()==11 || leptonstmp[0].pdgId()==-11) && 
	     leptonstmp[0].momentum().pT() < _etrigpt) return false;
	if ((leptonstmp[0].pdgId()==13 || leptonstmp[0].pdgId()==-13) && 
	     leptonstmp[0].momentum().pT() < _mutrigpt) return false;
// require total charge +/-1
	int totcharge(0);
	foreach (const Particle lepton, leptonstmp) {
	  totcharge += PID::charge(lepton.pdgId());
	}
	if (fabs(totcharge)!=1) return false;
	ParticleVector::iterator iter;
	for ( iter=leptonstmp.begin(); iter!=leptonstmp.end(); iter++) {
	  if (PID::charge((*iter).pdgId()) == -totcharge) {
	    isolatedLeptons.push_back(*iter);
	    leptonstmp.erase(iter);
	    break;
	  }
	}
	double dR1(deltaR(isolatedLeptons.front(),leptonstmp.front()));
	double dR2(deltaR(isolatedLeptons.front(),leptonstmp.back()));
	if (dR1 < dR2) {
	  isolatedLeptons.push_back(leptonstmp.front());
	  isolatedLeptons.push_back(leptonstmp.back());
	}
	else {
	  isolatedLeptons.push_back(leptonstmp.back());
	  isolatedLeptons.push_back(leptonstmp.front());
	}
// require tighter isolation for lepton 2
	if (!LeptonIsolation(isolatedLeptons[2],particles,_isoR2,_isoptfrac)) return false;
	return true;
    }



  public:
    MC_WH_ATLAS() : 
      Analysis("MC_WH_ATLAS"),
      _mueta(2.5), _mupt(10.*GeV), _eeta(2.47), _ept(10.*GeV),
      _jR(0.4),_jpt(25.*GeV),_isoptfrac(0.1), _isoR1(0.2), _isoR2(0.4), 
      _mutrigpt(21.*GeV), _etrigpt(25.*GeV), _etmisszrich(40.*GeV), _etmisszdepl(25.*GeV),
      _dMsfos(25.*GeV), _Mllmin(12.*GeV), _dR01max(2.)
    {}
    
    void init() {
      double leta = max(_mueta,_eeta), lpt  = min(_mupt,_ept);
      ChargedLeptons lfs(FinalState(-leta, leta, lpt*GeV));
      addProjection(lfs, "LeptonFS");
      VisibleFinalState vfs(VisibleFinalState(-4.9, 4.9));
      addProjection(vfs, "VisibleFS");
      IdentifiedFinalState efs(-5, 5, 0.0*GeV);
      efs.acceptIdPair(ELECTRON);
      addProjection(efs, "EFS");
      IdentifiedFinalState pfs(-5, 5, 0.0*GeV);
      pfs.acceptId(PHOTON);
      addProjection(pfs, "PFS");
      IdentifiedFinalState mufs(-_mueta, _mueta, _mupt*GeV);
      mufs.acceptIdPair(MUON);
      addProjection(mufs, "MUFS");
      std::vector<std::pair<double, double> > etaRanges;
      etaRanges.push_back(make_pair(-_eeta, _eeta));
      LeptonClusters dressedelectrons(pfs, efs,0.1, true, 
				      etaRanges, _ept*GeV);
      addProjection(dressedelectrons, "DRESSEDELECTRONS");
      VetoedFinalState fs(FinalState(-4.9, 4.9, 0*GeV));
      fs.addVetoOnThisFinalState(mufs);
      fs.addVetoOnThisFinalState(dressedelectrons);
      addProjection(FastJets(fs, FastJets::ANTIKT, _jR), "Jets");
      addProjection(MissingMomentum(vfs), "MissingET");
      
      inithistos();
    }

    void analyze(const Event& event) {
      const double weight = event.weight();
      const ParticleVector& muons = 
	applyProjection<IdentifiedFinalState>(event, "MUFS").particlesByPt();
      const ParticleVector& visibles = 
	applyProjection<VisibleFinalState>(event, "VisibleFS").particles();
      const MissingMomentum& met = 
	applyProjection<MissingMomentum>(event, "MissingET");
      const vector<ClusteredLepton>& dressedelectrons = 
	applyProjection<LeptonClusters>(event, "DRESSEDELECTRONS").clusteredLeptons();
      const Jets& jets = 
	applyProjection<FastJets>(event, "Jets").jetsByPt(25.);

      _histos["cuts_Zrich"]->fill(0,weight);
      _histos["cuts_Zdepl"]->fill(0,weight);

      ParticleVector leptons;

      foreach (const Particle & muon, muons)
	leptons.push_back(muon);
      foreach (const ClusteredLepton & electron, dressedelectrons)
	leptons.push_back(Particle(electron.pdgId(),electron.momentum()));
      std::sort(leptons.begin(), leptons.end(), ptsort());

      ParticleVector isolatedLeptons, orderedLeptons;
      if (!preSelection(leptons,isolatedLeptons,orderedLeptons,visibles)) vetoEvent;

      _histos["cuts_Zrich"]->fill(1,weight);
      _histos["cuts_Zdepl"]->fill(1,weight);

	int nbjets(0);
	foreach (const Jet & jet, jets) {
	  if (jet.containsBottom()) nbjets++;
	}

	double zrich;
	if (isolatedLeptons[0].pdgId() == -isolatedLeptons[1].pdgId() || 
	    isolatedLeptons[0].pdgId() == -isolatedLeptons[2].pdgId()) zrich = true;
	else zrich = false;


      const Vector3 etmiss = met.vectorEt();
	const double etmissrel = etmiss.mod();

	const double dR01(deltaR(isolatedLeptons[0],isolatedLeptons[1]));
	const double M01((isolatedLeptons[0].momentum()+isolatedLeptons[1].momentum()).mod());
	const double M02((isolatedLeptons[0].momentum()+isolatedLeptons[2].momentum()).mod());

      _histos["pt1st"]->fill(orderedLeptons[0].momentum().pT(),weight);
      _histos["pt2nd"]->fill(orderedLeptons[1].momentum().pT(),weight);
      _histos["pt3rd"]->fill(orderedLeptons[2].momentum().pT(),weight);
      _histos["mass01"]->fill(M01,weight);
      _histos["mass02"]->fill(M02,weight);
	_histos["Njets"]->fill(jets.size(),weight);
	_histos["Nbjets"]->fill(nbjets,weight);
	_histos["etmiss"]->fill(etmissrel,weight);
	_histos["deltaR01"]->fill(dR01,weight);
	if (zrich) {
	  _histos["etmiss_Zrich"]->fill(etmissrel,weight);
	  _histos["deltaR01_Zrich"]->fill(dR01,weight);
	}
	else {
	  _histos["etmiss_Zdepl"]->fill(etmissrel,weight);
	  _histos["deltaR01_Zdepl"]->fill(dR01,weight);
	}

	if (jets.size() > 1) vetoEvent;
	if (zrich) _histos["cuts_Zrich"]->fill(2,weight);
	else _histos["cuts_Zdepl"]->fill(2,weight);

	if (jets.size() > 0 && jets[0].containsBottom()) vetoEvent;
	if (zrich) _histos["cuts_Zrich"]->fill(3,weight);
	else _histos["cuts_Zdepl"]->fill(3,weight);

	if ((zrich && etmissrel < _etmisszrich) || 
	    (!zrich && etmissrel < _etmisszdepl)) vetoEvent;
	if (zrich) _histos["cuts_Zrich"]->fill(4,weight);
	else _histos["cuts_Zdepl"]->fill(4,weight);

	if (zrich) {
	  if(isolatedLeptons[0].pdgId() == -isolatedLeptons[1].pdgId() && 
	     fabs(M01 - 91.2*GeV) < _dMsfos) 
		vetoEvent;
	  if(isolatedLeptons[0].pdgId() == -isolatedLeptons[2].pdgId() && 
	     fabs(M02 - 91.2*GeV) < _dMsfos) 
		vetoEvent;
	}
	if (zrich) _histos["cuts_Zrich"]->fill(5,weight);
	else _histos["cuts_Zdepl"]->fill(5,weight);

	if (min(M01,M02) < _Mllmin) vetoEvent;
	if (zrich) _histos["cuts_Zrich"]->fill(6,weight);
	else _histos["cuts_Zdepl"]->fill(6,weight);

	if (dR01 > _dR01max) vetoEvent;
	if (zrich) _histos["cuts_Zrich"]->fill(7,weight);
	else _histos["cuts_Zdepl"]->fill(7,weight);
    }
    
    void finalize() {
      double scalefactor(crossSection()/sumOfWeights());
      for (std::map<std::string,AIDA::IHistogram1D *>::iterator 
	     hit=_histos.begin(); hit!=_histos.end();hit++) 
	scale(hit->second,scalefactor);
    }
  };

  
  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_WH_ATLAS);
}
