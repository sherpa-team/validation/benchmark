(leppts){
  if (DIFFS) PIECE_SETUP LepPT.C (dptee){ }(dptee);
  else PIECE_SETUP LepPT.C (uptee){ }(uptee);
  // if (DIFFS) PIECE_SETUP LepPT.C (dptees){ }(dptees);
  // else PIECE_SETUP LepPT.C (uptees){ }(uptees);
  if (DIFFS) PIECE_SETUP LepPT.C (dptep){ }(dptep);
  else PIECE_SETUP LepPT.C (ptep){ }(ptep);
}(leppts);

(dptee){
  PIECE_SETUP LepPT.C (ptee){ }(ptee);
  PIECE_SETUP LepPT.C (pteed){ }(pteed);
  @@ DIFFDR true; @@ OBS pt_ee; @@ REFNUM 0; @@ RESIZEDRP 100 2, 250 4;
  @@ HNAMEPART A; @@ RFAC 1;
  X_MIN 0; X_MAX 200;
}(dptee);
(uptee){
  PIECE_SETUP LepPT.C (ptee){ }(ptee);
  @@ OBS pt_ee; @@ REFNUM 0; @@ RESIZEDRP 100 2, 250 4;
  @@ HNAMEPART A; @@ RFAC 1;
  X_MIN 0; X_MAX 200;
}(uptee);

(dptees){
  PIECE_SETUP LepPT.C (ptee){ }(ptee);
  PIECE_SETUP LepPT.C (pteed){ }(pteed);
  @@ DIFFDR true; @@ OBS pt_ee_s; @@ REFNUM 0; @@ RESIZEDRP 100 2, 250 4;
  @@ HNAMEPART B; @@ RFAC 1e3;
  X_MIN 0.0; X_MAX 25;
//   X_SCALING Log_B_10;
//   LEG_LEFT 0.25;
//   LEG_RIGHT 0.5;
//   LEG_TOP 0.4
}(dptees);
(uptees){
  PIECE_SETUP LepPT.C (ptee){ }(ptee);
  @@ OBS pt_ee_s; @@ REFNUM 0; @@ RESIZEDRP 100 2, 250 4;
  @@ HNAMEPART B; @@ RFAC 1e4;
  X_MIN 0.0; X_MAX 25;
}(uptees);

(ptee){ 
  PIECE_SETUP Paths.C (paths){ }(paths);
  FILE_PIECE PTnu_ee+_HNAMEPART;
  HISTOGRAM_NAME PT_ee_D;
  Y_MIN 1.01e-2*RFAC; Y_MAX 1000;
  X_AXIS_TITLE P_{T, e^{+ }#nu_{e} } #left[ GeV #right];
  Y_AXIS_TITLE d#sigma/dP_{T, e^{+ }#nu_{e} } #left[ pb/GeV #right];
  Y_SCALING Log_B_10;
  LEG_LEFT 0.6;
  LEG_RIGHT 0.9;
  if (DIFFDR==true) {
    BOTTOM_MARGIN 0.3;
    X_TITLE_SIZE 0;
    X_AXIS_LABEL_SIZE 0;
  }
  DRAW_LATEX  SHERPA | SIZE 0.04 COLOUR 19 LEFT  0.15 TOP 0.05 PRIORITY -10 \;;
  DRAW YES;
  WEBPAGE_CAPTION P<sub>T,e+&nu\;</sub>;
}(ptee);
(pteed){
  PIECE_SETUP DPaths.C (dpaths){ }(dpaths);
  FILE_PIECE PTnu_ee+_HNAMEPART;
  HISTOGRAM_NAME PT_ee_D;
  Y_MIN -0.45; Y_MAX 0.45;
  X_AXIS_TITLE P_{T, e^{+ }#nu_{e} } #left[ GeV #right];
  Y_AXIS_TICK_LENGTH 0.08;
  Y_AXIS_NDIVISIONS 000505;
  TOP_MARGIN 0.7;
  DRAW_LINE H 0 | STYLE 1 COLOUR 12 PRIORITY -10 \;;
  @@ RESIZEDRF true;
  DRAW YES;
  DIFF_PLOT YES;
}(pteed);

(dptem){
  PIECE_SETUP LepPT.C (ptem){ }(ptem);
  PIECE_SETUP LepPT.C (ptemd){ }(ptemd);
  @@ DIFFDR true; @@ OBS pt_e-; @@ REFNUM 0; @@ RESIZEDRP 100 2, 250 4;
  X_MIN 0; X_MAX 300;
}(dptem);

(ptem){ 
  PIECE_SETUP Paths.C (paths){ }(paths);
  FILE_PIECE PTnu_e;
  HISTOGRAM_NAME PT_em_D;
  Y_MIN 1.01e-3; Y_MAX 1000;
  X_AXIS_TITLE P_{T, #nu_{e} } #left[ GeV #right];
  Y_AXIS_TITLE d#sigma/dP_{T, #nu_{e} } #left[ pb/GeV #right];
  Y_SCALING Log_B_10;
  X_MIN 0; X_MAX 300;
  if (DIFFDR==true) {
    BOTTOM_MARGIN 0.3;
    X_TITLE_SIZE 0;
    X_AXIS_LABEL_SIZE 0;
  }
  DRAW_LATEX  SHERPA | SIZE 0.04 COLOUR 19 LEFT  0.15 TOP 0.05 PRIORITY -10 \;;
  @@ DRAWCUT false;
  DRAW YES;
  WEBPAGE_CAPTION P<sub>T,&nu\;</sub>;
}(ptem);
(ptemd){
  PIECE_SETUP DPaths.C (dpaths){ }(dpaths);
  FILE_PIECE PTnu_e;
  HISTOGRAM_NAME PT_em_D;
  Y_MIN -0.42; Y_MAX 0.42;
  X_AXIS_TITLE P_{T, #nu_{e} } #left[ GeV #right];
  Y_AXIS_TICK_LENGTH 0.08;
  Y_AXIS_NDIVISIONS 000505;
  TOP_MARGIN 0.7;
  DRAW_LINE H 0 | STYLE 1 COLOUR 12 PRIORITY -10 \;;
  @@ RESIZEDRF true; @@ DRAWCUT false;
  DRAW YES;
  DIFF_PLOT YES;
}(ptemd);

(dptep){
  PIECE_SETUP LepPT.C (ptep){ }(ptep);
  PIECE_SETUP LepPT.C (ptepd){ }(ptepd);
  @@ DIFFDR true; @@ OBS pt_e+; @@ REFNUM 0; @@ RESIZEDRP 100 2, 250 4;
  X_MIN 0; X_MAX 300;
}(dptep);

(ptep){ 
  PIECE_SETUP Paths.C (paths){ }(paths);
  FILE_PIECE PTe+;
  HISTOGRAM_NAME PT_ep_D;
  Y_MIN 1.01e-4; Y_MAX 1000;
  X_AXIS_TITLE P_{T, e^{+ } } #left[ GeV #right];
  Y_AXIS_TITLE d#sigma/dP_{T, e^{+ } } #left[ pb/GeV #right];
  Y_SCALING Log_B_10;
  X_MIN 0; X_MAX 300;
  if (DIFFDR==true) {
    BOTTOM_MARGIN 0.3;
    X_TITLE_SIZE 0;
    X_AXIS_LABEL_SIZE 0;
  }
  DRAW_LATEX  SHERPA | SIZE 0.04 COLOUR 19 LEFT  0.15 TOP 0.05 PRIORITY -10 \;;
  @@ DRAWCUT false;
  DRAW YES;
  WEBPAGE_CAPTION P<sub>T,e+</sub>;
}(ptep);
(ptepd){
  PIECE_SETUP DPaths.C (dpaths){ }(dpaths);
  FILE_PIECE PTe+;
  HISTOGRAM_NAME PT_ep_D;
  Y_MIN -0.42; Y_MAX 0.42;
  X_AXIS_TITLE P_{T, e^{+ } } #left[ GeV #right];
  Y_AXIS_TICK_LENGTH 0.08;
  Y_AXIS_NDIVISIONS 000505;
  TOP_MARGIN 0.7;
  DRAW_LINE H 0 | STYLE 1 COLOUR 12 PRIORITY -10 \;;
  @@ RESIZEDRF true; @@ DRAWCUT false;
  DRAW YES;
  DIFF_PLOT YES;
}(ptepd);
