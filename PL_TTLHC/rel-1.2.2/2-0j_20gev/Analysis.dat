BEGIN_ANALYSIS {

 LEVEL Shower

 // jets for control sample
 Trigger {
  InList FinalState;
  OutList KTJetsWT;
  Finder 93 20 -4.5 4.5 1.0 -1;
  Qual !KF(24);
 }

 // jets for analysis
 Trigger {
  InList FinalState;
  OutList KTJetsW;
  Finder 93 20 -4.5 4.5 1.0 0;
  Qual !KF(24);
 }

 // global observables
 PT4 -24 24 97 97 0 1000 200 Lin KTJetsW
 PT4 -24 24 97 97 0.1 1000 200 Log KTJetsW

 // boson observables
 DPhi -24 24 0 3.2 64 Lin FinalState
 DEta -24 24 0 5 100 Lin FinalState
 DR -24 24 0 6 100 Lin FinalState

 PT2 -24 24 0 1000 250 Lin FinalState
 Mass -24 24 0 1000 200 Lin FinalState

 Eta -24 -5 5 200 Lin FinalState
 PT -24 0 1000 200 Lin FinalState

 // jet rates
 JetDRate  1.0e-1  1.0e3 400 1 1 5 Log KTJetsW

 // jet observables
 OneEta 93 0 -5 5 200 Lin KTJetsW
 OneEta 93 1 -5 5 200 Lin KTJetsW
 OnePT 93 0 0 250 250 Lin KTJetsW
 OnePT 93 1 0 250 250 Lin KTJetsW
 OnePhi 93 0 0 3.2 128 Lin KTJetsW
 OnePhi 93 1 0 3.2 128 Lin KTJetsW
 TwoDPhi 93 0 93 1 0 3.2 200 Lin KTJetsW
 TwoDR 93 0 93 1 0 6 150 Lin KTJetsW

 // jet observables control sample
 OneEta 93 0 -5 5 200 Lin KTJetsWT
 OneEta 93 1 -5 5 200 Lin KTJetsWT
 OnePT 93 0 0 250 250 Lin KTJetsWT
 OnePT 93 1 0 250 250 Lin KTJetsWT
 OnePhi 93 0 0 3.2 128 Lin KTJetsWT
 OnePhi 93 1 0 3.2 128 Lin KTJetsWT
 TwoDPhi 93 0 93 1 0 3.2 200 Lin KTJetsWT
 TwoDR 93 0 93 1 0 6 150 Lin KTJetsWT

 // bjet observables
 OneEta 97 0 -5 5 200 Lin KTJetsW
 OnePT 97 0 0 500 250 Lin KTJetsW
 TwoDPhi 97 0 97 1 0 3.2 64 Lin KTJetsW
 TwoDEta 97 0 97 1 0 5 100 Lin KTJetsW
 TwoDR 97 0 97 1 0 6 100 Lin KTJetsW
 TwoPT 97 0 97 1 0 1000 250 Lin KTJetsW
 TwoMass 97 0 97 1 0 1000 200 Lin KTJetsW

 // bjet observables control sample
 OneEta 97 0 -5 5 200 Lin KTJetsWT
 OnePT 97 0 0 500 250 Lin KTJetsWT
 TwoDPhi 97 0 -97 0 0 3.2 64 Lin KTJetsWT
 TwoDEta 97 0 -97 0 0 5 100 Lin KTJetsWT
 TwoDR 97 0 -97 0 0 6 100 Lin KTJetsWT
 TwoPT 97 0 -97 0 0 1000 250 Lin KTJetsWT
 TwoMass 97 0 -97 0 0 1000 200 Lin KTJetsWT

 // boson-jet observables
 TwoDPhi 24 0 97 0 0 3.2 64 Lin KTJetsW
 TwoDEta 24 0 97 0 0 5 100 Lin KTJetsW
 TwoDR 24 0 97 0 0 6 100 Lin KTJetsW
 TwoPT 24 0 97 0 0 1000 250 Lin KTJetsW
 TwoMass 24 0 97 0 0 600 150 Lin KTJetsW

 // boson-jet observables control sample
 TwoDPhi 24 0 97 0 0 3.2 64 Lin KTJetsWT
 TwoDEta 24 0 97 0 0 5 100 Lin KTJetsWT
 TwoDR 24 0 97 0 0 6 100 Lin KTJetsWT
 TwoPT 24 0 97 0 0 1000 250 Lin KTJetsWT
 TwoMass 24 0 97 0 0 600 150 Lin KTJetsWT

 // angles
 FourPlaneAngle 24 0 -24 0 97 0 97 1 0 3.2 128 Lin KTJetsW
 FourPlaneAngle 24 0 97 0 -24 0 97 1 0 3.2 128 Lin KTJetsW
 FourPlaneAngle 24 0 97 1 -24 0 97 0 0 3.2 128 Lin KTJetsW

 // angles control sample
 FourPlaneAngle 24 0 -24 0 97 0 -97 0 0 3.2 128 Lin KTJetsWT
 FourPlaneAngle 24 0 97 0 -24 0 -97 0 0 3.2 128 Lin KTJetsWT
 FourPlaneAngle 24 0 -97 0 -24 0 97 0 0 3.2 128 Lin KTJetsWT

} END_ANALYSIS
