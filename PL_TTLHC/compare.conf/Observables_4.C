(observables4){
  PIECE_SETUP Observables_4.C (KTJets_KtJetrates(1)jet_1_1_0.dat){ }(KTJets_KtJetrates(1)jet_1_1_0.dat);
  PIECE_SETUP Observables_4.C (KTJets_KtJetrates(1)jet_1_1_1.dat){ }(KTJets_KtJetrates(1)jet_1_1_1.dat);
  PIECE_SETUP Observables_4.C (KTJets_KtJetrates(1)jet_1_1_2.dat){ }(KTJets_KtJetrates(1)jet_1_1_2.dat);
  PIECE_SETUP Observables_4.C (KTJets_KtJetrates(1)jet_1_1_3.dat){ }(KTJets_KtJetrates(1)jet_1_1_3.dat);
  @@ SRPAR 1; @@ RPARS RESIZE_BINS -1 2;
  @@ DRAWLCUT true;
}(observables4);

(KTJets_KtJetrates(1)jet_1_1_0.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE KTJetsW_KtJetrates(1)jet_1_1_0.dat;
  X_MIN 1.6;
  X_MAX 2.8;
  @@ RYMIN 1.001e-3;
  @@ RYMAX 10.01;
  @@ YSCALING Log_B_10;
  LEG_TOP 0.95;
  LEG_LEFT 0.65;
  LEG_RIGHT 0.8;
  X_AXIS_TITLE log(Q_{1#rightarrow 0}/GeV);
  Y_AXIS_TITLE 1/#sigma d#sigma/dlog(Q_{1#rightarrow 0}/GeV) #left[ pb #right];
  HISTOGRAM_NAME KTJets_KtJetrates_1_j_1_1_0;
  FIGURE_CAPTION Differential 1-jet rate;
}(KTJets_KtJetrates(1)jet_1_1_0.dat);

(KTJets_KtJetrates(1)jet_1_1_1.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE KTJetsW_KtJetrates(1)jet_1_1_1.dat;
  X_MIN 1.4;
  X_MAX 2.8;
  @@ RYMIN 1.001e-4;
  @@ RYMAX 10.01;
  @@ YSCALING Log_B_10;
  LEG_TOP 0.95;
  LEG_LEFT 0.65;
  LEG_RIGHT 0.8;
  X_AXIS_TITLE log(Q_{2#rightarrow 1}/GeV);
  Y_AXIS_TITLE 1/#sigma d#sigma/dlog(Q_{2#rightarrow 1}/GeV) #left[ pb #right];
  HISTOGRAM_NAME KTJets_KtJetrates_1_j_1_1_1;
  FIGURE_CAPTION Differential 2-jet rate;
}(KTJets_KtJetrates(1)jet_1_1_1.dat);

(KTJets_KtJetrates(1)jet_1_1_2.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE KTJetsW_KtJetrates(1)jet_1_1_2.dat;
  X_MIN 0.6;
  X_MAX 2.6;
  @@ RYMIN 1.01e-3;
  @@ RYMAX 10.1;
  @@ YSCALING Log_B_10;
  LEG_TOP 0.95;
  LEG_LEFT 0.65;
  LEG_RIGHT 0.8;
  @@ DLYCUT true;
  X_AXIS_TITLE log(Q_{3#rightarrow 2}/GeV);
  Y_AXIS_TITLE 1/#sigma d#sigma/dlog(Q_{3#rightarrow 2}/GeV) #left[ pb #right];
  HISTOGRAM_NAME KTJets_KtJetrates_1_j_1_1_2;
  FIGURE_CAPTION Differential 3-jet rate;
}(KTJets_KtJetrates(1)jet_1_1_2.dat);

(KTJets_KtJetrates(1)jet_1_1_3.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE KTJetsW_KtJetrates(1)jet_1_1_3.dat;
  X_MIN 0.6;
  X_MAX 2.4;
  @@ RYMIN 1.01e-3;
  @@ RYMAX 10.1;
  @@ YSCALING Log_B_10;
  LEG_TOP 0.95;
  LEG_LEFT 0.65;
  LEG_RIGHT 0.8;
  @@ DLYCUT true;
  X_AXIS_TITLE log(Q_{4#rightarrow 3}/GeV);
  Y_AXIS_TITLE 1/#sigma d#sigma/dlog(Q_{4#rightarrow 3}/GeV) #left[ pb #right];
  HISTOGRAM_NAME KTJets_KtJetrates_1_j_1_1_3;
  FIGURE_CAPTION Differential 4-jet rate;
}(KTJets_KtJetrates(1)jet_1_1_3.dat);

