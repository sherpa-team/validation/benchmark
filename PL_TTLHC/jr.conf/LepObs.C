(lepobs){
  PIECE_SETUP LepPT.C (leppts){ }(leppts);
  PIECE_SETUP LepEta.C (lepetas){ }(lepetas);
  if (DIFFS) PIECE_SETUP LepObs.C (dmee){ }(dmee);
  else PIECE_SETUP LepObs.C (umee){ }(umee);
  @@ JMODE 0; @@ JETCONTS 0;
  Y_TITLE_OFFSET 1.4;
  X_TITLE_OFFSET 1.1;
  LEFT_MARGIN 0.15;
}(lepobs);

(dmee){
  PIECE_SETUP LepObs.C (mee){ }(mee);
  PIECE_SETUP LepObs.C (meed){ }(meed);
  @@ DIFFDR true; @@ OBS m_ee; @@ REFNUM 0;
  X_MIN 55; X_MAX 105;
}(dmee);
(umee){
  PIECE_SETUP LepObs.C (mee){ }(mee);
  @@ OBS m_ee; @@ REFNUM 0;
  X_MIN 55; X_MAX 105;
}(umee);

(mee){ 
  PIECE_SETUP Paths.C (paths){ }(paths);
  FILE_PIECE Massnu_ee+;
  HISTOGRAM_NAME M_ee_D;
  Y_MIN 1.01; Y_MAX 1.01e4;
  X_AXIS_TITLE m_{e^{+ }#nu_{e} } #left[ GeV #right];
  Y_AXIS_TITLE d#sigma/dm_{e^{+ }#nu_{e} } #left[ pb/GeV #right];
  Y_SCALING Log_B_10;
  if (DIFFDR==true) {
    BOTTOM_MARGIN 0.3;
    X_TITLE_SIZE 0;
    X_AXIS_LABEL_SIZE 0;
  }
  DRAW_LATEX  SHERPA | SIZE 0.04 COLOUR 19 LEFT  0.15 TOP 0.05 PRIORITY -10 \;;
  DRAW YES;
  WEBPAGE_CAPTION M<sub>e+e-</sub>;
  @@ DRAWCUT false;
}(mee);
(meed){
  PIECE_SETUP DPaths.C (dpaths){ }(dpaths);
  FILE_PIECE Massnu_ee+;
  HISTOGRAM_NAME M_ee_D;
  Y_MIN -0.21; Y_MAX 0.21;
  X_AXIS_TITLE m_{e^{+ }#nu_{e} } #left[ GeV #right];
  Y_AXIS_TICK_LENGTH 0.08;
  Y_AXIS_NDIVISIONS 000505;
  TOP_MARGIN 0.7;
  DRAW_LINE H 0 | STYLE 1 COLOUR 12 PRIORITY -10 \;;
  @@ RESIZEDRF true; @@ DRAWCUT false;
  DRAW YES;
  DIFF_PLOT YES;
}(meed);

