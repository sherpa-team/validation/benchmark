(observables1){
  PIECE_SETUP Observables_1.C (DR[2[+1]3].dat){ }(DR[2[+1]3].dat);
  PIECE_SETUP Observables_1.C (PTttb.dat){ }(PTttb.dat);
  PIECE_SETUP Observables_1.C (lPTttb.dat){ }(lPTttb.dat);
  PIECE_SETUP Observables_1.C (PT){ }(PT);
  PIECE_SETUP Observables_1.C (M){ }(M);
  PIECE_SETUP Observables_1.C (DY){ }(DY);
  X_TITLE_OFFSET 1.1; Y_TITLE_OFFSET 1.4;
  LEFT_MARGIN 0.125;
}(observables1);

(DR2.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE AllJets_FinalState_KtJetrates(1)jet_1_1_2.dat;
  X_MIN 0.35897; X_MAX 2.35897;
  @@ YMIN 1.1e-3; @@ YMAX 10; @@ YSCALING Log_B_10;
  X_AXIS_TITLE log(d_{23}/GeV);
  Y_AXIS_TITLE d#sigma/dlog(d_{23}/GeV) [pb];
  WEBPAGE_CAPTION log(d<sub>23</sub>);
  HISTOGRAM_NAME logd_23;
  LEG_LEFT 0.15; LEG_RIGHT 0.4;
  LEG_TOP 0.6; LEG_DELTA_Y 0.1;
}(DR2.dat);
(DR3.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE AllJets_FinalState_KtJetrates(1)jet_1_1_3.dat;
  X_MIN 0.35897; X_MAX 2.15897;
  @@ YMIN 1.1e-3; @@ YMAX 10; @@ YSCALING Log_B_10;
  X_AXIS_TITLE log(d_{34}/GeV);
  Y_AXIS_TITLE d#sigma/dlog(d_{34}/GeV) [pb];
  WEBPAGE_CAPTION log(d<sub>34</sub>);
  HISTOGRAM_NAME logd_34;
  LEG_LEFT 0.15; LEG_RIGHT 0.4;
  LEG_TOP 0.6; LEG_DELTA_Y 0.1;
}(DR3.dat);  

(PTttb.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE PTttb.dat; X_MIN 0; X_MAX 400;
  @@ YMIN 1.1e-6; @@ YMAX 1; @@ YSCALING Log_B_10;
  X_AXIS_TITLE p_{T,t#bar{t}} [GeV];
  Y_AXIS_TITLE d#sigma/dp_{T,t#bar{t}} [pb/GeV];
  WEBPAGE_CAPTION p<sub>T,tt</sub>;
  HISTOGRAM_NAME pT_tt;
  @@ RDPARS RESIZE_BINS 25 2, 50 4, 100 8, 200 16\;;
}(PTttb.dat);
(lPTttb.dat){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE PTttb_A.dat; X_MIN 0; X_MAX 3;
  @@ YMIN 1.1e-3; @@ YMAX 10; @@ YSCALING Log_B_10;
  X_AXIS_TITLE log(p_{T,t#bar{t}}/GeV);
  Y_AXIS_TITLE d#sigma/dlog(p_{T,t#bar{t}}/GeV) [pb];
  WEBPAGE_CAPTION log p<sub>T,tt</sub>;
  HISTOGRAM_NAME lpT_tt;
}(lPTttb.dat);


(PT){
  PIECE_SETUP Observables_1.C (FW){ }(FW);
  PIECE_SETUP Observables_1.C (BW){ }(BW);
  PIECE_SETUP Observables_1.C (Asymm){ }(Asymm);
  @@ DOBS PT; X_MIN 0; X_MAX 70;
  @@ YMSELF 1.1e-3; @@ YPSELF 1; @@ YSSELF Log_B_10;
  @@ YMASYM -0.3; @@ YPASYM 0.3;
  @@ NOBS p_{T,t#bar{t}}; X_AXIS_TITLE NOBS [GeV];
  @@ YTITLE d#sigma/dp_{T,t#bar{t}} [pb/GeV];
  DRAW_LINE H 0 | STYLE 2\;;
  @@ WTITLE p<sub>T,tt</sub>; @@ HNAME pT_tt;
}(PT);
(M){
  PIECE_SETUP Observables_1.C (FW){ }(FW);
  PIECE_SETUP Observables_1.C (BW){ }(BW);
  PIECE_SETUP Observables_1.C (Asymm){ }(Asymm);
  @@ DOBS m; X_MIN 350; X_MAX 750;
  @@ YMSELF 1.1e-4; @@ YPSELF 0.1; @@ YSSELF Log_B_10;
  @@ YMASYM 0; @@ YPASYM 0.6;
  @@ NOBS m_{t#bar{t}}; X_AXIS_TITLE NOBS;
  @@ YTITLE d#sigma/dm_{t#bar{t}} [pb];
  @@ WTITLE m<sub>tt</sub>; @@ HNAME m_tt;
}(M);
(DY){
  PIECE_SETUP Observables_1.C (FW){ }(FW);
  PIECE_SETUP Observables_1.C (BW){ }(BW);
  PIECE_SETUP Observables_1.C (Asymm){ }(Asymm);
  @@ DOBS Calc; X_MIN 0; X_MAX 2;
  @@ YMSELF 1e-3; @@ YPSELF 5; @@ YSSELF Id;
  @@ YMASYM 0; @@ YPASYM 0.6;
  @@ NOBS #Delta y_{t#bar{t}}; X_AXIS_TITLE NOBS;
  @@ YTITLE d#sigma/d#Delta y_{t#bar{t}} [pb];
  @@ WTITLE &Delta\;y<sub>tt</sub>; @@ HNAME Dy_tt;
}(DY);


(FW){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE FSFW_0_DOBS_t0_tb0.dat;
  @@ YMIN YMSELF; @@ YMAX YPSELF; @@ YSCALING YSSELF;
  Y_AXIS_TITLE YTITLE (#Delta y_{t#bar{t}}>0);
  WEBPAGE_CAPTION WTITLE (&Delta\;y<sub>tt</sub>&gt\;0);
  HISTOGRAM_NAME HNAME_fw;
}(FW);
(BW){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE FSBW_0_DOBS_t0_tb0.dat;
  @@ YMIN YMSELF; @@ YMAX YPSELF; @@ YSCALING YSSELF;
  Y_AXIS_TITLE YTITLE (#Delta y_{t#bar{t}}<0);
  WEBPAGE_CAPTION WTITLE (&Delta\;y<sub>tt</sub>&lt\;0);
  HISTOGRAM_NAME HNAME_bw;
}(BW);
(Asymm){
  PIECE_SETUP Level_0.C (level0){ }(level0);
  FILE_PIECE FSFW_0_DOBS_t0_tb0.dat FSBW_0_DOBS_t0_tb0.dat FSFW_0_DOBS_t0_tb0.dat FSBW_0_DOBS_t0_tb0.dat;
  ## COLUMNS 2 2 5 5; DRAW_OPTION h; @@ ! DJ N;
  X_ERROR_PLUS 3; X_ERROR_MINUS 4; Y_ERROR_PLUS 5; Y_ERROR_MINUS 6;
  X_MIN 0; X_MAX 2; @@ YMIN YMASYM; @@ YMAX YPASYM; @@ YSCALING Id;
  DATA_TYPE ALGEBRA((y[0]-y[1])/(y[0]+y[1]))(abs(y[0]-y[1])/(y[0]+y[1])*(y[2]*abs(2*y[1]/(y[0]*y[0]-y[1]*y[1]))+y[3]*abs(2*y[0]/(y[0]*y[0]-y[1]*y[1]))))(abs(y[0]-y[1])/(y[0]+y[1])*(y[2]*abs(2*y[1]/(y[0]*y[0]-y[1]*y[1]))+y[3]*abs(2*y[0]/(y[0]*y[0]-y[1]*y[1]))));
  Y_AXIS_TITLE A_{FB}(NOBS); @@ DDP N;
  WEBPAGE_CAPTION A<sub>FB</sub>(WTITLE);
  HISTOGRAM_NAME AFB_HNAME;
}(Asymm);

