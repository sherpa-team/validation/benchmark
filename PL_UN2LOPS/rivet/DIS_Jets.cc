// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/DISFinalState.hh"
#include "Rivet/Projections/DISKinematics.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {

  const double q2Bins[7]={150.0, 200.0, 270.0, 400.0, 700.0, 5000.0, 15000.0};

  class DIS_Jets : public Analysis {

  private:

    Histo1DPtr dsig1dQ2dpT[6];
    Histo1DPtr dsig2dQ2dpT[6];
    Histo1DPtr dsig3dQ2dpT[6];

    Histo1DPtr dsig2dQ2dxi[6];
    Histo1DPtr dsig3dQ2dxi[6];

  public:

    DIS_Jets(): Analysis("DIS_Jets") {}

    void init()
    {
      const DISKinematics &kin = DISKinematics();
      const DISFinalState &bfs = DISFinalState(kin, DISFinalState::BREIT);
      VetoedFinalState had(bfs);
      had.addVetoPairId(11);
      const FastJets      &jts = FastJets(had, FastJets::KT, 1.0);

      declare(kin,          "kin");
      declare(bfs,          "bfs");
      declare(had,          "had");
      declare(jts,          "jts");
      declare(DISLepton(),  "lep");

      for (int i(0);i<6;++i) {
	std::string id=std::to_string(i);
	dsig1dQ2dpT[i] = bookHisto1D("dsig1dQ2dpT"+id,std::vector<double>({7.0, 11.0, 18.0, 30.0, 50.0}));
	dsig2dQ2dpT[i] = bookHisto1D("dsig2dQ2dpT"+id,std::vector<double>({7.0, 11.0, 18.0, 30.0, 50.0}));
	dsig3dQ2dpT[i] = bookHisto1D("dsig3dQ2dpT"+id,std::vector<double>({7.0, 11.0, 18.0, 30.0, 50.0}));

	dsig2dQ2dxi[i] = bookHisto1D("dsig2dQ2dxi"+id,std::vector<double>({0.006, 0.02, 0.04, 0.08, 0.316}));
	dsig3dQ2dxi[i] = bookHisto1D("dsig3dQ2dxi"+id,std::vector<double>({0.01, 0.04, 0.08, 0.5}));
      }

    }

    void analyze(const Event& event)
    {
      const double        &wgt = event.weight();
      const DISKinematics &kin = applyProjection<DISKinematics>(event, "kin");
      const DISLepton     &lep = applyProjection<DISLepton>    (event, "lep");
      const Jets          &jts = applyProjection<FastJets>     (event, "jts").
	jetsByPt( (Cuts::pT>5.0) & (Cuts::pT<50.0));


      const FourMomentum& kprime = lep.out().momentum();
      const FourMomentum& k      = lep.in() .momentum();
      const double&       Q2     = kin.Q2();
      const double&       x      = kin.x();
      const double&       y      = kin.y();


      // Electron polar angle calculated as in H1_2000_S4129130
      const double thel = 180 - kprime.angle(k)/degree;
      const double enel = kprime.E();


      // Lepton selection
      if (enel < 11.0) vetoEvent;


      // LAr calo coverage. TODO: in actual analysis, theta is
      // restricted to the 98% trigger efficiency area on the LAr calo
      // which amounts to 90% of the entire calo.
      if (!inRange(thel,4.0, 154.0)) vetoEvent;


      // DIS measurement phase space
      if (!inRange(y,  0.2,  0.7   )) vetoEvent;
      if (!inRange(Q2, 150., 15000.)) vetoEvent;


      Jets jets; // part of jet selection is in terms of lab frame kinematics
      const LorentzTransform labboost = kin.boostBreit().inverse();
      foreach (const Jet& j, jts)
	{
	  const FourMomentum labmom = labboost.transform(j.momentum());
	  if (!inRange(labmom.pseudorapidity(), -1.0, 2.5)) continue;
	  if (labmom.pT() < 2.5) continue;
	  jets.push_back(j);
	}


      double M12  = jets.size()>1 ? (jets[0].momentum()+jets[1].momentum()).mass() : 0.0;
      double M123 = jets.size()>2 ? (jets[0].momentum()+jets[1].momentum()).mass() : 0.0;


      if(jets.size() >= 1 && jets[0].pT() > 7.0) // Inclusive jets
	{
	  MSG_DEBUG(jets[0].pT());
	  for (int i(0);i<6;++i)
	    if (Q2>=q2Bins[i] && Q2<q2Bins[i+1]) {
	      dsig1dQ2dpT[i]->fill(jets[0].pT(), wgt);
	    }
	}


      if(jets.size() >= 2 && M12 > 16.0) // Dijet events
	{
	  double ptmean = 0.5*(jets[0].pT() + jets[1].pT());
	  double xi     = x * (1.0 + M12*M12/Q2);

	  for (int i(0);i<6;++i)
	    if (Q2>=q2Bins[i] && Q2<q2Bins[i+1]) {
	      dsig2dQ2dpT[i]->fill(ptmean, wgt);
	      dsig2dQ2dxi[i]->fill(xi,     wgt);
	    }
	}


      if(jets.size() >= 3 && M12 > 16.0) // Trijet events (note cut on M12 and NOT on M123)
	{
	  double ptmean = 1.0/3.0*(jets[0].pT() + jets[1].pT() + jets[2].pT());
	  double xi     = x * (1.0 + M123*M123/Q2);

	  for (int i(0);i<6;++i)
	    if (Q2>=q2Bins[i] && Q2<q2Bins[i+1]) {
	      dsig3dQ2dpT[i]->fill(ptmean, wgt);
	      dsig3dQ2dxi[i]->fill(xi,     wgt);
	    }
	}
      
    }

    void finalize()
    {
      const double& wgt = crossSection()/picobarn/sumOfWeights();
      for (int i(0);i<6;++i) {
	scale(dsig1dQ2dpT[i], wgt/(q2Bins[i+1]-q2Bins[i]));
	scale(dsig2dQ2dpT[i], wgt/(q2Bins[i+1]-q2Bins[i]));
	scale(dsig3dQ2dpT[i], wgt/(q2Bins[i+1]-q2Bins[i]));
      }
    }


  };

  DECLARE_RIVET_PLUGIN(DIS_Jets);

}
