(initial){
  PIECE_SETUP ep_ej_nlo.C (SigR){ }(SigR);
  @@ REFN NLO; @@ REFP RUN_PATH/ep_ej_nlo/ANA_PATH_sum/INCLU;
  @@ N2LONAME nnlo; @@ NLONAME nlo;
}(initial);
(SigR){
  PIECE_SETUP ep_ej_nlo.C (xQ2){ }(xQ2);
  RIGHT_AXIS YES; TOP_AXIS YES;
  LEFT_MARGIN 0.15; Y_TITLE_OFFSET 1.6; LEG_DELTA_Y 0.05;
  DEFINE_COLOURS VIOLET1 195 0 185,;
  DEFINE_COLOURS BLUE1 19 88 144,;
  DEFINE_COLOURS GREEN1 30 145 35,;
  DEFINE_COLOURS YELLOW1 255 175 0,;
  DEFINE_COLOURS RED1 195 15 15,;
  DEFINE_COLOURS CYAN1 0 138 255,;
  DEFINE_COLOURS ORANGE1 255 75 0,;
  DEFINE_COLOURS BROWN1 212 120 0,;
  DEFINE_COLOURS STEELBLUE1 99 184 255,;
  DEFINE_COLOURS STEELBLUE2 92 178 231,;
  DEFINE_COLOURS STEELBLUE3 79 148 204,;
  DEFINE_COLOURS LTGRAY 150 150 150 0.5,;
}(SigR);

(xQ2){
  PIECE_SETUP ep_ej_nlo.C (xQ2[0[+1]19]){ }(xQ2[0[+1]19]);
  X_MIN 0.7; X_MAX 5; @@ RZB N; @@ RZPAR -7.5 2;
  @@ YMIN 0; @@ YMAX 4; @@ YSCALING Id;
  X_AXIS_TITLE log_{10}(Q^{2}/GeV^{2});
  @@ YTITLE #sigma_{r}(Q^{2});
  @@ LLEFT 0.29; @@ LRIGHT 0.52; @@ LTOP 0.375;
  @@ DRAWSTUFF A; @@ DYRANGE 0.15;
}(xQ2);
(xQ20){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h0_sigr.dat; HISTOGRAM_NAME xQ2_0;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=5e-5);
}(xQ20);
(xQ21){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h1_sigr.dat; HISTOGRAM_NAME xQ2_1;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=8e-5);
}(xQ21);
(xQ22){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h2_sigr.dat; HISTOGRAM_NAME xQ2_2;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=1.3e-4);
}(xQ22);
(xQ23){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h3_sigr.dat; HISTOGRAM_NAME xQ2_3;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=2e-4);
}(xQ23);
(xQ24){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h4_sigr.dat; HISTOGRAM_NAME xQ2_4;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=3.2e-4);
}(xQ24);
(xQ25){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h5_sigr.dat; HISTOGRAM_NAME xQ2_5;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=5e-4);
}(xQ25);
(xQ26){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h6_sigr.dat; HISTOGRAM_NAME xQ2_6;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=8e-4);
}(xQ26);
(xQ27){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h7_sigr.dat; HISTOGRAM_NAME xQ2_7;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=1.3e-3);
}(xQ27);
(xQ28){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h8_sigr.dat; HISTOGRAM_NAME xQ2_8;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=2e-3);
}(xQ28);
(xQ29){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h9_sigr.dat; HISTOGRAM_NAME xQ2_9;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=3.2e-3);
}(xQ29);
(xQ210){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h10_sigr.dat; HISTOGRAM_NAME xQ2_10;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=5e-3);
}(xQ210);
(xQ211){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h11_sigr.dat; HISTOGRAM_NAME xQ2_11;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=8e-3);
}(xQ211);
(xQ212){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h12_sigr.dat; HISTOGRAM_NAME xQ2_12;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=1.3e-2);
}(xQ212);
(xQ213){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h13_sigr.dat; HISTOGRAM_NAME xQ2_13;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=2e-2);
}(xQ213);
(xQ214){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h14_sigr.dat; HISTOGRAM_NAME xQ2_14;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=3.2e-2);
}(xQ214);
(xQ215){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h15_sigr.dat; HISTOGRAM_NAME xQ2_15;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=5e-2);
}(xQ215);
(xQ216){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h16_sigr.dat; HISTOGRAM_NAME xQ2_16;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=8e-2);
}(xQ216);
(xQ217){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h17_sigr.dat; HISTOGRAM_NAME xQ2_17;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=1.3e-1);
}(xQ217);
(xQ218){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h18_sigr.dat; HISTOGRAM_NAME xQ2_18;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=2e-1);
}(xQ218);
(xQ219){
  PIECE_SETUP ep_ej_nlo.C (level1){ }(level1);
  FILE_PIECE xQ2__FinalState_h19_sigr.dat; HISTOGRAM_NAME xQ2_19;
  WEBPAGE_CAPTION &sigma\;<sub>r</sub>(Q<sup>2</sup>,x<sub>B</sub>=3.2e-1);
}(xQ219);

(level1){
  PIECE_SETUP ep_ej_nlo.C (paths){ }(paths);
  PIECE_SETUP ep_ej_nlo.C (dpaths){ }(dpaths);
}(level1);

(paths){
  PIECE_SETUP ep_ej_nlo.C (nnlo){ }(nnlo);
  PIECE_SETUP ep_ej_nlo.C (nlo){ }(nlo);
  if (NOHNNLO!="Y") PIECE_SETUP ep_ej_nlo.C (dynnlo){ }(dynnlo);
  DRAW YES;
  BOTTOM_MARGIN 0.3; Y_AXIS_TITLE YTITLE;
  X_AXIS_LABEL_SIZE 0; X_TITLE_SIZE 0;
  Y_MIN YMIN; Y_MAX YMAX; Y_SCALING YSCALING;
}(paths);
(dynnlo){
  PIECE_SETUP ep_ej_nlo.C (central){ }(central);
  PATH_PIECE apfel/; @@ LCOLOR GREEN1; @@ LTITLE Apfel;
  @@ ! RZB N; @@ LPDFNUM LHAPDFNUM2;
}(dynnlo);
(nnlo){
  PIECE_SETUP ep_ej_nlo.C (central){ }(central);
  PIECE_SETUP ep_ej_nlo.C (down){ }(down);
  PIECE_SETUP ep_ej_nlo.C (up){ }(up);
  PIECE_SETUP ep_ej_nlo.C (band){ }(band);
  PATH_PIECE RUN_PATH/ep_ej_N2LONAME/ANA_PATH_sum/INCLU/; @@ LCOLOR 1; @@ BSTYLE 1000; @@ BCOLOR LTGRAY;
  @@ LTITLE NNLO; @@ LPDFNUM LHAPDFNUM2;
}(nnlo);
(nlo){
  PIECE_SETUP ep_ej_nlo.C (central){ }(central);
  PIECE_SETUP ep_ej_nlo.C (down){ }(down);
  PIECE_SETUP ep_ej_nlo.C (up){ }(up);
  PIECE_SETUP ep_ej_nlo.C (band){ }(band);
  PATH_PIECE RUN_PATH/ep_ej_NLONAME/ANA_PATH_sum/INCLU/; @@ LCOLOR BLUE1; @@ BSTYLE 3013; @@ BCOLOR STEELBLUE3;
  @@ LTITLE NLO; @@ LPDFNUM LHAPDFNUM1;
}(nlo);
(central){
  PIECE_SETUP ep_ej_nlo.C (level3){ }(level3);
  PATH_PIECE MUR1_MUF1_PDFLPDFNUM/; LEGEND_TITLE LTITLE;
  if (RZB=="Y") ## RESIZE_BINS RZPAR;
  LINE_COLOUR LCOLOR; DRAW_PRIORITY 10;
  LEG_LEFT LLEFT; LEG_RIGHT LRIGHT; LEG_TOP LTOP;
  DRAW_LATEX #sqrt{s} = 300 GeV | LEFT 0.05 TOP 0.9 COLOUR 1 ALIGN 12 SIZE 0.035 PRIORITY 20\;;
  // if (DRAWSTUFF=="B") {
  //   DRAW_LINES P 90,0.67:100,0.86 | STYLE 1 COLOUR STEELBLUE3 FSTYLE 3013 FCOLOUR STEELBLUE3\;;
  //   DRAW_LATEX m_{H }/2< #mu_{R/F}<2m_{H} NLO | LEFT 0.525 TOP 0.66 COLOUR 1 ALIGN 12 SIZE 0.035 PRIORITY 20\;;
  //   DRAW_LINES P 90,0.39:100,0.5 | STYLE 1 COLOUR LTGRAY FSTYLE 1000 FCOLOUR LTGRAY\;;
  //   DRAW_LATEX m_{H }/2< #mu_{R/F}<2m_{H} NNLO | LEFT 0.525 TOP 0.59 COLOUR 1 ALIGN 12 SIZE 0.035 PRIORITY 20\;;
  // }
  // if (DRAWSTUFF=="A") {
  //   DRAW_LINES P -1.3,1.86:-1.7,2.28 | STYLE 1 COLOUR STEELBLUE3 FSTYLE 3013 FCOLOUR STEELBLUE3\;;
  //   DRAW_LATEX m_{H }/2< #mu_{R/F}<2m_{H} NLO | LEFT 0.36 TOP 0.15 COLOUR 1 ALIGN 12 SIZE 0.035 PRIORITY 20\;;
  //   DRAW_LINES P -1.3,1.02:-1.7,1.44 | STYLE 1 COLOUR LTGRAY FSTYLE 1000 FCOLOUR LTGRAY\;;
  //   DRAW_LATEX m_{H }/2< #mu_{R/F}<2m_{H} NNLO | LEFT 0.36 TOP 0.08 COLOUR 1 ALIGN 12 SIZE 0.035 PRIORITY 20\;;
  // }
}(central);
(down){
  PIECE_SETUP ep_ej_nlo.C (level3){ }(level3);
  PATH_PIECE MUR0.5_MUF0.5_PDFLPDFNUM/; DRAW_LEGEND NO;
  if (RZB=="Y") ## RESIZE_BINS RZPAR;
  LINE_COLOUR BCOLOR; DRAW_OPTION hist; LINE_WIDTH 0.1;
}(down);
(up){
  PIECE_SETUP ep_ej_nlo.C (level3){ }(level3);
  PATH_PIECE MUR2_MUF2_PDFLPDFNUM/; DRAW_LEGEND NO;
  if (RZB=="Y") ## RESIZE_BINS RZPAR;
  LINE_COLOUR BCOLOR; DRAW_OPTION hist; LINE_WIDTH 0.1;
}(up);
(band){
  PIECE_SETUP ep_ej_nlo.C (level3){ }(level3);
  PATH_PIECE MUR1_MUF1_PDFLPDFNUM/ MUR0.5_MUF0.5_PDFLPDFNUM/ MUR2_MUF2_PDFLPDFNUM/;
  DATA_TYPE ALGEBRA(y[0])(y[0]-min(y[0],y[1],y[2]))(max(y[0],y[1],y[2])-y[0]);
  X_ERROR_PLUS 3; X_ERROR_MINUS 4; Y_ERROR_PLUS 5; Y_ERROR_MINUS 6;
  DRAW_OPTION P2; MARKER_SIZE 0; DRAW_LEGEND NO;
  FILL_STYLE BSTYLE; FILL_COLOUR BCOLOR;
}(band);

(dpaths){
  PIECE_SETUP ep_ej_nlo.C (dnlo){ }(dnlo);
  PIECE_SETUP ep_ej_nlo.C (dnnlo){ }(dnnlo);
  if (NOHNNLO!="Y") PIECE_SETUP ep_ej_nlo.C (ddynnlo){ }(ddynnlo);
  DIFF_PLOT YES; DRAW YES; DRAW_LEGEND NO;
  TOP_MARGIN 0.7; Y_AXIS_TICK_LENGTH 0.08;
  Y_AXIS_NDIVISIONS 505; Y_AXIS_LABEL_DIVISIONS 1;
  Y_MIN 1-DYRANGE; Y_MAX 1+DYRANGE;
}(dpaths);
(dnnlo){
  PIECE_SETUP ep_ej_nlo.C (centralcentral){ }(centralcentral);
  PIECE_SETUP ep_ej_nlo.C (centraldown){ }(centraldown);
  PIECE_SETUP ep_ej_nlo.C (centralup){ }(centralup);
  PIECE_SETUP ep_ej_nlo.C (centralband){ }(centralband);
  @@ PPC1 RUN_PATH/ep_ej_N2LONAME/ANA_PATH_sum/INCLU;
  Y_AXIS_TITLE Ratio to REFN;
  @@ LCOLOR 1; @@ BSTYLE 1000; @@ BCOLOR LTGRAY;
  @@ LPDFNUM LHAPDFNUM2;
}(dnnlo);
(dnlo){
  PIECE_SETUP ep_ej_nlo.C (centralcentral){ }(centralcentral);
  PIECE_SETUP ep_ej_nlo.C (centraldown){ }(centraldown);
  PIECE_SETUP ep_ej_nlo.C (centralup){ }(centralup);
  PIECE_SETUP ep_ej_nlo.C (centralband){ }(centralband);
  @@ PPC1 RUN_PATH/ep_ej_NLONAME/ANA_PATH_sum/INCLU;
  Y_AXIS_TITLE Ratio to REFN; @@ LCOLOR BLUE1;
  @@ BSTYLE 3013; @@ BCOLOR STEELBLUE3;
  Y_AXIS_NDIVISIONS 505; Y_AXIS_LABEL_DIVISIONS 1;
  Y_MIN 1-DYRANGE; Y_MAX 1+DYRANGE;
  @@ LPDFNUM LHAPDFNUM1;
}(dnlo);
(ddynnlo){
  PIECE_SETUP ep_ej_nlo.C (centralcentral){ }(centralcentral);
  @@ PPC1 apfel; @@ LPDFNUM LHAPDFNUM2;
  Y_AXIS_TITLE Ratio to REFN;
  @@ LCOLOR GREEN1;
}(ddynnlo);
(centralcentral){
  PIECE_SETUP ep_ej_nlo.C (level3){ }(level3);
  PATH_PIECE PPC1/MUR1_MUF1_PDFLPDFNUM/ PPC1/MUR1_MUF1_PDFLPDFNUM/ PPC1/MUR1_MUF1_PDFLPDFNUM/ REFP/MUR1_MUF1_PDFLHAPDFNUM1/;
  ## COLUMNS 2 5 5 2; DRAW_PRIORITY 10;
  DATA_TYPE ALGEBRA(y[0]/y[3])(y[1]/y[3])(y[2]/y[3]);
  X_ERROR_PLUS 3; X_ERROR_MINUS 4; Y_ERROR_PLUS 5; Y_ERROR_MINUS 6;
  LINE_COLOUR LCOLOR; DRAW_PRIORITY 10;
}(centralcentral);
(centraldown){
  PIECE_SETUP ep_ej_nlo.C (level3){ }(level3);
  PATH_PIECE PPC1/MUR0.5_MUF0.5_PDFLPDFNUM/ REFP/MUR1_MUF1_PDFLHAPDFNUM1/;
  DATA_TYPE ALGEBRA(y[0]/y[1]);
  LINE_COLOUR BCOLOR; LINE_WIDTH 0.1;
}(centraldown);
(centralup){
  PIECE_SETUP ep_ej_nlo.C (level3){ }(level3);
  PATH_PIECE PPC1/MUR2_MUF2_PDFLPDFNUM/ REFP/MUR1_MUF1_PDFLHAPDFNUM1/;
  DATA_TYPE ALGEBRA(y[0]/y[1]);
  LINE_COLOUR BCOLOR; LINE_WIDTH 0.1;
}(centralup);
(centralband){
  PIECE_SETUP ep_ej_nlo.C (level3){ }(level3);
  PATH_PIECE PPC1/MUR1_MUF1_PDFLPDFNUM/ PPC1/MUR0.5_MUF0.5_PDFLPDFNUM/ PPC1/MUR2_MUF2_PDFLPDFNUM/ REFP/MUR1_MUF1_PDFLHAPDFNUM1/;
  DATA_TYPE ALGEBRA(y[0]/y[3])((y[0]-min(y[0],y[1],y[2]))/y[3])((max(y[0],y[1],y[2])-y[0])/y[3]);
  X_ERROR_PLUS 3; X_ERROR_MINUS 4; Y_ERROR_PLUS 5; Y_ERROR_MINUS 6;
  DRAW_OPTION P2; MARKER_SIZE 0;
  FILL_STYLE BSTYLE; FILL_COLOUR BCOLOR;
}(centralband);

(level3){
  DATA_TYPE ATOOLS;
}(level3);
