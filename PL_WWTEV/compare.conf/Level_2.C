(level2){
  PIECE_SETUP Level_2.C (Oti){ }(Oti);
  PIECE_SETUP Level_2.C (Otx){ }(Otx);
}(level2);

(Oti){
  PIECE_SETUP Level_3.C (level3){ }(level3);
  PATH_PIECE Oti/;
}(Oti);

(Otx){
  PIECE_SETUP Level_3.C (level3){ }(level3);
  PATH_PIECE Otx/;
}(Otx);
