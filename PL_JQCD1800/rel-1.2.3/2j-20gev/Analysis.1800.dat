(analysis){

  BEGIN_ANALYSIS {

    LEVEL Shower;
  
    PATH_PIECE hep-ex_0011036/

    Detector {
       InList  FinalState; OutList Detected
       HadCal  -4.1 4.1 82 64
    }
  
    Trigger {
       InList Detected; OutList ConeJets
       Finder 93 1.0 -4.1 4.1 0.7 1
       JetMode 2
    }

    AbsEtaSel 0.0 0.5 ConeJets ConeJets_0.0-0.5;
    AbsEtaSel 0.5 1.0 ConeJets ConeJets_0.5-1.0;
    AbsEtaSel 1.0 1.5 ConeJets ConeJets_1.0-1.5;
    AbsEtaSel 1.5 2.0 ConeJets ConeJets_1.5-2.0;
    AbsEtaSel 2.0 3.0 ConeJets ConeJets_2.0-3.0;

    JetET 0 500 250 1 1 3 LinErr ConeJets_0.0-0.5;
    JetET 0 500 250 1 1 3 LinErr ConeJets_0.5-1.0;
    JetET 0 500 250 1 1 3 LinErr ConeJets_1.0-1.5;
    JetET 0 500 250 1 1 3 LinErr ConeJets_1.5-2.0;
    JetET 0 500 250 1 1 3 LinErr ConeJets_2.0-3.0;

  } END_ANALYSIS;

  BEGIN_ANALYSIS {

    LEVEL Shower;

    PATH_PIECE hep-ex_0009012/;
  
    Detector {  
      InList FinalState; OutList hcal_detected;
      HadCal -4.0 4.0 80 64;
    }

    Trigger {
      InList hcal_detected; OutList cone0_jets;
      Finder 93 1.0 -4.0 4.0 0.7 1 0.5;
      JetMode 10;
    }
 
    ETSel 40.0 10000.0 cone0_jets cone0_jet_1;
    YSel -3.0 3.0 cone0_jet_1 cone0_jet_inc;

    Trigger {
      InList cone0_jet_inc; OutList c0_two_jet_inc;
      Counts 93 2 -1;
    }
  
    Trigger {
      InList c0_two_jet_inc; OutList c0_three_jet_inc;
      Counts 93 3 -1;
    }

    HT 0. 600 60 LinErr c0_two_jet_inc;
    HT 0. 600 60 LinErr c0_three_jet_inc;

  } END_ANALYSIS;

  BEGIN_ANALYSIS {

    LEVEL Shower;

    PATH_PIECE hep-ex_9603010/;

    Detector {
      InList FinalState; OutList Detected;
      HadCal -4.2 4.2 84 64;
    }

    DBCone {
      InList Detected; OutList ConeJets;
      ETMin 1; DeltaRMin 0.001;
      DeltaRPre 0.3; DeltaR 0.7;
    }

    ETOrder ConeJets ConeJetsET;

    YSel -3 3 ConeJetsET ConeJetsETY;
    ETSel 20 10000 ConeJetsETY ConeJetsPTY;

    JetET 0 800 400 1 1 3 LinErr ConeJetsPTY;
    JetEta -5 5 200 1 1 3 LinErr ConeJetsPTY;

    CosDPhivsDEta 50 0 6 150 LinErr ConeJetsPTY;

    BFKLDEta 50 0 6 150 LinErr ConeJetsPTY;
    BFKLDPhi 50 0.5 1.5 -1.01 1.01 101 LinErr ConeJetsPTY;
    BFKLDPhi 50 2.5 3.5 -1.01 1.01 101 LinErr ConeJetsPTY;
    BFKLDPhi 50 4.5 5.5 -1.01 1.01 101 LinErr ConeJetsPTY;

    BFKLTwoEta 50 -5 5 100 LinErr ConeJetsPTY;

  } END_ANALYSIS

  BEGIN_ANALYSIS {

    LEVEL Shower;

    PATH_PIECE PHRVA_D50_5562/;

    Trigger {
      OutList jets; JetMode 10;
      Finder 93  10. -4. 4. 0.7;
    }
   
    OneEtaSel 93 0 0 -0.7  0.7  jets eta1;
    OneEtaSel 93 1 0  -0.7  0.7  eta1 jets_eta;
  
    OnePTSel 93 0 0 110. 1000.  jets_eta pt1;
    OnePTSel 93 1 0 10.  1000.  pt1 pt2;
    OnePTSel 93 2 0 10.  1000.  pt2 pt3;

    TwoDPhiSel 93 0 93 1 160 180  jets pt3 full_cuts;
    TwoDRSel   93 1 93 2 1.1 3.14 jets full_cuts full_cuts_alpha;

    JetEta  -4.0 4.0    40  1  1  3  LinErr  full_cuts;
    JetDR    0. 5.      50  1  1  3  LinErr  full_cuts;
    JetAlpha -90. 90.   36  1  1  3  LinErr  full_cuts_alpha;

  } END_ANALYSIS;

  BEGIN_ANALYSIS {

    LEVEL Shower;

    PATH_PIECE KTJetRates/;

    Trigger {
      OutList KTJets;
      Finder 93 0.5 -4 4 1;
    }
    JetDRate 1 1000 125 1 1 5 LogErr KTJets;

  } END_ANALYSIS;

}(analysis);

BEGIN_RIVET {
  -a CDF_1996_S3108457 CDF_1996_S3349578 CDF_1996_S3418421 CDF_1997_S3541940 CDF_1998_S3618439 CDF_2000_S4266730 CDF_2001_S4517016 CDF_2001_S4563131
} END_RIVET
