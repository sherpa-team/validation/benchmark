# Benchmark

This is a description of Sherpa's benchmarking setup.
Its purpose is to alleviate the comparison of various distributions and cross sections between arbitrary versions of Sherpa and possibly data. Note that a full benchmarking of Sherpa is quite time-consuming, in the current setup with 44 processes about 173 12-hour grid jobs on 2.5 GHz machines must be run. These do not yet include the library generation and compile jobs.

## Installation

* checkout this git repo
* install the Compare tool from [this repo](https://gitlab.com/sherpa-team/validation/compare)
* set the `ROOTSYS` variable in your environment
* compile Sherpa with Rivet and Analysis on
* compile the Rivet analyses in the `./custom-rivet-analyses` directory by calling `make` there
* add `./custom-rivet-analyses` directory to your `RIVET\_ANALYSIS\_PATH`
* add path `./benchmark/local/bin` to `PATH`
* set `SITE_NAME` in your environment
* (optional) set up the commands for the `SITE_NAME` in `benchmark/share/master.make` (take the `DURHAM-SLURM` as an example)
* run with `make libs ...`, `make runs ...` and `make plots ...`

## General remarks

* Benchmark uses the grid or local clusters to run all of its subjobs, i.e. library compile and integration / event generation jobs. **It is not (and will never be) designed for running on a single processor machine**
* It is based on Makefiles, such that all commands will be of the form `make <target>`
* The git repository is 

    git@gitlab.com:sherpa-team/validation/benchmark.git

* The directory structure looks like 

```
                         Runs             Setups
    
                                    ,--- reference
                  ,--- HL_LEPI -----|--- rel-1.0.9
                  |                 |--- rel-1.0.11
                  |                 `--- ...
                  |
                  |                 ,--- reference
     BENCHMARK ---|--- PL_WWTEV ----|--- rel-1.0.10
                  |                 `--- ...
                  |
                  |                 ,--- reference
                  |--- SC_w_taunu --|--- rel-1.0.11
                  |                 |--- hadrons
                  |                 `--- ...
                  `--- ...
```

The basic reasoning for the filenames is to have two capital letters classifying the type of the run,
e.g. `XS_...` for a cross section calculation, `HL_...` for hadron level events, etc.

The binaries to run the jobs are located in `/path/to/benchmark/local/bin`, this location must be added to the `PATH` variable.

## How to run a benchmark

Before any run, check if the targets need a custom Rivet analyis and if so, call `make` in the `./custom-rivet-analysis` directory to compile them.

There are 3 major targets involved in a normal benchmarking run:

* `make libs`
* `make runs`
* `make plots`

All of them have the corresponding clean target (make clean-libs, ...) which will basically remove everything created by that target.

Additionally there is the `libs-check` and the `runs-check` target, which will test whether the libraries necessary for `make runs` already exist and what the current status of the submitted jobs is, respectively.

When benchmark is run, a specific setup (e. g. rel-1.0.11) is used as a template and copied to a clean run directory on a process-by-process basis. Then Sherpa is run in the newly created setup paths. When generating the output, it is possible to compare any of the available results, the new run versus a reference (e. g. reference), a new run versus another new run, a reference versus another reference etc. This is done recursively for all processes as shown in the example above.

Different environment variables (potentially) have to be set for each of the targets:

| variable        | libs | runs | plots | Description                                 | Example                          |
|-----------------|------|------|-------|---------------------------------------------|----------------------------------|
| `SETUP_PATH`    |  x   |      |       | the setup you want to benchmark             | rel-1.0.11                       |
| `REF_PATH`      |      |      |   x   | the setup you want to compare against       | reference                        |
| `BIN_PATH`      |  x   |      |       | the binary path of your sherpa installation | /home/user/SHERPA-MC-1.0.11/bin  |
| (`RUN_PATH`)    |  x   |  x   |   x   | temporary grid run path                     | current                          |
| (`ANA_PATH`)    |      |  x   |   x   | run analysis subpath                        | a_1                              |
| (`ARF_PATH`)    |      |      |   x   | reference analysis subpath                  | a_1                              |
| (`OUTPUT_PATH`) |      |      |   x   | plot subpath                                | plots                            |
| `RUN_TITLE`     |      |      |   x   | legend title for run                        | "SHERPA 1.0.11"                  |
| `REF_TITLE`     |      |      |   x   | legend title for reference                  | "SHERPA 1.0.9"                   |
| (`TARGETS`)     |  x   |  X   |       | subset of Runs to be benchmarked            | "HL_LEP1 PL_WWTEV"               |
| (`IGNORES`)     |  x   |      |       | subset of Runs not to be benchmarked        | "HL_LEP1 PL_WWTEV"               |
| (`EVENTS`)      |      |  x   |       | events to be generated                      | 1000000                          |
| (`JOB_RUNTIME`) |      |  x   |       | job runtime in hours                        | 20                               |

### make libs

Creates `RUN_PATH` as a grid-ready setup directory from `SETUP_PATH`, runs Sherpa and submits the library compile jobs to the grid.
For setups with multiple runs employing the same library files, these libraries are generated only once.

### make runs

Starts the actual event generation in the created `RUN_PATH`. First Sherpa is run with a small number of events (approx. 10000) to estimate the run time for the required number of events. Then according to the time estimate given by Sherpa the run is split into multiple subjobs of equal runtime (approx. 12h) which are submitted to the grid. 

If runs failed for some reason, the log file in `REF_PATH/<setup>/<setup>.log` can be deleted to have the Makefile trigger the corresponding run again. The command `make clean-runs ...` will clean all the directories. 

### make plots

Creates a webpage containing the plots from all available runs.

It is necessary to have the most recent version of Compare installed and the binary path added to the PATH environment variable, see [Stefan's website](http://www.freacafe.de/compare/#documentation). The Compare sources are found at

   git@gitlab.com:sherpa-team/validation/compare.git

Also, Root must be loaded, i.e. the ROOTSYS varibale must be set in your system, and it must be configured with the rootsys option, `./configure --with-rootsys=<path-to-your-root> --prefix=<...>`.

If the plots have to be updated, the corresponding folder in the OUTPUT_PATH has to be deleted. 


## How to add a new benchmark setup

If you have a setup that only needs one set of libraries, but possibly multiple parameter settings, include the prepared Makefile snippet `share/events_branch.make` or `share/noevents_branch.make`. Create subdirectories for all parameter combinations in the corresponding Setup directory and create the Makefile similar to HL_LEPI/Makefile.

Note that in the `Makefile` there are two variables per setup directory, LIBDIRS and RUNDIRS. The former must contain all subfolders for which libraries need to be created, the latter just all subdirectories, i.e. `RUNDIRS="$LIBDIRS <all other directories which don't need libraries for their runs>"`. 

For a basic example (simpler than in `PL_QCUT`) have a look at `HL_JQCD7000`.

## Notes from tutorial

A typical run of the tool would look like this, with the exemplary RUN_PATH name `tutorial`:

```
$ make libs BIN_PATH=/mt/home/pmeinzinger/local/bin SETUP_PATH=rel-3.0.0 RUN_PATH=tutorial TARGETS=PL_QCUT QUEUE=cpu

$ make libs-check RUN_PATH=tutorial TARGETS=PL_QCUT

$ make runs RUN_PATH=tutorial TARGETS=PL_QCUT

$ make runs-check RUN_PATH=tutorial TARGETS=PL_QCUT

$ make plots RUN_PATH=tutorial TARGETS=PL_QCUT OUTPUT_PATH=$HOME/www/benchmarking BIN_PATH=$HOME/local/bin
```

If something goes wrong during the creation of the libraries/runs, the background jobs can be killed with the following lines: 

```
# check what processes are running
ps ux -u $USER
# kill all, e.g., "submit" processes
kill -9 $(ps ux -u $USER | grep submit | awk '{print $2}')
```

If the runs are stuck after the pilot run, i.e. as shown by the "runs left" comment in the `make runs-check` command, you can trigger it again by executing

```
touch run.log.split
```

in the respective run directory.
