#!/bin/bash

# begin default options
EDGPATH="$HOME"
if [ "$SITE_NAME" = "UKI-SCOTGRID-DURHAM" ]; then
  EDGPATH="/mt/data-grid/$USER"
elif [ "$SITE_NAME" = "UKI-SCOTGRID-GLASGOW" ]; then
  EDGPATH="/cluster/share/$USER"
fi
DEDGOPTS="-c $EDGPATH/edg.conf --config-vo $EDGPATH/edg-vo.conf"
DCMDOPTS=""
# end default options

print_help() 
{
  echo "grid_resubmit version 1.0" && echo && \
  echo "options: -p <path>      define setup path <path>" && \
  echo "         -g <option>    define job submit command <option>" && \
  echo "         -a <option>    define jdl command line option <option>" && \
  echo "         -S <name>      define split job name <name>" && \
  echo "         -d             use default options" && \
  echo "         -h             display this help and exit" && \
  echo
}

while getopts :p:g:a:U:S:dh OPT
do
  case $OPT in
  p) PATHS=$PATHS" "$OPTARG ;; 
  g) EDGOPTS=$EDGOPTS" "$OPTARG ;; 
  a) CMDOPTS=$CMDOPTS" "$OPTARG ;; 
  U) GTIME=$OPTARG ;; 
  S) LIBSUBDIR=$OPTARG ;; 
  d) CMDOPTS="$DCMDOPTS" && EDGOPTS="$DEDGOPTS" ;; 
  h) print_help && exit 0 ;;
  \?)
    shift `expr $OPTIND - 1`
    if [ "$1" = "--help" ]; then print_help && exit 0
    else 
      echo "grid_resubmit: error: unrecognized option '$1'."
      print_help && exit 1
    fi
    shift 1
    OPTIND=1
  esac
done
if test -z "$EDGOPTS"; then
  echo "grid_run: invalid job submit command. abort."
  exit 1
fi

if ! ( type grid-proxy-info > /dev/null 2>&1 ); then
  echo "grid_resubmit: grid-proxy-info command not found. abort."
  exit
fi
PROXYTIME=`grid-proxy-info | grep timeleft | awk '{ split($3,a,":"); \
  print a[1]*3600+a[2]*60+a[3]; }'`
if [ "$PROXYTIME" = "" ] || test $PROXYTIME -lt 3600; then
  echo "grid_resubmit: your proxy expires in less than 1 hour." \
"time left: `grid-proxy-info | grep timeleft | awk '{ print $3; }'`"
  echo "grid_resubmit: please renew your proxy"
  exit
fi
echo "grid_resubmit: proxy expires in" \
"`grid-proxy-info | grep timeleft | awk '{ print $3; }'` hours"

RPATH=$PWD
for I in $PATHS; do
  if ! test -d $I; then
    echo "grid_resubmit: path '"$I"' does not exist"
  else
    cd $I/
    SFILE=`echo $I | awk '{ n=split($1,a,"/"); \
        if (a[n]!="") print a[n]; else print a[n-1]; }'`
    if test -z "$LIBSUBDIR"; then
      echo "grid_resubmit: checking '"$I"'"
    else
      SFILE=$SFILE"."$LIBSUBDIR
      echo "grid_resubmit: checking '"$I"'("$LIBSUBDIR")"
    fi
    if test -f run.log.split; then rm run.log.split; fi
      if test -f submit_split_jobs; then 
      SPID=`ps aux | grep $PWD/submit_split_jobs | grep -v grep | awk '{ print $2; }'`
      if [ "$SPID" != "" ] ; then kill $SPID ; fi
      ( $PWD/submit_split_jobs > /dev/null 2>&1 ) &
    fi
    if ! test -f $SFILE.jdl.in; then cp $SFILE.jdl $SFILE.jdl.in; fi
    if [ "$CMDOPTS" != "" ]; then
      if [ "$GTIME" != "" ]; then
        sed -e"\$ a Arguments = \"$CMDOPTS\";" \
            -e"s/\(MaxCPUTime > \)[0-9.]*/\1$GTIME/g" < $SFILE.jdl.in > $SFILE.jdl
      else
        sed -e"\$ a Arguments = \"$CMDOPTS\";" < $SFILE.jdl.in > $SFILE.jdl
      fi
    fi
    $EDGOPTS $SFILE.jdl 2>&1 | tee -a $SFILE.log
    cd $RPATH
  fi
done

