#!/bin/bash

if test $# -lt 1; then
  echo "usage: get_names.sh <setup>";
  exit 1;
fi

sup=$1

opwd=$PWD
cd XS_SUSY/rel-1.4.0

gdc=$(echo $sup | sed 's/amegic/comix/g')
for i in $sup/r_1/Amegic/XS*; do
  nm=$(echo $i | sed 's:'$sup'/r_1/Amegic/XS_::g') 
  echo __RUN_PATH__/$sup/__RES_PATH__/Amegic/XS_$nm/$nm __RUN_PATH__/$gdc/__RES_PATH__/Comix/XS_$nm/$nm; 
done

cd $opwd