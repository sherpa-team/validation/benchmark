#! /usr/bin/env python

import sys, os, copy
from math import sqrt
from subprocess import Popen
import lighthisto

## Try to load faster but non-standard cElementTree module
try:
    import xml.etree.cElementTree as ET
except ImportError:
    try:
        import cElementTree as ET
    except ImportError:
        try:
            import xml.etree.ElementTree as ET
        except:
            sys.stderr.write("Can't load the ElementTree XML parser: please install it!\n")
            sys.exit(1)

from optparse import OptionParser
parser = OptionParser(usage="%prog aidafile[:fac] [aidafile2[:fac] ...]")
parser.add_option("-o", "--outfile", dest="OUTFILE",
                  default="added.aida", help="file for added aida output.")
opts, args = parser.parse_args()
headerprefix = ""

if len(args) < 1:
    sys.stderr.write("Must specify at least one AIDA histogram file\n")
    sys.exit(1)


try:
    outaida = open(opts.OUTFILE, "w")
except:
    sys.stderr.write("Couldn't open outfile %s for writing." % opts.OUTFILE)

try:
    outdat = open(opts.OUTFILE.replace(".aida", ".dat"), "w")
except:
    sys.stderr.write("Couldn't open outfile %s for writing." % opts.OUTFILE.replace(".aida", ".dat"))

## Get histos
files = []
prefacs = {}
inhistos = {}
weights = {}
for aidafile in args:
    arguments = aidafile.split(":")
    if len(arguments)>2:
        sys.stderr.write("To many arguments for %s." % arguments[0])
        sys.exit(1)
    aidafilepath = arguments[0]
    files.append(aidafilepath)
    if (len(arguments)==1):
        prefacs[aidafilepath] = 1.
    else:
        prefacs[aidafilepath] = float(arguments[1])
    tree = ET.parse(aidafilepath)
    for dps in tree.findall("dataPointSet"):
        h = lighthisto.Histo.fromDPS(dps)
        if not inhistos.has_key(h.fullPath()):
            inhistos[h.fullPath()] = {}
        inhistos[h.fullPath()][aidafilepath] = h

## Output info
print "========================================================================"
for aidafile in files:
    print aidafile, "--> fac=", prefacs[aidafile]
print "------------------------------------------------------------------------"
print " -> ", opts.OUTFILE
print "========================================================================"


## Add histos
outhistos = {}
for path, hs in inhistos.iteritems():
    outhistos[path] = copy.deepcopy(hs.values()[0])
    for i, b in enumerate(outhistos[path].getBins()):
        sum_val = 0.0
        sum_err2 = 0.0
        n = 0
        for infile, h in hs.iteritems():
            fac = prefacs[infile]
            sum_val += fac*h.getBin(i).val
            sum_err2 += (fac*h.getBin(i).getErr())**2
            n += 1
        outhistos[path].getBin(i).val = sum_val
        outhistos[path].getBin(i).setErr(sqrt(sum_err2))

## Write out added histos
#print sorted(outhistos.values())
outdat.write("\n\n".join([h.asFlat() for h in sorted(outhistos.values())]))
outdat.write("\n")
outdat.close()

Popen([os.path.join(sys.path[0], "flat2aida"), opts.OUTFILE.replace(".aida", ".dat")], stdout=outaida).wait()

os.unlink(opts.OUTFILE.replace(".aida", ".dat"))
