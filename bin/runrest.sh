#!/bin/bash

wmsconf=$(readlink -f $(dirname $0))/../share/wms.conf
for dir in $1; do
    cd $dir;
    for jdl in job_*.jdl; do
        base=$(basename $jdl .jdl)
        if ! test -f $base".log"; then
            echo $base" in "$PWD
            PROXYTIME=`grid-proxy-info | grep timeleft | awk '{ split($3,a,":"); \
                print a[1]*3600+a[2]*60+a[3]; }'`
            if [ "$2" = "1" ]; then
                if [ "$PROXYTIME" != "" ] && test $PROXYTIME -lt 3600; then
                    echo "proxy expires in < 1hr. abort"
                    exit 1
                fi
                echo $base" in "$PWD >> submit_split_jobs.log
                lsf-job-submit $jdl 2>&1 | tee -a submit_split_jobs.log
            fi
        fi
    done
    cd - > /dev/null
done
