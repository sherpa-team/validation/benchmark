function genpnm(prcnm)
{
  sub("_EW","__EW",prcnm);
  gnprcn="";
  pnmn=split(prcnm,pnma,"__");
  for (pni=2;pni<=pnmn;++pni) {
    cfn=pnma[pni]; cfb=0;
    sub("gluino","\\&#126;g",cfn);
    sub("sup","\\&#126;u",cfn);
    sub("sdown","\\&#126;d",cfn);
    sub("scharm","\\&#126;c",cfn);
    sub("sstrange","\\&#126;s",cfn);
    sub("stop","\\&#126;t",cfn);
    sub("sbottom","\\&#126;b",cfn);
    sub("smuon","\\&#126;mu",cfn);
    sub("selectron","\\&#126;e",cfn);
    sub("stau","\\&#126;tau",cfn);
    sub("snu","\\&#126;nu",cfn);
    sub("mu","\\&mu;",cfn);
    sub("tau","\\&tau;",cfn);
    sub("nu","\\&nu;",cfn);
    sub("v","\\&nu;_",cfn);
    sub("P","\\&gamma;",cfn);
    sub("G","g",cfn);
    sub("neutralino","\\&#126;\\&chi;<sup>0</sup>_",cfn);
    sub("chargino1b","\\&#126;\\&chi;-_1",cfn);
    sub("chargino2b","\\&#126;\\&chi;-_2",cfn);
    sub("chargino1","\\&#126;\\&chi;+_1",cfn);
    sub("chargino2","\\&#126;\\&chi;+_2",cfn);
    sub("h0","h_0",cfn);
    sub("H0","H_0",cfn);
    sub("A0","A_0",cfn);
    sub("+","<sup>+</sup>",cfn);
    sub("-","<sup>-</sup>",cfn);
    if (length(cfn)>1 && match(cfn,"b$")>0) {
      cft=substr(cfn,0,length(cfn)-1);
      cfn="<font style=\"text-decoration: overline;\">"cft;
      cfb=1;
    }
    cfnn=split(cfn,cfna,"_");
    if (cfnn<2) {
      if (cfb) cfn=cfn"</font>";
    }
    else {
      if (cfb) cfn=cfna[1]"</font><sub>";
      else cfn=cfna[1]"<sub>";
      for (cfni=2;cfni<cfnn;++cfni)
	cfn=cfn" "cfna[cfni];
      cfnm=split(cfna[cfnn],cfnb,"<");
      if (cfnm>1) {
	cfn=cfn" "cfnb[1]"</sub>";
	for (cfnj=2;cfnj<=cfnm;++cfnj)
	  cfn=cfn"<"cfnb[cfnj];
      }
      else {
	cfn=cfn" "cfna[cfni]"</sub>";
      }
    }
    gnprcn=gnprcn" "cfn;
    if (pni==3) gnprcn=gnprcn" &#8594;";
  }
  return gnprcn;
}
function compnm(proca,procb)
{
  pnmna=split(proca,pnma,"__");
  pnmnb=split(procb,pnmb,"__");
  if (pnma[2]!=pnmb[2] && pnma[2]!=pnmb[3]) return 0;
  if (pnma[3]!=pnmb[2] && pnma[3]!=pnmb[3]) return 0;
  for (pni in pnma) {
    pnmi=0;
    for (pnj in pnmb)
      if (pnma[pni]==pnmb[pnj]) {
	delete pnma[pni];
	delete pnmb[pnj];
	pnmi=1;
	break;
      }
    if (!pnmi) return 0;
  }
  return 1;
}
BEGIN { pi=0; i=0; j=0; pb=3.89379656e11; end1=0; warnings=0; errors=0; cf=0; } 
{
  while (NF==0) getline; 
  if (pi==0) { 
    if ($1!="") ++pi;
    filename=$1".xsd.dat"; 
    htmlname=$1"/index.html";
  } 
  else if (pi==1) {
    if ($1!="") ++pi;
    genone=$0;
  } 
  else if (pi==2) {
    if ($1!="") ++pi;
    gentwo=$0;
  } 
  else if (pi==3) {
    if ($1!="") ++pi;
    printf "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD " \
      "HTML 4.01 Transitional//EN\">\n" > htmlname
    printf "<html>\n  <head>\n" > htmlname
    printf "    <title>xs comparison</title>\n" > htmlname
    printf "  </head>\n  <body>\n    <hr size=\"1\">\n" > htmlname
    printf "    <center><font color=\"#0000ff\" size=\"+2\"><b>\n      "$0 \
      "</b></font></center>\n" > htmlname
    printf "    <hr size=\"1\"><br>\n    <table width=\"100%%\"" \
      " border=\"1\">\n" > htmlname 
    printf "      <tr bgcolor=\"#bbbbbb\"><td><b>Process</b></td>\n" > htmlname
    printf "        <td><center><b>XS from "genone \
      " [fb]</b></center></td>\n" > htmlname
    printf "        <td><center><b>XS from "gentwo \
      " [fb]</b></center></td>\n" > htmlname
#    printf "        <td><center><b>w<sub>2</sub>/w<sub>1</sub>-1 [%%]" \
#      "</b></center></td>\n" > htmlname
    printf "        <td><center><b>(w<sub>2</sub>-w<sub>1</sub>)/\n" \
      "          (&sigma;<sub>1</sub><sup>2</sup>+" \
      "&sigma;<sub>2</sub><sup>2</sup>)<sup>1/2</sup>" \
      "</b></center></td></tr>\n" > htmlname
  } 
  else { 
    if ($1=="end") {
      end1=1; 
      cf=0;
    }
    else {
    if (end1==0) { 
      proc1[i]=$1; 
      if (proc1[i]=="#") {
	proc1[i]=$0;
      }
      else if (proc1[i]=="%%") {
        cf=$2;
	--i;
      }
      else {
        file1[i]=cf;
        xs1[i]=$2;  
        max1[i]=$3;  
        err1[i]=$4;  
	if (xs1[i]==0 || $2=="nan") relerr1[i]=0;  
        else relerr1[i]=err1[i]/xs1[i];  
#        printf "1st file: process: "$1", xs = "xs1[i]*pb" pb, err = " \
#          err1[i]*pb" ( "relerr1[i]*100"% ), max = "max1[i]*pb" pb\n"; 
      }
      ++i; 
    } 
    else { 
      proc2[j]=$1; 
      if (proc2[j]=="#") {
        proc2[j]=$0;
      }
      else if (proc2[j]=="%%") {
        cf=$2;
	--j;
      }
      else {
	file2[j]=cf;
	xs2[j]=$2;  
	max2[j]=$3;  
	err2[j]=$4;  
	if (xs2[j]==0 || $2=="nan") relerr2[j]=0;  
	else relerr2[j]=err2[j]/xs2[j];  
#        printf "2nd file: process: "$1", xs = "xs2[j]*pb" pb, err = " \
#          err2[j]*pb" ( "relerr2[j]*100"% ), max = "max2[j]*pb" pb\n"; 
      }
      ++j; 
    } 
  } 
  }
} 
END { 
  min=-4.2; max=4.2; bins=21; binwidth=(max-min)/bins; 
  for (k=0;k<=bins+1;++k) { 
    histox[k]=min+binwidth*(k-1); 
    histoy[k]=0; 
  } 
  devsum = 0.; 
  devavg = 0.; 
  pss=0; oldt=-1;
  for (ii=0;ii<i;++ii) {
    gtag=0;
    if (match(proc1[ii],"SG")>0) continue;
## temporary workaround
#    if (match(proc1[ii],"j")>0 ||
#        match(proc1[ii],"Q")>0 || match(proc1[ii],"Qb")>0) gtag=1;
    match(proc1[ii],"#");
    if (RSTART==1) {
      if (oldt>=0) {
	ssearch=0;
	for (ji=0;ji<j;++ji) {
	  if (match(proc2[ji],"SG")>0) continue;
	  match(proc2[ji],"#");
	  if (RSTART==1) {
	    if (compnm(proc1[oldt],proc2[ji])) ssearch=1;
	    else if (ssearch==1) break;
	    continue;
	  }
	  if (ssearch==0) continue;
	  found=0;
	  for (ij=0;ij<i;++ij) { 
	    if (file2[ji]!=file1[ij] || !compnm(proc2[ji],proc1[ij])) continue; 
	    found=1;
	    break; 
	  }
	  if (found==0) {
	    printf "test process: \033[1m"proc2[ji]	\
	      "\033[0m, not found in file 1\n"; 
	    printf "      <tr bgcolor=\"#ffffff\">" > htmlname
	    printf "<td><b>"genpnm(proc2[ji])"</b></td>\n" > htmlname
	    printf "        <td><center>-</center></td>\n" > htmlname
	    printf "        <td><center>"xs2[ji]*pb" +- "err2[ji]*pb	\
	      " ( "relerr2[ji]*100"%% )</center></td>\n" > htmlname
#	    printf "        <td><center>n.a.</center></td>\n" > htmlname
	    printf "        <td><center>n.a.</center></td>\n" > htmlname
	  }
	}
      }
      printf "      <tr><td colspan=\"5\"><center>" \
	substr(proc1[ii],2)"</center></td></tr>\n" > htmlname
      printf "# \033[1m"substr(proc1[ii],2)"\033[0m\n"; 
      oldt=ii;
      continue;
    }
    found=0;
    for (jj=0;jj<j;++jj) { 
      if (file1[ii]!=file2[jj] || !compnm(proc1[ii],proc2[jj])) continue; 
      ++pss;
      meanerr=sqrt(err1[ii]*err1[ii]+err2[jj]*err2[jj]); 
      devvar=(xs2[jj]-xs1[ii])/meanerr; 
      devsum += devvar*devvar; 
      devavg += devvar; 
      reldev=devvar; 
      if (reldev<0) reldev=-reldev;
      if (gtag==0) {
        for (k=1;k<=bins;++k) { 
          if (devvar>=histox[k] && devvar<histox[k+1]) ++histoy[k]; 
        } 
        if (devvar<histox[0]) ++histoy[0]; 
        if (devvar>=histox[bins+1]) ++histoy[bins+1];
      }
      printf "test process: \033[1m"proc1[ii] \
        "\033[0m, rel deviation = "; 
      if (reldev>1.0) { 
	if (reldev>2.0) { 
	  printf "      <tr bgcolor=\"#ffcccc\">" > htmlname
	  printf "\033[41m";
	}
	else {
	  printf "      <tr bgcolor=\"#ffffcc\">" > htmlname
	  printf "\033[31m";
	}
      }
      else {
	printf "      <tr bgcolor=\"#ccffcc\">" > htmlname
	printf "\033[34m";
      }
      printf "<td><b>"genpnm(proc1[ii])"</b></td>\n" > htmlname
      printf "        <td><center>"xs1[ii]*pb" +- "err1[ii]*pb \
        " ( "relerr1[ii]*100"%% )</center></td>\n" > htmlname
      printf "        <td><center>"xs2[jj]*pb" +- "err2[jj]*pb \
        " ( "relerr2[jj]*100"%% )</center></td>\n" > htmlname
#      printf "        <td><center>"(xs2[jj]/xs1[ii]-1)*100 \
#        "</center></td>\n        " > htmlname
      printf devvar"\033[0m sigma vs. rel errors = \033[32m" \
	relerr1[ii]*100"%%\033[0m, \033[32m"relerr2[jj]*100"%%\033[0m\n"; 
      if (reldev>1.0) { 
        if (reldev>2.0) { 
	  printf "<td><center><font color=\"#aa0000\"><b>"devvar \
            "</b></font></center></td></tr>\n" > htmlname
	  ++errors; 
        } 
        else { 
	  printf "<td><center><font color=\"#dddd00\"><b>"devvar \
            "</b></font></center></td></tr>\n" > htmlname
	  ++warnings; 
        }
      }
      else {
	printf "<td><center><font color=\"#00aa00\"><b>"devvar	\
	  "</b></font></center></td></tr>\n" > htmlname
      }
      found=1;
      break; 
    }
    if (found==0) {
      printf "test process: \033[1m"proc1[ii] \
        "\033[0m, not found in file 2\n"; 
      printf "      <tr bgcolor=\"#ffffff\">" > htmlname
      printf "<td><b>"genpnm(proc1[ii])"</b></td>\n" > htmlname
      printf "        <td><center>"xs1[ii]*pb" +- "err1[ii]*pb \
        " ( "relerr1[ii]*100"%% )</center></td>\n" > htmlname
      printf "        <td><center>-</center></td>\n" > htmlname
#      printf "        <td><center>n.a.</center></td>\n" > htmlname
      printf "        <td><center>n.a.</center></td>\n" > htmlname
    }
  }
  printf "finished test with "errors \
    " errors and "warnings" warnings in "pss" processes\n"; 
  if (pss>1) printf "mean sigma is "devavg/pss \
    ", delta sigma is "sqrt((devsum-devavg*devavg/pss)/(pss-1))"\n"; 
  else printf "Only one process."; 
  printf "write deviation histo to "filename"\n"; 
  for (k=0;k<=bins+1;++k) { 
    printf histox[k]" "histoy[k]"\n" > filename; 
  } 
  printf "    </table><br>\n    <hr size=\"1\">\n" > htmlname
  printf "    Test yields "errors \
    " errors and "warnings" warnings in "pss" processes" > htmlname 
  if (pss>1) printf ",\n    &lt;&sigma;&gt; = "devavg/pss \
    ", &Delta;&sigma; = " sqrt((devsum-devavg*devavg/pss)/(pss-1)) > htmlname
  printf "\n    <hr size=\"1\">\n    " > htmlname
  printf "<img style=\"border:ridge #ffffff 3px\"" \
    " alt=\"deviation distribution\" src=\"Dev_Stat.png\">\n    " > htmlname
  printf "<hr size=\"1\">\n    <br><br>\n  </body>\n</html>\n" > htmlname
  printf "wrote data to "htmlname"\n"; 
}
