#!/bin/bash

echo $1

for dir in $PWD/$1/*/Process; do
    cd $dir;
    for j in P*_*; do
	cd $j;
	for i in *.jdl; do
	    base=$(basename $i .jdl)
	    if test ! -f ../lib/libProc_$base.so; then
		echo $i;
		if [ "$2" = "1" ]; then
		    make -C $base install;
		    make -C $base clean;
		fi
	    fi;
	done;
	cd - > /dev/null;
    done
    cd - > /dev/null;
done