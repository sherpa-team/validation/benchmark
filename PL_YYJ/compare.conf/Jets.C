(jets){
  if (JETCONTS&1) {
    PIECE_SETUP Jets.C (jet[2[+1]11]){ }(jet[2[+1]11]);
  }
  // if (KFAC!=1 && NOCONT!=true) DRAW_LATEX K = KFAC | COLOUR ORANGE1 ALIGN 12 TOP 0.125 LEFT 0.05 PRIORITY 20 \;;
  if (JETCONTS&2) LINE_COLOUR CLC;
  PIECE_SETUP Jets.C (jet1){ }(jet1);
}(jets);

(jet1){
  PIECE_SETUP Data.C (data){ }(data);
  LINE_WIDTH 1;
  DRAW_PRIORITY 10;
//   if (RESIZEDRF==true) {
//     ## RESIZE_BINS RESIZEDRP;
//   }
}(jet1);

(jet2){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j2/;
  if (JETCONTS&2) LINE_STYLE 2;
  else LINE_COLOUR RED1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet2);

(jet3){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j3/;
  if (JETCONTS&2) LINE_STYLE 3;
  else LINE_COLOUR GREEN1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet3);

(jet4){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j2/S/;
  if (JETCONTS&2) LINE_STYLE 4;
  else LINE_COLOUR BLUE1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet4);

(jet5){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j2/H/;
  if (JETCONTS&2) LINE_STYLE 5;
  else LINE_COLOUR YELLOW1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet5);

(jet6){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j3/S/;
  if (JETCONTS&2) LINE_STYLE 6;
  else LINE_COLOUR VIOLET1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet6);

(jet7){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j3/H/;
  if (JETCONTS&2) LINE_STYLE 7;
  else LINE_COLOUR CYAN1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet7);

(jet8){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j4/;
  if (JETCONTS&2) LINE_STYLE 8;
  else LINE_COLOUR ORANGE1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet8);

(jet9){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j4/S/;
  if (JETCONTS&2) LINE_STYLE 2;
  else LINE_COLOUR RED1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet9);

(jet10){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j4/H/;
  if (JETCONTS&2) LINE_STYLE 3;
  else LINE_COLOUR GREEN1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet10);

(jet11){
  PIECE_SETUP Data.C (data){ }(data);
  PATH_PIECE j5/;
  if (JETCONTS&2) LINE_STYLE 4;
  else LINE_COLOUR BLUE1;
  if (NOCONT==true) NODRAW YES;
  DRAW_LEGEND NO;
}(jet11);

