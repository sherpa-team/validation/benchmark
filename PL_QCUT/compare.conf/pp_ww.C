(initial){
  if (PTYPE=="l0j1") {
    @@ HTAP1 RUN_PATH/pp_ww_l0-j1/ANA_PATH_sum/Jets/; @@ TITLE1 1j 20GeV;
    @@ HTAP2 RUN_PATH/pp_ww_l0-j1/ANA_PATH_sum/Jets/QCUT2/; @@ TITLE2 1j 40GeV;
    @@ HTAP3 RUN_PATH/pp_ww_l0-j1/ANA_PATH_sum/Jets/QCUT0.5/; @@ TITLE3 1j 10GeV;
    @@ HTAP5 RUN_PATH/pp_ww_l0-j1/ANA_PATH_sum/Jets/QCUT1e+12/; @@ TITLE5 PS;
  }
  if (PTYPE=="l0j2") {
    @@ HTAP1 RUN_PATH/pp_ww_l0-j2/ANA_PATH_sum/Jets/; @@ TITLE1 2j 20GeV;
    @@ HTAP2 RUN_PATH/pp_ww_l0-j2/ANA_PATH_sum/Jets/QCUT2/; @@ TITLE2 2j 40GeV;
    @@ HTAP3 RUN_PATH/pp_ww_l0-j2/ANA_PATH_sum/Jets/QCUT0.5/; @@ TITLE3 2j 10GeV;
    @@ HTAP5 RUN_PATH/pp_ww_l0-j2/ANA_PATH_sum/Jets/QCUT1e+12/; @@ TITLE5 PS;
  }
  if (PTYPE=="l2j1") {
    @@ HTAP1 RUN_PATH/pp_ww_l2-j1/ANA_PATH_sum/Jets/; @@ TITLE1 1(0)j 20GeV;
    @@ HTAP2 RUN_PATH/pp_ww_l2-j1/ANA_PATH_sum/Jets/QCUT2/; @@ TITLE2 1(0)j 40GeV;
    @@ HTAP3 RUN_PATH/pp_ww_l2-j1/ANA_PATH_sum/Jets/QCUT0.5/; @@ TITLE3 1(0)j 10GeV;
    @@ HTAP5 RUN_PATH/pp_ww_l2-j1/ANA_PATH_sum/Jets/QCUT1e+12/; @@ TITLE5 MCNLO;
  }
  if (PTYPE=="l2j2") {
    @@ HTAP1 RUN_PATH/pp_ww_l2-j2/ANA_PATH_sum/Jets/; @@ TITLE1 2(0)j 20GeV;
    @@ HTAP2 RUN_PATH/pp_ww_l2-j2/ANA_PATH_sum/Jets/QCUT2/; @@ TITLE2 2(0)j 40GeV;
    @@ HTAP3 RUN_PATH/pp_ww_l2-j2/ANA_PATH_sum/Jets/QCUT0.5/; @@ TITLE3 2(0)j 10GeV;
    @@ HTAP5 RUN_PATH/pp_ww_l2-j2/ANA_PATH_sum/Jets/QCUT1e+12/; @@ TITLE5 MCNLO;
  }
  if (PTYPE=="l3j2") {
    @@ HTAP1 RUN_PATH/pp_ww_l3-j2/ANA_PATH_sum/Jets/; @@ TITLE1 2(1)j 20GeV;
    @@ HTAP2 RUN_PATH/pp_ww_l3-j2/ANA_PATH_sum/Jets/QCUT2/; @@ TITLE2 2(1)j 40GeV;
    @@ HTAP3 RUN_PATH/pp_ww_l3-j2/ANA_PATH_sum/Jets/QCUT0.5/; @@ TITLE3 2(1)j 10GeV;
    @@ HTAP5 RUN_PATH/pp_ww_l3-j2/ANA_PATH_sum/Jets/QCUT1e+12/; @@ TITLE5 MCNLO;
  }
  if (PTYPE=="q10") {
    @@ HTAP1 RUN_PATH/pp_ww_l2-j1/ANA_PATH_sum/Jets/; @@ TITLE1 1(0)j;
    @@ HTAP2 RUN_PATH/pp_ww_l2-j2/ANA_PATH_sum/Jets/; @@ TITLE2 2(0)j;
    @@ HTAP3 RUN_PATH/pp_ww_l3-j2/ANA_PATH_sum/Jets/; @@ TITLE3 2(1)j;
    @@ HTAP4 RUN_PATH/pp_ww_l0-j1/ANA_PATH_sum/Jets/; @@ TITLE4 1j;
    @@ HTAP5 RUN_PATH/pp_ww_l0-j2/ANA_PATH_sum/Jets/; @@ TITLE5 2j;
  }
  @@ LISTNAME Jets;

  PIECE_SETUP pp_ww.C (DRate[0[+1]4]){ }(DRate[0[+1]4]);
  RIGHT_AXIS YES; TOP_AXIS YES;
  DEFINE_COLOURS VIOLET1 195 0 185,;
  DEFINE_COLOURS BLUE1 25 25 205,;
  DEFINE_COLOURS GREEN1 30 145 35,;
  DEFINE_COLOURS YELLOW1 255 175 0,;
  DEFINE_COLOURS RED1 195 15 15,;
  DEFINE_COLOURS CYAN1 0 138 255,;
  DEFINE_COLOURS ORANGE1 255 75 0;
}(initial);

(DRate0){
  PIECE_SETUP pp_ww.C (Plots){ }(Plots);
  FILE_PIECE LISTNAME_KtJetrates(1)jet_1_1_0.dat;
  X_MIN 0.3; X_MAX 3; @@ RZB 0.3 2;
  @@ YMIN 1.1e-5; @@ YMAX 1e1; @@ YSCALING Log_B_10;
  X_AXIS_TITLE log_{10}(Q_{1#rightarrow 0}/GeV);
  @@ YTITLE d#sigma/dlog_{10}(Q_{1#rightarrow 0}/GeV) [pb];
  HISTOGRAM_NAME DRate0;
  WEBPAGE_CAPTION Differential 0-jet rate;
}(DRate0);
(DRate2){
  PIECE_SETUP pp_ww.C (Plots){ }(Plots);
  FILE_PIECE LISTNAME_KtJetrates(1)jet_1_1_1.dat;
  X_MIN 0.3; X_MAX 2.7; @@ RZB 0.3 2;
  @@ YMIN 1.1e-5; @@ YMAX 1e1; @@ YSCALING Log_B_10;
  X_AXIS_TITLE log_{10}(Q_{2#rightarrow 1}/GeV);
  @@ YTITLE d#sigma/dlog_{10}(Q_{2#rightarrow 1}/GeV) [pb];
  HISTOGRAM_NAME DRate0;
  WEBPAGE_CAPTION Differential 2-jet rate;
}(DRate2);
(DRate3){
  PIECE_SETUP pp_ww.C (Plots){ }(Plots);
  FILE_PIECE LISTNAME_KtJetrates(1)jet_1_1_2.dat;
  X_MIN 0.3; X_MAX 2.4; @@ RZB 0.3 2;
  @@ YMIN 1.1e-5; @@ YMAX 1e1; @@ YSCALING Log_B_10;
  X_AXIS_TITLE log_{10}(Q_{3#rightarrow 2}/GeV);
  @@ YTITLE d#sigma/dlog_{10}(Q_{3#rightarrow 2}/GeV) [pb];
  HISTOGRAM_NAME DRate0;
  WEBPAGE_CAPTION Differential 2-jet rate;
}(DRate3);
(DRate4){
  PIECE_SETUP pp_ww.C (Plots){ }(Plots);
  FILE_PIECE LISTNAME_KtJetrates(1)jet_1_1_3.dat;
  X_MIN 0.3; X_MAX 2.1; @@ RZB 0.3 2;
  @@ YMIN 1.1e-5; @@ YMAX 1e1; @@ YSCALING Log_B_10;
  X_AXIS_TITLE log_{10}(Q_{4#rightarrow 3}/GeV);
  @@ YTITLE d#sigma/dlog_{10}(Q_{4#rightarrow 3}/GeV) [pb];
  HISTOGRAM_NAME DRate0;
  WEBPAGE_CAPTION Differential 3-jet rate;
}(DRate4);

(Plots){
  PIECE_SETUP pp_ww.C (Main){ }(Main);
  PIECE_SETUP pp_ww.C (Ratio){ }(Ratio);
  PIECE_SETUP pp_ww.C (JetRatio){ }(JetRatio);
}(Plots);
(Main){
  PIECE_SETUP pp_ww.C (Main[1[+1]5]){ }(Main[1[+1]5]);
  DRAW YES; BOTTOM_MARGIN 0.5; Y_AXIS_TITLE YTITLE;
  X_AXIS_LABEL_SIZE 0; X_TITLE_SIZE 0;
  Y_MIN YMIN; Y_MAX YMAX; Y_SCALING YSCALING;
  DATA_TYPE ATOOLS; ## RESIZE_BINS RZB;
  LEG_TEXT_SIZE 0.025; LEG_DELTA_Y 0.05;
  LEG_LEFT 0.2; LEG_RIGHT 0.5; LEG_TOP 0.5;
}(Main);
(Ratio){
  PIECE_SETUP pp_ww.C (Ratio[1[+1]5]){ }(Ratio[1[+1]5]);
  ## COLUMNS 2 2 5 5; Y_MIN -0.25; Y_MAX 0.25;
  DATA_TYPE ALGEBRA(y[1]/y[0]-1)((y[2]/y[0]+y[3]/y[1])*y[1]/y[0])((y[2]/y[0]+y[3]/y[1])*y[1]/y[0]);
  X_ERROR_PLUS 3; X_ERROR_MINUS 4; Y_ERROR_PLUS 5; Y_ERROR_MINUS 6;
  DRAW YES; TOP_MARGIN 0.5; BOTTOM_MARGIN 0.3; DRAW_LEGEND NO;
  DIFF_PLOT YES; Y_AXIS_TITLE Ratio; Y_CENTER_TITLE YES;
  Y_AXIS_TICK_LENGTH 0.07; Y_AXIS_NDIVISIONS 505; Y_AXIS_LABEL_DIVISIONS 1;
  X_AXIS_LABEL_SIZE 0; X_TITLE_SIZE 0;
}(Ratio);
(JetRatio){
  PIECE_SETUP pp_ww.C (JetRatio[1[+1]5]){ }(JetRatio[1[+1]5]);
  ## COLUMNS 2 2 5 5; Y_MIN 0.001; Y_MAX 0.999;
  DATA_TYPE ALGEBRA(y[1]/y[0])((y[2]/y[0]+y[3]/y[1])*y[1]/y[0])((y[2]/y[0]+y[3]/y[1])*y[1]/y[0]);
  X_ERROR_PLUS 3; X_ERROR_MINUS 4; Y_ERROR_PLUS 5; Y_ERROR_MINUS 6;
  DRAW YES; TOP_MARGIN 0.7; DIFF_PLOT YES;
  Y_AXIS_TITLE Fraction; Y_CENTER_TITLE YES;
  Y_AXIS_TICK_LENGTH 0.07; Y_AXIS_NDIVISIONS 505; Y_AXIS_LABEL_DIVISIONS 1;
  LEG_LEFT 0.8; LEG_RIGHT 1; LEG_TOP 0.75;
  LEG_DELTA_Y 0.125; LEG_TEXT_SIZE 0.03;
}(JetRatio);

(Main1){
  PIECE_SETUP pp_ww.C (Jet[0[+1]3]){ }(Jet[0[+1]3]);
  PATH_PIECE HTAP1; LINE_COLOUR RED1; LEGEND_TITLE TITLE1;
}(Main1);
(Main2){
  PIECE_SETUP pp_ww.C (Jet[0[+1]3]){ }(Jet[0[+1]3]);
  PATH_PIECE HTAP2; LINE_COLOUR GREEN1; LEGEND_TITLE TITLE2;
}(Main2);
(Main3){
  PIECE_SETUP pp_ww.C (Jet[0[+1]3]){ }(Jet[0[+1]3]);
  PATH_PIECE HTAP3; LINE_COLOUR BLUE1; LEGEND_TITLE TITLE3;
}(Main3);
(Main4){
  PIECE_SETUP pp_ww.C (Jet[0[+1]3]){ }(Jet[0[+1]3]);
  PATH_PIECE HTAP4; LINE_COLOUR VIOLET1; LEGEND_TITLE TITLE4;
}(Main4);
(Main5){
  PIECE_SETUP pp_ww.C (Jet[0[+1]3]){ }(Jet[0[+1]3]);
  PATH_PIECE HTAP5; LINE_COLOUR YELLOW1; LEGEND_TITLE TITLE5;
}(Main5);

(Jet1){
  PATH_PIECE j4/; LINE_STYLE 2; DRAW_LEGEND NO;
  if (JETCONTS!="Y") NODRAW YES;
}(Jet1);
(Jet2){
  PATH_PIECE j5/; LINE_STYLE 3; DRAW_LEGEND NO;
  if (JETCONTS!="Y") NODRAW YES;
}(Jet2);
(Jet3){
  PATH_PIECE j6/; LINE_STYLE 4; DRAW_LEGEND NO;
  if (JETCONTS!="Y") NODRAW YES;
}(Jet3);

(Ratio1){
  PATH_PIECE HTAP1 HTAP1 HTAP1 HTAP1;
  LINE_COLOUR RED1;
}(Ratio1);
(Ratio2){
  PATH_PIECE HTAP1 HTAP2 HTAP1 HTAP2;
  LINE_COLOUR GREEN1;
}(Ratio2);
(Ratio3){
  PATH_PIECE HTAP1 HTAP3 HTAP1 HTAP3;
  LINE_COLOUR BLUE1;
}(Ratio3);
(Ratio4){
  PATH_PIECE HTAP1 HTAP4 HTAP1 HTAP4;
  LINE_COLOUR VIOLET1;
}(Ratio4);
(Ratio5){
  PATH_PIECE HTAP1 HTAP5 HTAP1 HTAP5;
  LINE_COLOUR YELLOW1;
}(Ratio5);

(JetRatio1){
  PIECE_SETUP pp_ww.C (RatioJet[1[+1]4]){ }(RatioJet[1[+1]4]);
  @@ PPC1 HTAP1; LINE_COLOUR RED1;
}(JetRatio1);
(JetRatio2){
  PIECE_SETUP pp_ww.C (RatioJet[1[+1]4]){ }(RatioJet[1[+1]4]);
  @@ PPC1 HTAP2; LINE_COLOUR GREEN1; DRAW_LEGEND NO;
}(JetRatio2);
(JetRatio3){
  PIECE_SETUP pp_ww.C (RatioJet[1[+1]4]){ }(RatioJet[1[+1]4]);
  @@ PPC1 HTAP3; LINE_COLOUR BLUE1; DRAW_LEGEND NO;
}(JetRatio3);
(JetRatio4){
  PIECE_SETUP pp_ww.C (RatioJet[1[+1]4]){ }(RatioJet[1[+1]4]);
  @@ PPC1 HTAP4; LINE_COLOUR VIOLET1; DRAW_LEGEND NO;
}(JetRatio4);
(JetRatio5){
  PIECE_SETUP pp_ww.C (RatioJet[1[+1]4]){ }(RatioJet[1[+1]4]);
  @@ PPC1 HTAP5; LINE_COLOUR YELLOW1; DRAW_LEGEND NO;
}(JetRatio5);

(RatioJet1){
  PATH_PIECE PPC1 PPC1j4/ PPC1 PPC1j4/; LINE_STYLE 1; LEGEND_TITLE +0 jet;
}(RatioJet1);
(RatioJet2){
  PATH_PIECE PPC1 PPC1j5/ PPC1 PPC1j5/; LINE_STYLE 2; LEGEND_TITLE +1 jet;
}(RatioJet2);
(RatioJet3){
  PATH_PIECE PPC1 PPC1j6/ PPC1 PPC1j6/; LINE_STYLE 3; LEGEND_TITLE +2 jet;
}(RatioJet3);
