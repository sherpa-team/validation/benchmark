# Conversion to rel3 format
Here follows a list of all `yaml` files in the tree.
Once a runcard is fully converted, remove it from the list, if all of a
directory are done, keep the outmost folder and add a ✔, so signal it has been
done (see `PL_UN2LOPS`)


```bash
.
├── AFB_TT
│   └── rel-3.0.0
│       ├── 0-2j_20gev
│       │   └── Sherpa.yaml
│       ├── 0-2j_20gev_qcd
│       │   └── Sherpa.yaml
│       ├── 0-3j_10gev_qcd
│       │   └── Sherpa.yaml
│       ├── 0-3j_5gev_qcd
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_fdown
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_fdown_qcd
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_fup
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_fup_qcd
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_qcd
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_rdown
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_rdown_qcd
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_rup
│       │   └── Sherpa.yaml
│       ├── 0-3j_7gev_rup_qcd
│       │   └── Sherpa.yaml
│       ├── 0-4j_10gev
│       │   └── Sherpa.yaml
│       ├── 0-4j_5gev
│       │   └── Sherpa.yaml
│       ├── 0-4j_7gev
│       │   └── Sherpa.yaml
│       ├── 2-2j_20gev
│       │   └── Sherpa.yaml
│       ├── 2-2j_20gev_check_qcd
│       │   └── Sherpa.yaml
│       ├── 2-2j_20gev_qcd
│       │   └── Sherpa.yaml
│       ├── 2-2j_20gev_reco_qcd
│       │   └── Sherpa.yaml
│       ├── 2-3j_7gev
│       │   └── Sherpa.yaml
│       ├── 2-3j_7gev_fdown
│       │   └── Sherpa.yaml
│       ├── 2-3j_7gev_fup
│       │   └── Sherpa.yaml
│       ├── 2-3j_7gev_qcd
│       │   └── Sherpa.yaml
│       ├── 2-3j_7gev_rdown
│       │   └── Sherpa.yaml
│       ├── 2-3j_7gev_rup
│       │   └── Sherpa.yaml
│       ├── 2-4j_10gev
│       │   └── Sherpa.yaml
│       ├── 2-4j_5gev
│       │   └── Sherpa.yaml
│       ├── 2-4j_7gev
│       │   └── Sherpa.yaml
│       ├── 3-3j_10gev
│       │   └── Sherpa.yaml
│       ├── 3-3j_10gev_qcd
│       │   └── Sherpa.yaml
│       ├── 3-3j_5gev
│       │   └── Sherpa.yaml
│       ├── 3-3j_5gev_qcd
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_fdown
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_fdown_qcd
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_fup
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_fup_qcd
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_qcd
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_rdown
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_rdown_qcd
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_rup
│       │   └── Sherpa.yaml
│       ├── 3-3j_7gev_rup_qcd
│       │   └── Sherpa.yaml
│       ├── 3-4j_10gev
│       │   └── Sherpa.yaml
│       ├── 3-4j_5gev
│       │   └── Sherpa.yaml
│       ├── 3-4j_7gev
│       │   └── Sherpa.yaml
│       ├── 3-4j_7gev_fdown
│       │   └── Sherpa.yaml
│       ├── 3-4j_7gev_fup
│       │   └── Sherpa.yaml
│       ├── 3-4j_7gev_qcd
│       │   └── Sherpa.yaml
│       ├── 3-4j_7gev_rdown
│       │   └── Sherpa.yaml
│       └── 3-4j_7gev_rup
│           └── Sherpa.yaml
├── HL_JQCD7000 (✔)
├── HL_LEPI (✔)
├── HL_MPI (✔)
├── MENLOPS_DISH
│   └── rel-3.0.0
│       ├── cnlo0-meps4-5-6
│       │   └── Sherpa.yaml
│       ├── dnlo0-meps4-5-6
│       │   └── Sherpa.yaml
│       ├── nlo2-meps0-5-6
│       │   └── Sherpa.yaml
│       ├── nlo2-meps3-5-6
│       │   └── Sherpa.yaml
│       ├── nlo2-meps4-3-6
│       │   └── Sherpa.yaml
│       ├── nlo2-meps4-5-6
│       │   └── Sherpa.yaml
│       ├── nlo2-meps4-9-6
│       │   └── Sherpa.yaml
│       ├── nlo3-meps4-5-6
│       │   └── Sherpa.yaml
│       └── nlo4-meps4-5-6
│           └── Sherpa.yaml
├── MENLOPS_WLHC
│   └── rel-3.0.0
│       ├── mcnlo2-meps0-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-20
│       │   └── Sherpa.yaml
│       ├── nlo2-meps3-15
│       │   └── Sherpa.yaml
│       ├── nlo2-meps3-20
│       │   └── Sherpa.yaml
│       ├── nlo2-meps3-20-hadue
│       │   └── Sherpa.yaml
│       ├── nlo2-meps3-20-mperp
│       │   └── Sherpa.yaml
│       ├── nlo2-meps3-40
│       │   └── Sherpa.yaml
│       ├── nlo3-meps2-10
│       │   └── Sherpa.yaml
│       ├── nlo3-meps2-20
│       │   └── Sherpa.yaml
│       ├── nlo3-meps2-20-hadue
│       │   └── Sherpa.yaml
│       ├── nlo3-meps2-40
│       │   └── Sherpa.yaml
│       ├── nlo4-meps3-20
│       │   └── Sherpa.yaml
│       └── nlo4-meps3-20-hadue
│           └── Sherpa.yaml
├── MENLOPS_WTEV
│   └── rel-3.0.0
│       ├── mcnlo2-meps0-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps0-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps1-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-15-comix
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-20-comix
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-20-comix-hadue
│       │   └── Sherpa.yaml
│       └── nlo0-meps3-40-comix
│           └── Sherpa.yaml
├── MENLOPS_ZLHC
│   └── rel-3.0.0
│       ├── mcnlo2-meps0-20
│       │   └── Sherpa.yaml
│       ├── mcnlo2-meps3-20
│       │   └── Sherpa.yaml
│       ├── mcnlo2-meps3-20-hadue
│       │   └── Sherpa.yaml
│       ├── mcnlo3-meps3-20
│       │   └── Sherpa.yaml
│       ├── mcnlo3-meps3-20-hadue
│       │   └── Sherpa.yaml
│       ├── mcnlo4-meps3-20
│       │   └── Sherpa.yaml
│       ├── mcnlo4-meps3-20-hadue
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-20
│       │   └── Sherpa.yaml
│       └── nlo0-meps3-20-hadue
│           └── Sherpa.yaml
├── MENLOPS_ZTEV
│   └── rel-3.0.0
│       ├── mcnlo2-meps0-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps0-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps1-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-15-comix
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-15-comix-hadue
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-20
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-20-comix
│       │   └── Sherpa.yaml
│       ├── nlo0-meps3-40-comix
│       │   └── Sherpa.yaml
│       ├── nlo2-meps3-20
│       │   └── Sherpa.yaml
│       └── nlo3-meps2-20
│           └── Sherpa.yaml
├── PL_DIPHOTON
│   └── rel-3.0.0
│       ├── me
│       │   └── Sherpa.yaml
│       └── ps
│           └── Sherpa.yaml
├── PL_HQCD
│   └── rel-3.0.0
│       ├── 0j-60gev
│       │   └── Sherpa.yaml
│       ├── 1j-60gev
│       │   └── Sherpa.yaml
│       ├── 2j-40gev
│       │   └── Sherpa.yaml
│       ├── 2j-60gev
│       │   └── Sherpa.yaml
│       ├── 2j-90gev
│       │   └── Sherpa.yaml
│       ├── 2l-0j-40gev
│       │   └── Sherpa.yaml
│       ├── 2l-1j-40gev
│       │   └── Sherpa.yaml
│       ├── 2l-2j-40gev
│       │   └── Sherpa.yaml
│       ├── 3j-25gev
│       │   └── Sherpa.yaml
│       ├── 3l-2j-20gev
│       │   └── Sherpa.yaml
│       ├── 3l-2j-40gev
│       │   └── Sherpa.yaml
│       └── 3l-2j-60gev
│           └── Sherpa.yaml
├── PL_HVBF
│   └── rel-3.0.0
│       ├── LO
│       │   └── Sherpa.yaml
│       ├── LO_ggh_eft
│       │   └── Sherpa.yaml
│       ├── LO_vbf
│       │   └── Sherpa.yaml
│       ├── LO_vh
│       │   └── Sherpa.yaml
│       ├── MEPS
│       │   └── Sherpa.yaml
│       ├── MEPSNLO_ggh
│       │   └── Sherpa.yaml
│       ├── MEPS_vbf
│       │   └── Sherpa.yaml
│       ├── MEPS_vh
│       │   └── Sherpa.yaml
│       ├── NLO
│       │   └── Sherpa.yaml
│       ├── NLOPS
│       │   └── Sherpa.yaml
│       ├── NLOPS_vbf
│       │   └── Sherpa.yaml
│       ├── NLOPS_vh
│       │   └── Sherpa.yaml
│       ├── NLO_ggh_eft
│       │   └── Sherpa.yaml
│       ├── NLO_vbf
│       │   └── Sherpa.yaml
│       └── NLO_vh
│           └── Sherpa.yaml
├── PL_HWJ
│   └── rel-3.0.0
│       ├── HWJ_2-0j_30gev
│       │   └── Sherpa.yaml
│       ├── HWJ_2-1j_30gev
│       │   └── Sherpa.yaml
│       ├── HWJ_2-2j_15gev
│       │   └── Sherpa.yaml
│       ├── HWJ_2-2j_30gev
│       │   └── Sherpa.yaml
│       ├── HWJ_2-2j_30gev_fdown
│       │   └── Sherpa.yaml
│       ├── HWJ_2-2j_30gev_fup
│       │   └── Sherpa.yaml
│       ├── HWJ_2-2j_30gev_rdown
│       │   └── Sherpa.yaml
│       ├── HWJ_2-2j_30gev_rup
│       │   └── Sherpa.yaml
│       ├── HWJ_2-2j_60gev
│       │   └── Sherpa.yaml
│       ├── HWJ_3-2j_15gev
│       │   └── Sherpa.yaml
│       ├── HWJ_3-2j_30gev
│       │   └── Sherpa.yaml
│       ├── HWJ_3-2j_30gev_fdown
│       │   └── Sherpa.yaml
│       ├── HWJ_3-2j_30gev_fup
│       │   └── Sherpa.yaml
│       ├── HWJ_3-2j_30gev_rdown
│       │   └── Sherpa.yaml
│       ├── HWJ_3-2j_30gev_rup
│       │   └── Sherpa.yaml
│       ├── HWJ_3-2j_60gev
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_hwj
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_hwj_me
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_hwj_nosc
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_os_hwj
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_os_hwj_me
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_os_hwj_nosc
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_os_v3j
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_os_v3j_me
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_os_v3j_nosc
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_v3j
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_v3j_me
│       │   └── Sherpa.yaml
│       ├── SC_0-0j_v3j_nosc
│       │   └── Sherpa.yaml
│       ├── V3J_2-0j_30gev
│       │   └── Sherpa.yaml
│       ├── V3J_2-1j_30gev
│       │   └── Sherpa.yaml
│       ├── V3J_2-2j_15gev
│       │   └── Sherpa.yaml
│       ├── V3J_2-2j_30gev
│       │   └── Sherpa.yaml
│       ├── V3J_2-2j_30gev_fdown
│       │   └── Sherpa.yaml
│       ├── V3J_2-2j_30gev_fup
│       │   └── Sherpa.yaml
│       ├── V3J_2-2j_30gev_rdown
│       │   └── Sherpa.yaml
│       ├── V3J_2-2j_30gev_rup
│       │   └── Sherpa.yaml
│       ├── V3J_2-2j_60gev
│       │   └── Sherpa.yaml
│       ├── V3J_3-2j_15gev
│       │   └── Sherpa.yaml
│       ├── V3J_3-2j_30gev
│       │   └── Sherpa.yaml
│       ├── V3J_3-2j_30gev_fdown
│       │   └── Sherpa.yaml
│       ├── V3J_3-2j_30gev_fup
│       │   └── Sherpa.yaml
│       ├── V3J_3-2j_30gev_rdown
│       │   └── Sherpa.yaml
│       ├── V3J_3-2j_30gev_rup
│       │   └── Sherpa.yaml
│       └── V3J_3-2j_60gev
│           └── Sherpa.yaml
├── PL_JQCD1800
│   └── rel-3.0.0
│       ├── 2j-20gev
│       │   └── Sherpa.yaml
│       └── 4j-20gev
│           └── Sherpa.yaml
├── PL_JQCD1960
│   └── rel-3.0.0
│       ├── 2j-20gev
│       │   └── Sherpa.yaml
│       ├── 4j-20gev
│       │   └── Sherpa.yaml
│       ├── 4j-40gev
│       │   └── Sherpa.yaml
│       └── 4j-60gev
│           └── Sherpa.yaml
├── PL_QCUT (✔)
├── PL_TTLHC ✔
├── PL_UN2LOPS (✔)
├── PL_WWTEV
│   └── rel-3.0.0
│       ├── 0j-15gev
│       │   └── Sherpa.yaml
│       ├── 1j-15gev
│       │   └── Sherpa.yaml
│       ├── 2j-15gev
│       │   └── Sherpa.yaml
│       ├── 2j-30gev
│       │   └── Sherpa.yaml
│       └── 2j-60gev
│           └── Sherpa.yaml
├── PL_YJETS
│   └── rel-3.0.0
│       ├── me
│       │   └── Sherpa.yaml
│       └── ps
│           └── Sherpa.yaml
├── PL_YYJ
│   └── rel-3.0.0
│       ├── 0-0j_20gev
│       │   └── Sherpa.yaml
│       ├── 2-0j_20gev
│       │   └── Sherpa.yaml
│       ├── 2-1j_10gev
│       │   └── Sherpa.yaml
│       ├── 2-1j_20gev
│       │   └── Sherpa.yaml
│       ├── 2-1j_40gev
│       │   └── Sherpa.yaml
│       ├── 2-2j_20gev
│       │   └── Sherpa.yaml
│       ├── 2-3j_10gev
│       │   └── Sherpa.yaml
│       ├── 2-3j_20gev
│       │   └── Sherpa.yaml
│       ├── 2-3j_20gev_fdown
│       │   └── Sherpa.yaml
│       ├── 2-3j_20gev_fup
│       │   └── Sherpa.yaml
│       ├── 2-3j_20gev_rdown
│       │   └── Sherpa.yaml
│       ├── 2-3j_20gev_rup
│       │   └── Sherpa.yaml
│       └── 2-3j_40gev
│           └── Sherpa.yaml
├── XS_BOSON_PRODUCTION
│   └── rel-3.0.0
│       ├── amegic
│       │   └── Sherpa.yaml
│       ├── amegic_ehc
│       │   └── Sherpa.yaml
│       ├── comix
│       │   └── Sherpa.yaml
│       └── comix_ehc
│           └── Sherpa.yaml
├── XS_ILC
│   └── rel-3.0.0
│       ├── amegic_bbxbbxmumux
│       │   └── Sherpa.yaml
│       ├── amegic_bbxbbxmumux_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxbbxmumux_ew
│       │   └── Sherpa.yaml
│       ├── amegic_bbxbbxmumux_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxbbxmumux_noh
│       │   └── Sherpa.yaml
│       ├── amegic_bbxbbxmumux_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxbbxmumux_noh_ew
│       │   └── Sherpa.yaml
│       ├── amegic_bbxbbxmumux_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxddxuux
│       │   └── Sherpa.yaml
│       ├── amegic_bbxddxuux_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxddxuux_ew
│       │   └── Sherpa.yaml
│       ├── amegic_bbxddxuux_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxdxuenex
│       │   └── Sherpa.yaml
│       ├── amegic_bbxdxuenex_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxdxuenex_ew
│       │   └── Sherpa.yaml
│       ├── amegic_bbxdxuenex_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxexneenex
│       │   └── Sherpa.yaml
│       ├── amegic_bbxexneenex_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxexnemunmux
│       │   └── Sherpa.yaml
│       ├── amegic_bbxexnemunmux_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxgggg
│       │   └── Sherpa.yaml
│       ├── amegic_bbxgggg_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxmuxnmumunmux
│       │   └── Sherpa.yaml
│       ├── amegic_bbxmuxnmumunmux_360
│       │   └── Sherpa.yaml
│       ├── amegic_bbxuuxgg
│       │   └── Sherpa.yaml
│       ├── amegic_bbxuuxgg_360
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxddx
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxddx_360
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxddx_ew
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxddx_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxddx_noh
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxddx_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxddx_noh_ew
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxddx_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxeex
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxeex_360
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxeex_noh
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxeex_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxmumux
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxmumux_360
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxmumux_noh
│       │   └── Sherpa.yaml
│       ├── amegic_eexuuxmumux_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxmumuxeex
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxmumuxeex_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxmumuxeex_noh
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxmumuxeex_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxmunmuxenex
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxmunmuxenex_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxmunmuxenex_noh
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxmunmuxenex_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxudxenex
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxudxenex_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxudxenex_noh
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxudxenex_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxddx
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxddx_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxddx_ew
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxddx_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxddx_noh
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxddx_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxddx_noh_ew
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxddx_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxuux
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxuux_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxuux_ew
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxuux_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxuux_noh
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxuux_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxuux_noh_ew
│       │   └── Sherpa.yaml
│       ├── amegic_mumuxuuxuux_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_nenexudxenex
│       │   └── Sherpa.yaml
│       ├── amegic_nenexudxenex_360
│       │   └── Sherpa.yaml
│       ├── amegic_nenexudxenex_noh
│       │   └── Sherpa.yaml
│       ├── amegic_nenexudxenex_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_nenexudxmunmux
│       │   └── Sherpa.yaml
│       ├── amegic_nenexudxmunmux_360
│       │   └── Sherpa.yaml
│       ├── amegic_nenexudxmunmux_noh
│       │   └── Sherpa.yaml
│       ├── amegic_nenexudxmunmux_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_nenexuuxddx
│       │   └── Sherpa.yaml
│       ├── amegic_nenexuuxddx_360
│       │   └── Sherpa.yaml
│       ├── amegic_nenexuuxddx_ew
│       │   └── Sherpa.yaml
│       ├── amegic_nenexuuxddx_ew_360
│       │   └── Sherpa.yaml
│       ├── amegic_nenexuuxddx_noh
│       │   └── Sherpa.yaml
│       ├── amegic_nenexuuxddx_noh_360
│       │   └── Sherpa.yaml
│       ├── amegic_nenexuuxddx_noh_ew
│       │   └── Sherpa.yaml
│       ├── amegic_nenexuuxddx_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxbbxmumux
│       │   └── Sherpa.yaml
│       ├── comix_bbxbbxmumux_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxbbxmumux_ew
│       │   └── Sherpa.yaml
│       ├── comix_bbxbbxmumux_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxbbxmumux_noh
│       │   └── Sherpa.yaml
│       ├── comix_bbxbbxmumux_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxbbxmumux_noh_ew
│       │   └── Sherpa.yaml
│       ├── comix_bbxbbxmumux_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxddxuux
│       │   └── Sherpa.yaml
│       ├── comix_bbxddxuux_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxddxuux_ew
│       │   └── Sherpa.yaml
│       ├── comix_bbxddxuux_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxdxuenex
│       │   └── Sherpa.yaml
│       ├── comix_bbxdxuenex_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxdxuenex_ew
│       │   └── Sherpa.yaml
│       ├── comix_bbxdxuenex_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxexneenex
│       │   └── Sherpa.yaml
│       ├── comix_bbxexneenex_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxexnemunmux
│       │   └── Sherpa.yaml
│       ├── comix_bbxexnemunmux_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxgggg
│       │   └── Sherpa.yaml
│       ├── comix_bbxgggg_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxmuxnmumunmux
│       │   └── Sherpa.yaml
│       ├── comix_bbxmuxnmumunmux_360
│       │   └── Sherpa.yaml
│       ├── comix_bbxuuxgg
│       │   └── Sherpa.yaml
│       ├── comix_bbxuuxgg_360
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxddx
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxddx_360
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxddx_ew
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxddx_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxddx_noh
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxddx_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxddx_noh_ew
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxddx_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxeex
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxeex_360
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxeex_noh
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxeex_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxmumux
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxmumux_360
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxmumux_noh
│       │   └── Sherpa.yaml
│       ├── comix_eexuuxmumux_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxmumuxeex
│       │   └── Sherpa.yaml
│       ├── comix_mumuxmumuxeex_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxmumuxeex_noh
│       │   └── Sherpa.yaml
│       ├── comix_mumuxmumuxeex_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxmunmuxenex
│       │   └── Sherpa.yaml
│       ├── comix_mumuxmunmuxenex_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxmunmuxenex_noh
│       │   └── Sherpa.yaml
│       ├── comix_mumuxmunmuxenex_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxudxenex
│       │   └── Sherpa.yaml
│       ├── comix_mumuxudxenex_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxudxenex_noh
│       │   └── Sherpa.yaml
│       ├── comix_mumuxudxenex_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxddx
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxddx_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxddx_ew
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxddx_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxddx_noh
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxddx_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxddx_noh_ew
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxddx_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxuux
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxuux_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxuux_ew
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxuux_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxuux_noh
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxuux_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxuux_noh_ew
│       │   └── Sherpa.yaml
│       ├── comix_mumuxuuxuux_noh_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_nenexudxenex
│       │   └── Sherpa.yaml
│       ├── comix_nenexudxenex_360
│       │   └── Sherpa.yaml
│       ├── comix_nenexudxenex_noh
│       │   └── Sherpa.yaml
│       ├── comix_nenexudxenex_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_nenexudxmunmux
│       │   └── Sherpa.yaml
│       ├── comix_nenexudxmunmux_360
│       │   └── Sherpa.yaml
│       ├── comix_nenexudxmunmux_noh
│       │   └── Sherpa.yaml
│       ├── comix_nenexudxmunmux_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_nenexuuxddx
│       │   └── Sherpa.yaml
│       ├── comix_nenexuuxddx_360
│       │   └── Sherpa.yaml
│       ├── comix_nenexuuxddx_ew
│       │   └── Sherpa.yaml
│       ├── comix_nenexuuxddx_ew_360
│       │   └── Sherpa.yaml
│       ├── comix_nenexuuxddx_noh
│       │   └── Sherpa.yaml
│       ├── comix_nenexuuxddx_noh_360
│       │   └── Sherpa.yaml
│       ├── comix_nenexuuxddx_noh_ew
│       │   └── Sherpa.yaml
│       └── comix_nenexuuxddx_noh_ew_360
│           └── Sherpa.yaml
├── XS_MC4LHC
│   └── rel-3.0.0
│       ├── amegic
│       │   └── Sherpa.yaml
│       └── comix
│           └── Sherpa.yaml
└── XS_SUSY
    └── rel-3.0.0
        ├── amegic
        │   └── Sherpa.yaml
        ├── amegic_WP
        │   └── Sherpa.yaml
        ├── amegic_WW
        │   └── Sherpa.yaml
        ├── amegic_ZZ
        │   └── Sherpa.yaml
        ├── amegic_btbar
        │   └── Sherpa.yaml
        ├── amegic_dd
        │   └── Sherpa.yaml
        ├── amegic_ee
        │   └── Sherpa.yaml
        ├── amegic_enue
        │   └── Sherpa.yaml
        ├── amegic_gamgam
        │   └── Sherpa.yaml
        ├── amegic_gamglu
        │   └── Sherpa.yaml
        ├── amegic_gluglu
        │   └── Sherpa.yaml
        ├── amegic_taunutau
        │   └── Sherpa.yaml
        ├── amegic_tautau
        │   └── Sherpa.yaml
        ├── amegic_uu
        │   └── Sherpa.yaml
        ├── comix_WP
        │   └── Sherpa.yaml
        ├── comix_WW
        │   └── Sherpa.yaml
        ├── comix_ZZ
        │   └── Sherpa.yaml
        ├── comix_btbar
        │   └── Sherpa.yaml
        ├── comix_dd
        │   └── Sherpa.yaml
        ├── comix_ee
        │   └── Sherpa.yaml
        ├── comix_enue
        │   └── Sherpa.yaml
        ├── comix_gamgam
        │   └── Sherpa.yaml
        ├── comix_gamglu
        │   └── Sherpa.yaml
        ├── comix_gluglu
        │   └── Sherpa.yaml
        ├── comix_taunutau
        │   └── Sherpa.yaml
        ├── comix_tautau
        │   └── Sherpa.yaml
        └── comix_uu
            └── Sherpa.yaml
```
