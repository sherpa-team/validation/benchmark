(level2){
  PIECE_SETUP Level_2.C (noSC){ }(noSC);
  PIECE_SETUP Level_2.C (withSC){ }(withSC);
}(level2);

(noSC){
  PIECE_SETUP Level_3.C (level3){ }(level3);
  PATH_PIECE noSC/;
  LINE_COLOUR 2;
  LEGEND_TITLE w/o SC;
}(noSC);

(withSC){
  PIECE_SETUP Level_3.C (level3){ }(level3);
  PATH_PIECE withSC/;
  LINE_COLOUR 3;
  LEGEND_TITLE w/ SC;
}(withSC);
