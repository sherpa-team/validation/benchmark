(level1){
  PIECE_SETUP Level_1.C (run){ }(run);
  PIECE_SETUP Level_1.C (reference){ }(reference);
}(level1);

(run){
  PIECE_SETUP Level_2.C (level2){ }(level2);
  PATH_PIECE TAG_RUN_PATH;
  LINE_STYLE 1;
  LEGEND_TITLE TAG_RUN_LEGEND;
  @@ ANAPATH TAG_ANA_PATH;
}(run);

(reference){
  PIECE_SETUP Level_2.C (level2){ }(level2);
  PATH_PIECE TAG_REF_PATH;
  LINE_STYLE 2;
  LEGEND_TITLE TAG_REF_LEGEND;
  @@ ANAPATH TAG_ARF_PATH;
}(reference);
