print(){
    if test -f $1; then
        if tail -1 $1 | grep -q -eoptimization -eintegration; then
            tail -2 $1 | awk '{ print $0; exit; }';
        elif tail -n 500 $1 | grep -q "Generated events"; then
            tail -n 500 $1 | grep "Generated events";
        else tail -1 $1;
        fi;
    else echo \"not running\";
    fi;
};
list(){
    jn=`! test -f $1/$3.split ||
    head -1 $1/$3.split | awk '{ print $2; }'`;
    echo -en "  \033[1m"$subdir/$1"\033[0m";
    echo -en "`test -z "$5" || echo \" [\033[34m\"`";
    echo -en "$5`test -z \"$5\" || echo \"\033[0m]\"` ";
    echo -e `test -z "$jn" || echo -n '('$jn') '`"{";
    if test -d $1; then
        echo -n "    `test -f $1/$3.split && echo -n '0 '`";
        echo "master: `print $1/$3`";
        j=1;
        for I in $1/$2"_"*.log; do if test -f $I; then
                echo "    $j `echo $I | awk '{ \
                        split($1,a,"'$2'_"); split(a[2],b,".log"); \
                        print b[1]; }'`: `print $I`";
                j=`expr $j + 1`;
            fi;
        done;
    fi;
    j=`expr $j - 1`;
    echo "  } `test -z $jn || test $j -eq $jn ||
                    echo 'jobs left'`";
};
runscheck(){
    od=$PWD;
    for subdir in $1; do
        if test -d $subdir/$2; then
            echo -e "Checking runs in $subdir {";
            cd $subdir;
            for i in $2/*; do
                if test -f $(echo $i/$(basename $i)*.sh |
                        awk '{ print $1; }'); then
                    if test -f $i/$(basename $i).sh; then
                        list $i job run.log $subdir;
                    else
                        for j in $(echo $i/$(basename $i)*.sh |
                            awk '{ for (i=1;i<=NF;++i) { n=split($i,a,"/"); \
                                   split(a[n],b,"[.]"); print b[2]; } }'
                            ); do
                            list $i job.$j run.log.$j $subdir $j;
                        done;
                    fi;
                fi;
            done;
            if test -f $2/$2.sh; then
                if test -f $2/$2.sh; then
                    list $2 job run.log $subdir;
                else
                    for j in $(echo $2/$2.sh |
                        awk '{ for (i=1;i<=NF;++i) { n=split($2,a,"/"); \
                                   split(a[n],b,"[.]"); print b[2]; } }'
                        ); do
                        list $2 job.$j run.log.$j $subdir $j;
                    done;
                fi;
            fi;
            echo "}";
            cd $od;
        fi;
    done;
};
