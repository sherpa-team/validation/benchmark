runsrest()
{
  od=$PWD; list=$1; rp=$2; scm=$3
  for dir in $list; do
    if test -d $dir; then
      echo "Checking runs in '$dir' ... {";
      opi=$PWD; cd $dir;
      for pn in $rp/*; do
        if test -d $pn; then
          echo -e "  \033[1m"$dir/$pn"\033[0m {"
	  opii=$PWD; cd $pn
          for jdl in job_*.jdl; do
	    if ! test -f $jdl; then break; fi
            base=$(basename $jdl .jdl)
            if ! test -f $base".log"; then
              echo "    "$base" in "$PWD
              PROXYTIME=`grid-proxy-info | grep timeleft | awk '{ split($3,a,":"); \
                print a[1]*3600+a[2]*60+a[3]; }'`
              if [ "$scm" != "" ]; then
                if [ "$PROXYTIME" != "" ] && test $PROXYTIME -lt 3600; then
                    echo "proxy expires in < 1hr. abort"
                    exit 1
                fi
                echo $base" in "$PWD >> submit_split_jobs.log
                $scm $jdl 2>&1 | tee -a submit_split_jobs.log
              fi
            fi
          done
          cd $opii > /dev/null
          echo "  }"
        fi
      done
      cd $opi > /dev/null
      echo "}"
    fi
  done
}
