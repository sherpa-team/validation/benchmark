TARGET = $(notdir $(PWD))

export XSC_RUN_CMD := \
	sed -e's:__REF_TITLE__:$(REF_TITLE):g' \
	  -e's:__RUN_TITLE__:$(RUN_TITLE):g' \
	  -e's:__REF_PATH__:$(REF_PATH):g' \
	  -e's:__RUN_PATH__:$(RUN_PATH):g' \
	  -e's:__OUTPUT_PATH__:$(OUTPUT_PATH)/$(TARGET):g' \
	< $$xsc > testxs.conf; $(TEST_XS) < testxs.conf
export COMPARE_RUN_CMD := \
	sed -e's:__REF_TITLE__:$(REF_TITLE):g' \
	  -e's:__RUN_TITLE__:$(RUN_TITLE):g' \
	  -e's:__REF_PATH__:$(REF_PATH):g' \
	  -e's:__RUN_PATH__:$(RUN_PATH):g' \
	  -e's:__ANA_PATH__:$(ANA_PATH):g' \
	  -e's:__ARF_PATH__:$(ARF_PATH):g' \
	  -e's:__OUTPUT_PATH__:$(OUTPUT_PATH)/$(TARGET):g' \
	  -e's:__RIVET_MKHTML__:$(RIVET_MKHTML):g' \
	  -e's:Compare:$(COMPARE):g' < analyse.in > analyse; \
	chmod 755 analyse; PATH=$(SCRIPT_PATH):$$PATH ./analyse \

.PHONY: clean-libs clean-runs clean-plots clean-checks libs runs plots checks

clean-runs:
	@subdirs='$(RUNDIRS)'; for i in $$subdirs; do \
	  sf=`echo $$i | awk '{ n=split($$1,a,"/"); print a[n]; }'`; \
	  echo "making clean-runs in '$$sf'"; \
	  if test -f $(RUN_PATH)/$$i/$$sf.log || \
	    test -f $(RUN_PATH)/$$i/$$sf.libs.log; then $(JOB_CANCEL_COMMAND) \
	    `grep -ho 'https:.*:9000.*$$' $(RUN_PATH)/$$i/$$sf.log \
	      $(RUN_PATH)/$$i/$$sf.libs.log $(RUN_PATH)/$$i/submit_split_jobs.log`; \
	  fi; \
	  test $(CLEAN_ANALYSIS) -eq 0 || rm -rf $(RUN_PATH)/$$i/$(ANA_PATH)_$(RANDOMS)* $(RUN_PATH)/$$i/$(ANA_PATH)_sum*; \
	  test $(CLEAN_RESULTS) -eq 0 || rm -rf $(RUN_PATH)/$$i/$(RES_PATH)*; \
	  rm -f $(RUN_PATH)/$$i/job_*; rm -f $(RUN_PATH)/$$i/*.log*; done; 

clean-libs: clean-runs
	rm -rf $(RUN_PATH)

clean-plots:
	rm -rf $(OUTPUT_PATH)/$(TARGET)

clean-checks: clean-plots

$(RUN_PATH):
	@if ! test -d $(RUN_PATH); then mkdir $(RUN_PATH); fi; \
	for i in $(LIBDIRS); do if ! test -d $@/$$i; then $(GRID_LIB_RUN_CMD) \
	  -p$(SETUP_PATH)/$$i -n$@/$$i $(EVENTTAG) -r$(RES_PATH)/ -a$(ANA_PATH)/; fi; done

%.log:
	@cur=`echo $(dir $@) | sed 's/\/$$//'`; \
	ct=`echo $(subst $(RUN_PATH),$(SETUP_PATH),$(dir $@)) | sed 's/\/$$//'`; \
	if test -z "$(LIBDIRS)"; then \
	  libdirarg=""; \
	else \
	  libdirarg="-l$(RUN_PATH)/$(word 1,$(LIBDIRS))"; \
	fi; \
	if test -d $$cur; then $(GRID_RERUN_CMD) -p$$cur \
	  -a"-a$(ANA_PATH)/ -e$(EVENTS)"; else \
	  $(GRID_RUN_CMD) $$libdirarg $(EVENTTAG) \
	  -p$$ct -n$$cur -r$(RES_PATH)/ -a$(ANA_PATH)/ \
	  `test -z '$(NOSUBMIT)' || echo -M`; fi

%/$(ANA_PATH)_sum: %/$(ANA_PATH)_$(RANDOMS)
	cd $(subst /$(ANA_PATH)_sum,,$@); \
	dbs=$$(find $(ANA_PATH)_$(RANDOMS)/ -name '*.db'); \
	if test -z "$$dbs"; then $(ANA_SUM) -pl5 -i"$(ANA_PATH)_$(RANDOMS)/" -o$(ANA_PATH)_sum/ 2>&1 > /dev/null; else \
	files=$$(for i in $$dbs; do basename $$(echo $$i) .db; done | sort | uniq); for i in $$files; do \
	$(COMBINE_ANALYSIS) -m=cmb $(ANA_PATH)_sum/$$i $(ANA_PATH)_$(RANDOMS)/$${i}.db; done; fi; \
	test -d $(ANA_PATH)_sum && touch $(ANA_PATH)_sum; \
	cd ..;

%/$(ANA_PATH)_sum.yoda.gz: %/$(ANA_PATH)_$(RANDOMS).yoda.gz
	cd $(subst /$(ANA_PATH)_sum.yoda.gz,,$@); \
	$(YODAMERGE) $(ANA_PATH)_$(RANDOMS).yoda.gz -o $(ANA_PATH)_sum.yoda.gz; \
	cd ..;

%/$(ANA_PATH)_sum.SL.yoda.gz: %/$(ANA_PATH)_$(RANDOMS).SL.yoda.gz
	cd $(subst /$(ANA_PATH)_sum.SL.yoda.gz,,$@); \
	$(YODAMERGE) $(ANA_PATH)_$(RANDOMS).SL.yoda.gz -o $(ANA_PATH)_sum.SL.yoda.gz; \
	cd ..;

%/$(ANA_PATH)_sum.H.yoda.gz: %/$(ANA_PATH)_$(RANDOMS).H.yoda.gz
	cd $(subst /$(ANA_PATH)_sum.H.yoda.gz,,$@); \
	$(YODAMERGE) $(ANA_PATH)_$(RANDOMS).H.yoda.gz -o $(ANA_PATH)_sum.H.yoda.gz; \
	cd ..;

%/$(ANA_PATH)_sum.S.yoda.gz: %/$(ANA_PATH)_$(RANDOMS).S.yoda.gz
	cd $(subst /$(ANA_PATH)_sum.S.yoda.gz,,$@); \
	$(YODAMERGE) $(ANA_PATH)_$(RANDOMS).S.yoda.gz -o $(ANA_PATH)_sum.S.yoda.gz; \
	cd ..;

libs: $(RUN_PATH)
	@subdirs='$(filter-out $(LIBDIRS),$(RUNDIRS))'; for i in $$subdirs; do \
	  sf=`echo $$i | awk '{ n=split($$1,a,"/"); print a[n]; }'`; \
	  if ! test -d $(RUN_PATH)/$$i; then \
	  $(MAKE) $(MYMFLAGS) $(RUN_PATH)/$$i/$$sf.log NOSUBMIT=1; fi; done

runs: $(foreach CUR,$(RUNDIRS),$(addsuffix .log,$(RUN_PATH)/$(CUR)/$(CUR)))

sums:
	@for rundir in $(RUNDIRS); do \
	  ls $(RUN_PATH)/$$rundir/$(ANA_PATH)_$(RANDOMS) > /dev/null 2>&1 && $(MAKE) $(RUN_PATH)/$$rundir/$(ANA_PATH)_sum; \
	  ls $(RUN_PATH)/$$rundir/$(ANA_PATH)_$(RANDOMS).yoda.gz > /dev/null 2>&1 && $(MAKE) $(RUN_PATH)/$$rundir/$(ANA_PATH)_sum.yoda.gz; \
	  ls $(RUN_PATH)/$$rundir/$(ANA_PATH)_$(RANDOMS).SL.yoda.gz > /dev/null 2>&1 && $(MAKE) $(RUN_PATH)/$$rundir/$(ANA_PATH)_sum.SL.yoda.gz; \
	  ls $(RUN_PATH)/$$rundir/$(ANA_PATH)_$(RANDOMS).H.yoda.gz > /dev/null 2>&1 && $(MAKE) $(RUN_PATH)/$$rundir/$(ANA_PATH)_sum.H.yoda.gz; \
	  ls $(RUN_PATH)/$$rundir/$(ANA_PATH)_$(RANDOMS).S.yoda.gz > /dev/null 2>&1 && $(MAKE) $(RUN_PATH)/$$rundir/$(ANA_PATH)_sum.S.yoda.gz; \
	done; exit 0;

export HTML_DESCRIPTION
plots: sums $(OUTPUT_PATH)/$(TARGET)
	@echo >> "$(HTML_OUTPUT)";
	@echo "$$HTML_DESCRIPTION" >> "$(HTML_OUTPUT)";
	@echo >> "$(HTML_OUTPUT)";

dist: sums
	cur='$(DISTDIR)/$(DISTRUN_PATH)'; mkdir -p $$cur; \
	subdirs='$(RUNDIRS)'; for i in $$subdirs; do \
	  ( mkdir -p $$cur/$$i ) || exit 1; \
	  if test -e $(RUN_PATH)/$$i/$(ANA_PATH)_sum; then \
	    cp -dpr $(RUN_PATH)/$$i/$(ANA_PATH)_sum $$cur/$$i/$(DISTANA_PATH)_sum; \
	  fi; \
	  if test -e $(RUN_PATH)/$$i/$(ANA_PATH)_sum.yoda.gz; then \
	    cp -dpr $(RUN_PATH)/$$i/$(ANA_PATH)_sum.yoda.gz $$cur/$$i/$(DISTANA_PATH)_sum.yoda.gz; \
	  fi; \
	  if test -e $(RUN_PATH)/$$i/$(ANA_PATH)_sum.SL.yoda.gz; then \
	    cp -dpr $(RUN_PATH)/$$i/$(ANA_PATH)_sum.SL.yoda.gz $$cur/$$i/$(DISTANA_PATH)_sum.SL.yoda.gz; \
	  fi; \
	  if test -e $(RUN_PATH)/$$i/$(ANA_PATH)_sum.H.yoda.gz; then \
	    cp -dpr $(RUN_PATH)/$$i/$(ANA_PATH)_sum.H.yoda.gz $$cur/$$i/$(DISTANA_PATH)_sum.H.yoda.gz; \
	  fi; \
	  if test -e $(RUN_PATH)/$$i/$(ANA_PATH)_sum.S.yoda.gz; then \
	    cp -dpr $(RUN_PATH)/$$i/$(ANA_PATH)_sum.S.yoda.gz $$cur/$$i/$(DISTANA_PATH)_sum.S.yoda.gz; \
	  fi; \
	done;

.PHONY: $(RUN_PATH)
