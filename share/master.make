MAKEFLAGS = -j1

BINARIES = Sherpa

PLOT_TITLE ?= Sherpa results

DISTDIR ?= benchmark

ifneq (,$(BUILD_PATH))
export BIN_PATH ?= $(BUILD_PATH)/bin
export RUN_PATH ?= $(shell cd $(BUILD_PATH); svn info | awk \
	'{ if ($$1=="URL:") { split($$2,a,"svn/sherpa/"); \
	sub("/","_",a[2]); sub("branches_","",a[2]); \
	sub("_SHERPA","",a[2]); print a[2]; } }')_$(shell \
	cd $(BUILD_PATH); svnversion | awk '{ sub(":","-",$$1); \
	print $1; }')
endif

export SETUP_PATH ?= trunk
export REF_PATH ?= trunk
export RUN_PATH ?= current
export DISTRUN_PATH ?= $(RUN_PATH)
export ANA_PATH ?= a_1
export RES_PATH ?= r_1
export DISTANA_PATH ?= $(ANA_PATH)
export ARF_PATH ?= a_1

export REF_TITLE ?= $(REF_PATH)
export RUN_TITLE ?= $(RUN_PATH)

export CLEAN_RESULTS ?= 0
export CLEAN_ANALYSIS ?= 1

export SCRIPT_PATH := $(PWD)/bin

export GRID_RUN ?= $(SCRIPT_PATH)/grid_run
export GRID_RESUBMIT ?= $(SCRIPT_PATH)/grid_resubmit
export GRID_MAKELIBS ?= $(SCRIPT_PATH)/grid_makelibs
export DIR2DB ?= $(SCRIPT_PATH)/dir2db
export DB2DIR ?= $(SCRIPT_PATH)/db2dir
export TEST_XS ?= $(SCRIPT_PATH)/testxs
export ANA_SUM ?= $(SCRIPT_PATH)/anasum
export COMBINE_ANALYSIS ?= $(BIN_PATH)/Combine_Analysis
export AIDAMERGE ?= $(SCRIPT_PATH)/aidamerge
export YODAMERGE ?= yodamerge # --assume-normalized
export COMPARE ?= Compare
export RIVET_MKHTML ?= rivet-mkhtml --pdf --mc-errs 

export OUTPUT_PATH := $(PWD)/output
export HTML_OUTPUT ?= $(OUTPUT_PATH)/index.html

export RANDOMS = [1-9][1-9][1-9][1-9][1-9][1-9]

override IGNORES += bin share

export SITE_NAME := $(shell echo $$SITE_NAME)

export JOB_RUNTIME ?= 0
export JOB_LIBTIME ?= 7.9
export JT_SAFETY_FACTOR ?= 3

ifeq (UKI-SCOTGRID-DURHAM,$(SITE_NAME))
export JOB_CANCEL_COMMAND = glite-wms-job-cancel --noint
export CE_UNIQUE_ID ?= dur.scotgrid.ac.uk
JOB_SUBMIT_COMMAND = glite-wms-job-submit -a -c $(PWD)/share/wms.conf
else
ifeq (UKI-SCOTGRID-GLASGOW,$(SITE_NAME))
export JOB_CANCEL_COMMAND = glite-wms-job-cancel --noint
export CE_UNIQUE_ID ?= gla.scotgrid.ac.uk
JOB_SUBMIT_COMMAND = glite-wms-job-submit -a -c $(PWD)/share/wms.conf
else
ifeq (UNI-FREIBURG,$(SITE_NAME))
export JOB_CANCEL_COMMAND = torque-job-cancel
export CE_UNIQUE_ID ?= bfg.uni-freiburg.de
JOB_SUBMIT_COMMAND = torque-job-submit
else
ifeq (BWGRID-FREIBURG,$(SITE_NAME))
export JOB_CANCEL_COMMAND = torque-job-cancel
export CE_UNIQUE_ID ?= bwgrid.uni-freiburg.de
JOB_SUBMIT_COMMAND = torque-job-submit
else
ifeq (UCLA_HOFFMAN2,$(SITE_NAME))
export JOB_CANCEL_COMMAND = sge-job-cancel
export CE_UNIQUE_ID ?= hoffman2.idre.ucla.edu
JOB_SUBMIT_COMMAND = sge-job-submit
else
ifeq (SLAC,$(SITE_NAME))
export JOB_CANCEL_COMMAND = lsf-job-cancel
export CE_UNIQUE_ID ?= slac.stanford.edu
JOB_SUBMIT_COMMAND = lsf-job-submit
else
ifeq (FERMILAB,$(SITE_NAME))
export JOB_CANCEL_COMMAND = slurm-job-cancel
export CE_UNIQUE_ID ?= fnal.gov
JOB_SUBMIT_COMMAND = slurm-job-submit
else
ifeq (CERN,$(SITE_NAME))
export JOB_CANCEL_COMMAND = lsf-job-cancel
export CE_UNIQUE_ID ?= lxplus.cern.ch
JOB_SUBMIT_COMMAND = lsf-job-submit
else
ifeq (SCC_GOETTINGEN,$(SITE_NAME))
export JOB_CANCEL_COMMAND = lsf-job-cancel
export CE_UNIQUE_ID ?= gwdu102.gwdg.de
JOB_SUBMIT_COMMAND = lsf-job-submit
else
ifeq (DURHAM-SLURM,$(SITE_NAME))
export JOB_CANCEL_COMMAND = slurm-job-cancel
export CE_UNIQUE_ID ?= phyip3.dur.ac.uk
JOB_SUBMIT_COMMAND = slurm-job-submit
else
ifeq (GOEGRID_GOETTINGEN,$(SITE_NAME))
export JOB_CANCEL_COMMAND = condor_rm
export CE_UNIQUE_ID ?= rocks7.local
JOB_SUBMIT_COMMAND = htcondor-job-submit
endif
endif
endif
endif
endif
endif
endif
endif
endif
endif
endif

ifneq (,$(QUEUE))
  GRID_RUN_OPTS = -t -G"&& RegExp(\"$(QUEUE)\",other.GlueCEUniqueId) "
endif

ifneq (,$(CMD_OPTS))
CMDL_OPTS = -o"$(CMD_OPTS)"
else
CMDL_OPTS = 
endif

export GRID_RUN_CMD = $(GRID_RUN) -b$(BIN_PATH) -LR $(CMDL_OPTS) \
	-G"Requirements = RegExp(\"$(CE_UNIQUE_ID)\", other.GlueCEUniqueId) " \
	-g"$(JOB_SUBMIT_COMMAND)" -W$(shell echo $(JOB_LIBTIME)*3600 | bc -l) \
	-U$(shell echo $(JOB_LIBTIME)*60*$(JT_SAFETY_FACTOR) | bc -l) $(GRID_RUN_OPTS)
ifeq (UKI-SCOTGRID-DURHAM,$(SITE_NAME))
GRID_RUN_CMD += -s/mt/data-grid/$(USER)/set-pheno-env.sh
endif
ifeq (UCLA_HOFFMAN2,$(SITE_NAME))
GRID_RUN_CMD += -s$(PWD)/share/set-ucla-env.sh
endif
ifeq (DURHAM-SLURM,$(SITE_NAME))
GRID_RUN_CMD += -s$(PWD)/share/set-durham-env.sh
endif
ifeq (GOEGRID_GOETTINGEN,$(SITE_NAME))
GRID_RUN_CMD += -s$(PWD)/share/set-goegrid-env.sh
endif
export GRID_LIB_RUN_CMD = $(GRID_RUN_CMD) -m$(GRID_MAKELIBS)
export GRID_RERUN_CMD = $(GRID_RESUBMIT) -g"$(JOB_SUBMIT_COMMAND)"
ifneq ($(JOB_RUNTIME),0)
JOB_GTIME=$(shell echo $(JOB_RUNTIME)*60*$(JT_SAFETY_FACTOR) | bc -l)
GRID_RERUN_CMD += -U$(JOB_GTIME) \
	-a"-E$(JOB_RUNTIME) -U$(JOB_GTIME) -W$(shell echo $(JOB_RUNTIME)*3600 | bc -l)"
endif

export MYMFLAGS ?= --no-print-directory

ALLDIRS := $(shell for i in `ls`; do if test -d $$i; \
	then echo $$i; fi; done)

ifeq (,$(TARGETS))
SUBDIRS = $(filter-out $(IGNORES),$(ALLDIRS))
else
SUBDIRS = $(filter-out $(IGNORES),$(filter $(TARGETS),$(ALLDIRS)))
endif

export DATE := $(shell date)

all:
	@echo 'target all not applicable, please choose one of the following'; \
	echo '  libs runs plots checks dist clean clean-libs clean-runs clean-plots clean-checks'

.PHONY: clean clean-libs clean-runs clean-plots clean-checks \
	libs runs plots dist checks \
	clean-libs-all clean-runs-all clean-plots-all clean-checks-all \
	libs-all runs-all plots-all dist-all checks-all \
	new-libs new-runs new-plots new-checks

new-libs: clean-libs libs
new-runs: clean-runs runs
new-plots: clean-plots plots
new-checks: clean-checks checks

clean-libs: clean-libs-recursive
clean-runs: clean-runs-recursive
clean-plots: clean-plots-recursive
clean-checks: clean-checks-recursive

clean: clean-runs

libs: override SUBDIRS := $(shell for i in $(SUBDIRS); do \
  if test -d $$i -a -d $$i/$(SETUP_PATH); then echo $$i; fi; done)
runs dist clean-libs clean-runs libs-check runs-check: \
  override SUBDIRS := $(shell for i in $(SUBDIRS); do \
  if test -d $$i -a -d $$i/$(RUN_PATH); then echo $$i; fi; done)
plots checks clean-plots clean-checks: \
  override SUBDIRS := $(shell for i in $(SUBDIRS); do \
  if test -d $$i -a -d $$i/$(RUN_PATH); then echo $$i; fi; done)

libs: libs-recursive
runs: runs-recursive
plots: plots-recursive
dist: dist-recursive
checks: checks-recursive

PROXY_CHECK = if ! ( type grid-proxy-info > /dev/null 2>&1 ); \
	then echo "grid-proxy-info command not found. abort."; \
	  exit 1; fi; ptm=$$(grid-proxy-info 2>&1 | grep timeleft | \
	  awk '{ split($$3,a,":"); print a[1]*3600+a[2]*60+a[3]; }'); \
	if test -z "$$ptm" || test $$ptm -lt 3600; then \
	  echo -e "\033[1myour proxy expires in less than 1 hour. time left: " \
	  "`grid-proxy-info 2>&1 | grep timeleft | awk '{ print $$3; }'`\033[0m"; \
	  echo -e "\033[1m\033[31mplease renew your proxy\033[0m"; exit 1; fi

clean-libs-checks: 
	@$(PROXY_CHECK)
clean-runs-checks: 
	@$(PROXY_CHECK)
clean-plots-checks:
clean-checks-checks:
libs-checks: 
	@$(PROXY_CHECK); \
	echo "job runtime set to '$(JOB_LIBTIME)'"; \
	echo "binary path set to '$(BIN_PATH)'"; \
	for i in $(BINARIES); do \
	  if ! test -f $(BIN_PATH)/$$i; then \
	    echo "$$i executeable not found in '$(BIN_PATH)'" \
	    && exit 1; fi; done; \
	if [ "$(CE_UNIQUE_ID)" = "" ]; then echo "unknown ce"; exit 1; fi;
runs-checks: 
	@$(PROXY_CHECK); \
	echo "job runtime set to '$(JOB_RUNTIME)'";
plots-checks:
	@test -d $(OUTPUT_PATH) || mkdir $(OUTPUT_PATH); \
	rm -f $(HTML_OUTPUT); \
	if ! ( type $(COMPARE) > /dev/null 2>&1 ); then \
	  echo "Compare not found. abort." \
	  exit; fi
dist-checks:
	@mkdir $(DISTDIR) || exit 1; \
	list='$(SUBDIRS)'; for subdir in $$list; do \
	  mkdir $(DISTDIR)/$$subdir || exit 1; done
checks-checks:

clean-libs-all:
clean-runs-all:
clean-plots-all:
	@rm -rf $(OUTPUT_PATH)
clean-checks-all:
libs-all:
runs-all:
plots-all:
	@echo >> $(HTML_OUTPUT); \
	sed '1 i \
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\
  <html>\
    <head>\
      <title>$(PLOT_TITLE)</title>\
    <head>\
    <body>\
      <h1>$(PLOT_TITLE)</h1>\
      <hr size="1">' < $(HTML_OUTPUT) > $(HTML_OUTPUT)~	; \
	sed '$$ a \
    This document was generated on $(DATE)\
  </body>\
</html>' < $(HTML_OUTPUT)~ > $(HTML_OUTPUT);
dist-all:
	@echo -n "compressing ... "; \
	cd $(DISTDIR); tar -cjf $(DISTRUN_PATH).tar.bz2 *; echo "done"; \
	mv $(DISTRUN_PATH).tar.bz2 ..; cd ..; echo -n "removing copy ... "; \
	rm -rf $(DISTDIR); echo "done";
checks-all:

libs-compile:
	@. share/libs-compile.sh; \
	libscompile '$(SUBDIRS)' '$(RUN_PATH)' '$(JOB_SUBMIT_COMMAND)';

runs-rest:
	@. share/runs-rest.sh; \
	runsrest '$(SUBDIRS)' '$(RUN_PATH)' '$(JOB_SUBMIT_COMMAND)';

libs-check:
	@. share/libs-check.sh; \
	libscheck '$(SUBDIRS)' '$(RUN_PATH)' '$(SHOW_WHICH)';

runs-check:
	@. share/runs-check.sh; \
	runscheck '$(SUBDIRS)' '$(RUN_PATH)';

RECURSIVE_TARGETS = clean-libs-recursive clean-runs-recursive \
	clean-plots-recursive clean-checks-recursive \
	libs-recursive runs-recursive plots-recursive \
	dist-recursive checks-recursive 

$(RECURSIVE_TARGETS):
	@set fnord $$MAKEFLAGS; amf=$$2; \
	target=`echo $@ | sed s/-recursive//`; \
	$(MAKE) $(MAKEFLAGS) "$$target-checks" || exit 1; \
	list='$(SUBDIRS)'; for subdir in $$list; do \
	  echo "Making $$target in $$subdir"; \
	  local_target="$$target"; \
	  (cd $$subdir && $(MAKE) $(MAKEFLAGS) \
	    DISTDIR=$(PWD)/$(DISTDIR)/$$subdir $$local_target) \
	   || case "$$amf" in *=*) exit 1;; \
	    *k*) fail=yes;; *) exit 1;; esac; \
	done; \
	$(MAKE) $(MAKEFLAGS) "$$target-all" || exit 1; \
	test -z "$$fail"

