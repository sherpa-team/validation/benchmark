TARGET = $(notdir $(PWD))

export OLD_XSC_RUN_CMD := \
	sed -e's:__REF_TITLE__:$(REF_TITLE):g' \
	  -e's:__RUN_TITLE__:$(RUN_TITLE):g' \
	  -e's:__REF_PATH__:$(REF_PATH):g' \
	  -e's:__RUN_PATH__:$(RUN_PATH):g' \
	  -e's:__RES_PATH__:$(RES_PATH):g' \
	  -e's:__OUTPUT_PATH__:$(OUTPUT_PATH)/$(TARGET):g' \
	< $$xsc > testxs.conf; $(TEST_XS) $(OUTPUT_PATH)/$(TARGET) < testxs.conf
export XSC_RUN_CMD := \
        for i in $(RUNDIRS); do \
	  if test -f $(RUN_PATH)/$$i/$(RES_PATH).db && \
	  test $(RUN_PATH)/$$i/$(RES_PATH).db -nt $(RUN_PATH)/$$i/$(RES_PATH); then \
	    rm -rf $(RUN_PATH)/$$i/$(RES_PATH); \
	    $(DB2DIR) $(RUN_PATH)/$$i/$(RES_PATH).db XS_; fi; done; \
	$(OLD_XSC_RUN_CMD)
export COMPARE_RUN_CMD := \
	sed -e's:__REF_TITLE__:$(REF_TITLE):g' \
	  -e's:__RUN_TITLE__:$(RUN_TITLE):g' \
	  -e's:__REF_PATH__:$(REF_PATH):g' \
	  -e's:__RUN_PATH__:$(RUN_PATH):g' \
	  -e's:__ANA_PATH__:$(ANA_PATH):g' \
	  -e's:__ARF_PATH__:$(ARF_PATH):g' \
	  -e's:__OUTPUT_PATH__:$(OUTPUT_PATH)/$(TARGET):g' \
	  -e's:__RIVET_MKHTML__:$(RIVET_MKHTML):g' \
	  -e's:Compare:$(COMPARE):g' < analyse.in > analyse; \
	chmod 755 analyse; PATH=$(SCRIPT_PATH):$$PATH ./analyse \

.PHONY: clean-libs clean-runs clean-plots clean-checks libs runs plots checks

clean-runs:
	@subdirs='$(RUNDIRS)'; for i in $$subdirs; do \
	  spbn=$$(echo $$i | awk '{ split($$1,a,"["); print a[1]; }'); \
	  echo "making clean-runs in '$$spbn'"; \
	  sf=`echo $$spbn | awk '{ n=split($$1,a,"/"); print a[n]; }'`; \
	  if test -f $$(echo $(RUN_PATH)/$$spbn/$$sf*.log | \
	      awk '{ print $$1; }'); then $(JOB_CANCEL_COMMAND) \
	    `grep -ho 'https:.*:9000.*$$' $(RUN_PATH)/$$spbn/$$sf*.log`; \
	  fi; rm -f $(RUN_PATH)/$$spbn/*.log*; test $(CLEAN_RESULTS) -eq 0 || \
	  rm -rf $(RUN_PATH)/$$spbn/$(RES_PATH)*; done;

clean-libs:
	@subdirs='$(RUNDIRS)'; for i in $$subdirs; do \
	  spbn=$$(echo $$i | awk '{ split($$1,a,"["); print a[1]; }'); \
	  echo "making clean-libs in '$$spbn'"; \
	  sf=`echo $$spbn | awk '{ n=split($$1,a,"/"); print a[n]; }'`; \
	  if test -f $$(echo $(RUN_PATH)/$$spbn/$$sf*.log | \
	      awk '{ print $$1; }'); then $(JOB_CANCEL_COMMAND) \
	    `grep -ho 'https:.*:9000.*$$' $(RUN_PATH)/$$spbn/$$sf*.log`; \
	  fi; rm -rf $(RUN_PATH)/$$spbn; done; \
	if test $$(ls -1A $(RUN_PATH)/ | wc -l) -eq 0; then \
	  echo "removing '$(RUN_PATH)'"; rm -rf $(RUN_PATH); fi

clean-plots:
	rm -rf $(OUTPUT_PATH)/$(TARGET)

clean-checks: clean-plots

$(RUN_PATH):
	@if ! test -d $(RUN_PATH); then mkdir $(RUN_PATH); fi; \
	for i in $(LIBDIRS); do \
	  spbn=$$(echo $$i | awk '{ split($$1,a,"["); print a[1]; }'); \
	  splist=$$(echo $$i | awk '{ split($$1,a,"["); split(a[2],b,"]"); \
	    split(b[1],ps,","); for (i in ps) printf" "ps[i]; }'); \
	  if ! test -d $@/$$spbn; then if test -z "$$splist"; then \
	    $(GRID_LIB_RUN_CMD) -p$(SETUP_PATH)/$$spbn -n$@/$$spbn $(EVENTTAG) -r$(RES_PATH)/; \
	    else for j in $$splist; do \
	      $(GRID_LIB_RUN_CMD) -S$$j -cl$(RUN_PATH)/$$spbn/P_$$j \
	        -p$(SETUP_PATH)/$$spbn -n$@/$$spbn $(EVENTTAG) -r$(RES_PATH)_$$j/; done; fi; fi; done

%.log:
	@cur=`echo $(dir $@) | sed 's/\/$$//'`; \
	ct=`echo $(subst $(RUN_PATH),$(SETUP_PATH),$(dir $@)) | sed 's/\/$$//'`; \
	if test -f $(basename $@).sh; then \
	  if test -z '$(NOSUBMIT)'; then $(GRID_RERUN_CMD) -p$$cur \
	  `test -z '$(RUN_SUBDIR)' || echo -S$(RUN_SUBDIR)`; fi; else \
	  $(GRID_RUN_CMD) -l$(RUN_PATH)$(RUN_SUBLIBDIR) $(EVENTTAG) -p$$ct -n$$cur \
	  -r$(RES_PATH)`test -z '$(RUN_SUBDIR)' || echo _$(RUN_SUBDIR)`/ `test -z '$(NOSUBMIT)' \
	    || echo -M` `test -z '$(RUN_SUBDIR)' || echo -S$(RUN_SUBDIR)`; fi

%.slog:
	@cur='$@'; if echo $$cur | awk '{ if (match($$1,",")>0) exit 1; }'; then \
	  $(MAKE) $(MYMFLAGS) `if ! test -z '$(NOSUBMIT)'; then echo NOSUBMIT=1; fi` \
	  $(basename $@).log; else \
	  srf=$$(echo $$cur | awk '{ n=split($$1,a,"/"); print a[n-1]; }'); \
	  spbn=$$(echo $$cur | awk '{ split($$1,a,"["); print a[1]; }'); \
	  splist=$$(echo $$cur | awk '{ split($$1,a,"["); split(a[2],b,"]"); \
	    split(b[1],ps,","); for (i in ps) printf" "ps[i]; }'); \
	  splrf=$$(echo $$splist | awk '{ split($$1,a," "); print a[1]; }'); \
	  for j in $$splist; do $(MAKE) $(MYMFLAGS) $$spbn.$$j.log \
	    `if ! test -z '$(NOSUBMIT)'; then echo NOSUBMIT=1; fi` \
	    RUN_SUBDIR=$$j RUN_SUBLIBDIR=/$$srf/P_$$splrf; done; fi

libs: $(RUN_PATH)
	@subdirs='$(filter-out $(LIBDIRS),$(RUNDIRS))'; for i in $$subdirs; do \
	  sf=`echo $$i | awk '{ n=split($$1,a,"/"); print a[n]; }'`; \
	  spbn=`echo $$sf | awk '{ split($$1,a,"["); print a[1]; }'`; \
	  $(MAKE) $(MYMFLAGS) $(RUN_PATH)/$$spbn/$$sf.slog NOSUBMIT=1; done

runs: 
	@subdirs='$(RUNDIRS)'; for i in $$subdirs; do \
	  sf=`echo $$i | awk '{ n=split($$1,a,"/"); print a[n]; }'`; \
	  spbn=`echo $$sf | awk '{ split($$1,a,"["); print a[1]; }'`; \
	  $(MAKE) $(MYMFLAGS) $(RUN_PATH)/$$spbn/$$sf.slog; done

export HTML_DESCRIPTION
plots: $(OUTPUT_PATH)/$(TARGET)
	@echo >> "$(HTML_OUTPUT)";
	@echo "$$HTML_DESCRIPTION" >> "$(HTML_OUTPUT)";
	@echo >> "$(HTML_OUTPUT)";

dist:
	@mkdir $(DISTDIR)/$(DISTRUN_PATH) || exit 1; \
	subdirs='$(RUNDIRS)'; for i in $$subdirs; do \
	  sf=`echo $$i | awk '{ n=split($$1,a,"/"); print a[n]; }'`; \
	  spbn=`echo $$sf | awk '{ split($$1,a,"["); print a[1]; }'`; \
	  mkdir $(DISTDIR)/$(DISTRUN_PATH)/$$spbn || exit 1; \
	  echo "making dist in '$$spbn'"; \
	  cd $(RUN_PATH); \
	  acp=$$(find $$spbn/$(RES_PATH)* -name \*XS\*); \
	  acp=$$acp" "$$(find $$spbn/$(RES_PATH)* -name WD\*); \
	  cd -; \
	  for cr in $$acp; do \
	    if ! test -d $(DISTDIR)/$(DISTRUN_PATH)/$$(dirname $$cr); \
	    then mkdir -p $(DISTDIR)/$(DISTRUN_PATH)/$$(dirname $$cr) || exit 1; fi; \
	    cp -dpr $(RUN_PATH)/$$cr $(DISTDIR)/$(DISTRUN_PATH)/$$cr; done; done

.PHONY: $(RUN_PATH)
