libscompile()
{
    od=$PWD; list=$1; rp=$2; scm=$3;
    for subdir in $list; do
	if test -d $subdir/$rp; then
	    echo -n "Checking libs in $subdir ... ";
	    cd $subdir/$rp; nd=$PWD; lr=1;
	    apd=`find . -maxdepth 3 -name Amegic`;
	    if test -z "$apd"; then echo -n "no libs"; fi;
	    npp=`echo $apd | wc -w`; 
	    for pd in $apd; do
		if test $npp -gt 1; then echo -en "\n  $pd "; fi;
		echo "{";
	        ln=`echo $pd/P*/P* | awk '{ for (i=1;i<=NF;++i) \
		    { n=split($i,a,"P"); if (match(a[n],"[.]")==0 && \
	            match(a[n],"[*]")==0) print "P"a[n]; } }'`;
	        ln=$ln" "`echo $pd/P*/fsr* | \
		    awk '{ for (i=1;i<=NF;++i) { n=split($i,a,"fsr"); \
		    if (match(a[n],"[.]")==0 && match(a[n],"[*]")==0) \
                    print "fsr"a[n]; } }'`;
	        ml=0; 
		for i in $ln; do 
		    if ! test -f $pd/lib/libProc_$i.so; then
			cdir=`echo $pd/P?_*/$i | awk \
			    '{ sub("'$i'","",$0); print $0; }'`;
			cd $cdir; if ! test -f $i.log && test -f $i.jdl; then
			    echo "    resubmit $i"; 
			    $scm $i.jdl; 
			fi; 
			cd $nd; 
		    fi; 
		done;
		echo -n "  }"; 
	    done; 
	    if test $lr -eq 0; then
		if test `find . -maxdepth 3 -name run.log | wc -l` -eq 0; then 
		    echo -n ", no run log"; 
		fi; 
	    fi;
	    echo; 
	    cd $od; 
	fi; 
    done;
};
