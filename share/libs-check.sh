libscheck()
{
    od=$PWD; list=$1; rp=$2; sw=$3;
    for subdir in $list; do
	if test -d $subdir/$rp; then
	    echo -n "Checking libs in $subdir ... ";
	    cd $subdir/$rp; 
	    lr=1;
	    apd=`find . -maxdepth 3 -name Amegic`; 
	    if test -z "$apd"; then 
		echo -n "no libs"; 
	    fi;
	    npp=`echo $apd | wc -w`; 
	    for pd in $apd; do
		if test $npp -gt 1; then echo -en "\n  $pd "; fi;
		test -z "$sw" || echo "{";
		ln=`echo $pd/P*/P* | awk '{ for (i=1;i<=NF;++i) \
		    { n=split($i,a,"P"); if (match(a[n],"[.]")==0 && \
		    match(a[n],"[*]")==0) print "P"a[n]; } }'`; \
		ln=$ln" "`echo $pd/P*/fsr* | \
		    awk '{ for (i=1;i<=NF;++i) { n=split($i,a,"fsr"); \
		    if (match(a[n],"[.]")==0 && match(a[n],"[*]")==0) \
		    print "fsr"a[n]; } }'`;
		ml=0; 
		for i in $ln; do 
		    if ! test -f $pd/lib/libProc_$i.so; then 
			let ++ml;
			test -z "$sw" || echo "    $i"; 
		    fi; 
		done;
		test -z "$sw" || echo -n "  } -> ";
		if test $ml -gt 0; then 
		    echo -en "\033[31m$ml(`echo $ln | wc -w`) not done\033[0m";
		    lr=0; 
		else 
		    echo -en "\033[32mdone\033[0m"; 
		fi; 
	    done;
	    if test $lr -eq 0; then
		if test `find . -maxdepth 3 -name run.log | wc -l` -eq 0; then 
		    echo -n ", no run log"; 
		fi; 
	    fi;
	    echo; 
	    cd $od; 
	fi; 
    done;
};
